<?php

Route::group(['prefix' => 'ajax'], function () {

    Route::get('subcategories/{category?}', [
        'as' => 'ajax.loadSubcategories',
        'uses' => 'AjaxController@subcategories'
    ])->where('category', '[0-9]+');

    Route::get('years/{carModel?}', [
        'as' => 'ajax.loadYears',
        'uses' => 'AjaxController@years'
    ])->where('carModel', '[0-9]+');

    Route::get('adresses/{user?}', [
        'as' => 'ajax.loadAdress',
        'uses' => 'AjaxController@adress'
    ])->where('user', '[0-9]+');

});

Route::group(['prefix' => 'admin'], function () {




    Route::get('login', [
        'as' => 'admin.loginForm',
        'uses' => 'Admin\LoginController@index'
    ]);

    Route::post('login', [
        'as' => 'admin.login',
        'uses' => 'Admin\LoginController@login'
    ]);

    Route::get('logout', [
        'as' => 'admin.logout',
        'uses' => 'Admin\LoginController@logout'
    ]);

    Route::group(['middleware' => 'admin.auth'], function () {

        Route::get('/', [
            'as' => 'admin.dashboard',
            'uses' => 'Admin\DashboardController@index'
        ]);

        /*** SELLERS ***/
        Route::group(['prefix' => 'sellers', 'middleware' => 'admin.isAdmin'], function () {
            Route::get('/', [
                'as' => 'admin.sellers',
                'uses' => 'Admin\SellersController@index'
            ]);

            Route::get('create', [
                'as' => 'admin.sellers.create',
                'uses' => 'Admin\SellersController@create'
            ]);

            Route::post('create', [
                'as' => 'admin.sellers.store',
                'uses' => 'Admin\SellersController@store'
            ]);

            Route::get('edit/{user}', [
                'as' => 'admin.sellers.edit',
                'uses' => 'Admin\SellersController@edit'
            ])->where('user', '[0-9]+');

            Route::patch('edit/{user}', [
                'as' => 'admin.sellers.update',
                'uses' => 'Admin\SellersController@update'
            ])->where('user', '[0-9]+');

            Route::get('updateStatus/{user}', [
                'as' => 'admin.sellers.updateStatus',
                'uses' => 'Admin\SellersController@updateStatus'
            ])->where('user', '[0-9]+');


            Route::get('delete/{user}', [
                'as' => 'admin.sellers.delete',
                'uses' => 'Admin\SellersController@delete'
            ])->where('user', '[0-9]+');

            Route::delete('delete/{user}', [
                'as' => 'admin.sellers.destroy',
                'uses' => 'Admin\SellersController@destroy'
            ])->where('user', '[0-9]+');
        });

        /*** CAR MODELS ***/
        Route::group(['prefix' => 'car-models', 'middleware' => 'admin.isAdmin'], function () {
            Route::get('/', [
                'as' => 'admin.carModels',
                'uses' => 'Admin\CarModelsController@index'
            ]);

            Route::get('create', [
                'as' => 'admin.carModels.create',
                'uses' => 'Admin\CarModelsController@create'
            ]);

            Route::post('create', [
                'as' => 'admin.carModels.store',
                'uses' => 'Admin\CarModelsController@store'
            ]);

            Route::get('edit/{carModel}', [
                'as' => 'admin.carModels.edit',
                'uses' => 'Admin\CarModelsController@edit'
            ])->where('carModel', '[0-9]+');

            Route::patch('edit/{carModel}', [
                'as' => 'admin.carModels.update',
                'uses' => 'Admin\CarModelsController@update'
            ])->where('carModel', '[0-9]+');

            Route::get('delete/{carModel}', [
                'as' => 'admin.carModels.delete',
                'uses' => 'Admin\CarModelsController@delete'
            ])->where('carModel', '[0-9]+');

            Route::delete('delete/{carModel}', [
                'as' => 'admin.carModels.destroy',
                'uses' => 'Admin\CarModelsController@destroy'
            ])->where('carModel', '[0-9]+');
        });

        /*** CATEGORIES ***/
        Route::group(['prefix' => 'categories', 'middleware' => 'admin.isAdmin'], function () {
            Route::get('/', [
                'as' => 'admin.categories',
                'uses' => 'Admin\CategoriesController@index'
            ]);

            Route::get('create', [
                'as' => 'admin.categories.create',
                'uses' => 'Admin\CategoriesController@create'
            ]);

            Route::post('create', [
                'as' => 'admin.categories.store',
                'uses' => 'Admin\CategoriesController@store'
            ]);

            Route::get('edit/{category}', [
                'as' => 'admin.categories.edit',
                'uses' => 'Admin\CategoriesController@edit'
            ])->where('category', '[0-9]+');

            Route::patch('edit/{category}', [
                'as' => 'admin.categories.update',
                'uses' => 'Admin\CategoriesController@update'
            ])->where('category', '[0-9]+');

            Route::get('delete/{category}', [
                'as' => 'admin.categories.delete',
                'uses' => 'Admin\CategoriesController@delete'
            ])->where('category', '[0-9]+');

            Route::delete('delete/{category}', [
                'as' => 'admin.categories.destroy',
                'uses' => 'Admin\CategoriesController@destroy'
            ])->where('category', '[0-9]+');
        });

        /*** SUBCATEGORIES ***/
        Route::group(['prefix' => 'subcategories', 'middleware' => 'admin.isAdmin'], function () {
            Route::get('/', [
                'as' => 'admin.subcategories',
                'uses' => 'Admin\SubcategoriesController@index'
            ]);

            Route::get('create', [
                'as' => 'admin.subcategories.create',
                'uses' => 'Admin\SubcategoriesController@create'
            ]);

            Route::post('create', [
                'as' => 'admin.subcategories.store',
                'uses' => 'Admin\SubcategoriesController@store'
            ]);

            Route::get('edit/{subcategory}', [
                'as' => 'admin.subcategories.edit',
                'uses' => 'Admin\SubcategoriesController@edit'
            ])->where('subcategory', '[0-9]+');

            Route::patch('edit/{subcategory}', [
                'as' => 'admin.subcategories.update',
                'uses' => 'Admin\SubcategoriesController@update'
            ])->where('subcategory', '[0-9]+');

            Route::get('delete/{subcategory}', [
                'as' => 'admin.subcategories.delete',
                'uses' => 'Admin\SubcategoriesController@delete'
            ])->where('subcategory', '[0-9]+');

            Route::delete('delete/{subcategory}', [
                'as' => 'admin.subcategories.destroy',
                'uses' => 'Admin\SubcategoriesController@destroy'
            ])->where('subcategory', '[0-9]+');
        });

        /*** COUPONS ***/
        Route::group(['prefix' => 'coupons', 'middleware' => 'admin.isAdmin'], function () {
            Route::get('/', [
                'as' => 'admin.coupons',
                'uses' => 'Admin\CouponsController@index'
            ]);

            Route::get('create', [
                'as' => 'admin.coupons.create',
                'uses' => 'Admin\CouponsController@create'
            ]);

            Route::post('create', [
                'as' => 'admin.coupons.store',
                'uses' => 'Admin\CouponsController@store'
            ]);

            Route::get('edit/{coupon}', [
                'as' => 'admin.coupons.edit',
                'uses' => 'Admin\CouponsController@edit'
            ])->where('coupon', '[0-9]+');

            Route::patch('edit/{coupon}', [
                'as' => 'admin.coupons.update',
                'uses' => 'Admin\CouponsController@update'
            ])->where('coupon', '[0-9]+');

            Route::get('delete/{coupon}', [
                'as' => 'admin.coupons.delete',
                'uses' => 'Admin\CouponsController@delete'
            ])->where('coupon', '[0-9]+');

            Route::delete('delete/{coupon}', [
                'as' => 'admin.coupons.destroy',
                'uses' => 'Admin\CouponsController@destroy'
            ])->where('coupon', '[0-9]+');
        });

        /*** CLIENTS ***/
        Route::group(['prefix' => 'clients'], function () {
            Route::get('/', [
                'as' => 'admin.clients',
                'uses' => 'Admin\ClientsController@index'
            ]);

            Route::get('create', [
                'as' => 'admin.clients.create',
                'uses' => 'Admin\ClientsController@create'
            ]);

            Route::post('create', [
                'as' => 'admin.clients.store',
                'uses' => 'Admin\ClientsController@store'
            ]);

            Route::get('edit/{client}', [
                'as' => 'admin.clients.edit',
                'uses' => 'Admin\ClientsController@edit'
            ])->where('client', '[0-9]+');

            Route::patch('edit/{client}', [
                'as' => 'admin.clients.update',
                'uses' => 'Admin\ClientsController@update'
            ])->where('client', '[0-9]+');

            Route::get('updateStatus/{client}', [
                'as' => 'admin.clients.updateStatus',
                'uses' => 'Admin\ClientsController@updateStatus'
            ])->where('client', '[0-9]+');

            Route::get('delete/{client}', [
                'as' => 'admin.clients.delete',
                'uses' => 'Admin\ClientsController@delete'
            ])->where('client', '[0-9]+');

            Route::delete('delete/{client}', [
                'as' => 'admin.clients.destroy',
                'uses' => 'Admin\ClientsController@destroy'
            ])->where('client', '[0-9]+');

            Route::get('show/{client}', [
                'as' => 'admin.clients.show',
                'uses' => 'Admin\ClientsController@show'
            ])->where('client', '[0-9]+');

            Route::get('ver/{user}', [
                'as' => 'admin.clients.ver',
                'uses' => 'Admin\ClientsController@ver'
            ])->where('client', '[0-9]+');

            Route::get('updateStatusUser/{user}', [
                'as' => 'admin.clients.updateStatusUser',
                'uses' => 'Admin\ClientsController@updateStatusUser'
            ])->where('client', '[0-9]+');

            Route::delete('deleteUser/{user}', [
                'as' => 'admin.clients.deleteUser',
                'uses' => 'Admin\ClientsController@deleteUser'
            ])->where('client', '[0-9]+');

            Route::get('eliminarUser/{user}', [
                'as' => 'admin.clients.eliminarUser',
                'uses' => 'Admin\ClientsController@eliminarUser'
            ])->where('client', '[0-9]+');

        });

        /*** CLIENTS ***/
        Route::group(['prefix' => 'dashboard'], function () {
            Route::get('/', [
                'as' => 'admin.dashboard',
                'uses' => 'Admin\DashboardController@index'
            ]);

            Route::post('updateNoGuia', [
                'as' => 'admin.updateNoGuia',
                'uses' => 'Admin\DashboardController@updateNoGuia'
            ]);

            Route::post('updateOrderToCompleted', [
                'as' => 'admin.updateOrderToCompleted',
                'uses' => 'Admin\DashboardController@updateOrderToCompleted'
            ]);

            Route::get('create', [
                'as' => 'admin.dashboard.create',
                'uses' => 'Admin\DashboardController@create'
            ]);

            Route::post('create', [
                'as' => 'admin.dashboard.store',
                'uses' => 'Admin\DashboardController@store'
            ]);

            Route::get('edit/{order}', [
                'as' => 'admin.dashboard.edit',
                'uses' => 'Admin\DashboardController@edit'
            ])->where('client', '[0-9]+');

            Route::patch('edit/{order}', [
                'as' => 'admin.dashboard.update',
                'uses' => 'Admin\DashboardController@update'
            ])->where('client', '[0-9]+');

            Route::get('delete/{order}', [
                'as' => 'admin.dashboard.delete',
                'uses' => 'Admin\DashboardController@delete'
            ])->where('client', '[0-9]+');

            Route::delete('delete/{order}', [
                'as' => 'admin.dashboard.destroy',
                'uses' => 'Admin\DashboardController@destroy'
            ])->where('client', '[0-9]+');

            Route::get('show/{order}', [
                'as' => 'admin.dashboard.show',
                'uses' => 'Admin\DashboardController@show'
            ]);
        });

        Route::group(['prefix' => 'shipcost', 'middleware' => 'admin.isAdmin'], function () {
            Route::get('/', [
                'as' => 'admin.shipcost',
                'uses' => 'Admin\ShipcostController@index'
            ]);

            Route::get('create', [
                'as' => 'admin.shipcost.create',
                'uses' => 'Admin\ShipcostController@create'
            ]);

            Route::post('create', [
                'as' => 'admin.shipcost.store',
                'uses' => 'Admin\ShipcostController@store'
            ]);

            Route::get('edit/{order}', [
                'as' => 'admin.shipcost.edit',
                'uses' => 'Admin\ShipcostController@edit'
            ])->where('client', '[0-9]+');

            Route::patch('edit/{order}', [
                'as' => 'admin.shipcost.update',
                'uses' => 'Admin\ShipcostController@update'
            ])->where('client', '[0-9]+');

            Route::get('delete/{order}', [
                'as' => 'admin.shipcost.delete',
                'uses' => 'Admin\ShipcostController@delete'
            ])->where('client', '[0-9]+');

            Route::delete('delete/{order}', [
                'as' => 'admin.shipcost.destroy',
                'uses' => 'Admin\ShipcostController@destroy'
            ])->where('client', '[0-9]+');

            Route::get('show/{order}', [
                'as' => 'admin.shipcost.show',
                'uses' => 'Admin\ShipcostController@show'
            ]);
        });

        /*** PRODUCTS ***/
        Route::group(['prefix' => 'products', 'middleware' => 'admin.isAdmin'], function () {
            Route::get('/', [
                'as' => 'admin.products',
                'uses' => 'Admin\ProductsController@index'
            ]);

            Route::get('search', [
                'as' => 'admin.products.search',
                'uses' => 'Admin\ProductsController@search'
            ]);

            Route::get('create', [
                'as' => 'admin.products.create',
                'uses' => 'Admin\ProductsController@create'
            ]);

            Route::post('create', [
                'as' => 'admin.products.store',
                'uses' => 'Admin\ProductsController@store'
            ]);

            Route::post('uploadExcel', [
                'as' => 'admin.products.uploadExcel',
                'uses' => 'Admin\ProductsController@uploadExcel'
            ]);


            Route::get('edit/{product}', [
                'as' => 'admin.products.edit',
                'uses' => 'Admin\ProductsController@edit'
            ])->where('product', '[0-9]+');

            Route::get('addToPromo/{product}', [
                'as' => 'admin.products.addToPromo',
                'uses' => 'Admin\ProductsController@addToPromo'
            ])->where('product', '[0-9]+');

            Route::get('removeOffPromo/{product}', [
                'as' => 'admin.products.removeOffPromo',
                'uses' => 'Admin\ProductsController@removeOffPromo'
            ])->where('product', '[0-9]+');

            Route::patch('edit/{product}', [
                'as' => 'admin.products.update',
                'uses' => 'Admin\ProductsController@update'
            ])->where('product', '[0-9]+');

            Route::get('delete/{product}', [
                'as' => 'admin.products.delete',
                'uses' => 'Admin\ProductsController@delete'
            ])->where('product', '[0-9]+');

            Route::delete('delete/{product}', [
                'as' => 'admin.products.destroy',
                'uses' => 'Admin\ProductsController@destroy'
            ])->where('product', '[0-9]+');

            Route::get('show/{product}', [
                'as' => 'admin.products.show',
                'uses' => 'Admin\ProductsController@show'
            ])->where('product', '[0-9]+');

            Route::get('importar', [
                'as' => 'admin.products.import',
                'uses' => 'Admin\ProductsController@importByFile'
            ]);

            Route::post('/import', ['as'=>'import', 'uses'=>'Controller@import']);
        });

        /*** BANNERS ***/
        Route::group(['prefix' => 'banners', 'middleware' => 'admin.isAdmin'], function () {
            Route::get('/', [
                'as' => 'admin.banners',
                'uses' => 'Admin\BannersController@index'
            ]);

            Route::get('create', [
                'as' => 'admin.banners.create',
                'uses' => 'Admin\BannersController@create'
            ]);

            Route::post('create', [
                'as' => 'admin.banners.store',
                'uses' => 'Admin\BannersController@store'
            ]);

            Route::get('edit/{banner}', [
                'as' => 'admin.banners.edit',
                'uses' => 'Admin\BannersController@edit'
            ])->where('banner', '[0-9]+');

            Route::patch('edit/{banner}', [
                'as' => 'admin.banners.update',
                'uses' => 'Admin\BannersController@update'
            ])->where('banner', '[0-9]+');

            Route::get('delete/{banner}', [
                'as' => 'admin.banners.delete',
                'uses' => 'Admin\BannersController@delete'
            ])->where('banner', '[0-9]+');

            Route::delete('delete/{banner}', [
                'as' => 'admin.banners.destroy',
                'uses' => 'Admin\BannersController@destroy'
            ])->where('banner', '[0-9]+');
        });

    });

});



/*** FRONT-END ROUTES ***/

Route::name('home')->get('/', 'PagesController@index');

Route::name('activar')->get('/activar/{token}', 'PagesController@activarUsuario');
Route::name('about')->get('/nosotros', 'PagesController@about'); //Falta
Route::name('terms')->get('/terminos-y-condiciones', 'PagesController@terms');
Route::name('questions')->get('/preguntas-frecuentes', 'PagesController@questions');
Route::name('login')->get('/ingreso', 'PagesController@login');
Route::name('log')->post('/log', 'PagesController@log');
Route::name('logout')->get('/logout', 'PagesController@logout');

Route::name('orders')->get('/compras', 'PagesController@orders')->middleware('auth');
Route::name('infoOrder')->get('/orden/{order}', 'PagesController@infoOrder')->middleware('auth');

Route::name('adresses')->get('/direcciones', 'PagesController@adresses')->middleware('auth');
Route::name('myAdress')->post('/myAdress', 'PagesController@myAdress')->middleware('auth');
Route::name('createAdress')->post('/direccion', 'PagesController@createAdress')->middleware('auth');
Route::name('rmAdress')->post('/rmadress', 'PagesController@rmAdress')->middleware('auth');
Route::name('updateAdress')->post('/updateAdress', 'PagesController@updateAdress')->middleware('auth');

Route::name('contact')->get('/contacto', 'PagesController@contact'); //Falta

Route::name('register')->get('/registro', 'PagesController@register');
Route::name('reg')->post('/reg', 'PagesController@reg');

Route::name('recover')->get('/recuperar-clave', 'PagesController@recover');
Route::name('my-account')->get('/perfil', 'PagesController@myAccount'); //Faltan detalles
Route::name('products')->get('/productos', 'PagesController@products');
Route::name('product')->get('/productos/{product}/{slug}', 'PagesController@product');

Route::name('checkout')->get('/checkout', 'PagesController@checkout');
Route::name('add-to-cart')->post('/añadir', 'PagesController@addToCart');
Route::name('change-item-cart')->post('/changeItem', 'PagesController@changeQtyOnCart');
Route::name('remove-item')->get('/eliminar/{id}', 'PagesController@removeItem');

Route::name('summary')->post('/summary', 'PagesController@summary');
Route::name('confirmOrder')->post('/confirmOrder', 'PagesController@confirmOrder');
Route::name('changePassword')->post('/changePassword', 'PagesController@changePassword');


Route::name('pay')->get('/direccion', 'PagesController@pay')->middleware('auth');
Route::name('paycheck')->post('/pagar', 'PagesController@paycheck')->middleware('auth');
Route::name('payConekta')->post('/payConekta', 'PagesController@payConekta')->middleware('auth');
Route::name('payOxxoConekta')->post('/payOxxoConekta', 'PagesController@payOxxoConekta')->middleware('auth');

Route::name('oxxoPayInvoice')->get('/oxxoPayInvoice/{order}', 'PagesController@oxxoPayInvoice')->middleware('auth');

Route::name('searcher')->post('/buscar', 'PagesController@search');
Route::name('searcher')->get('/buscar', 'PagesController@search');

Route::name('datas')->get('/datas', 'PagesController@datas');

// Route::name('webhook')->get('/webhooks', 'PagesController@webhooks');

Route::name('webhook')->post('/webhooks', 'PagesController@webhooks');

Route::name('addToClient')->post('/addToClient', 'PagesController@addToClient');
// Show payment form
// Route::get('/payment/add-funds/paypal', 'PagesController@checkout');

// Post payment details for store/process API request
Route::name('paypalp')->post('/payment/add-funds/paypal', 'PaypalController@store');

// Handle status
Route::name('paypalg')->get('/payment/add-funds/paypal/status', 'PaypalController@getPaymentStatus');

Route::name('resend')->get('/resend', 'PagesController@resend');
Route::name('resendActivation')->post('/resendActivation', 'PagesController@resendActivation');

Route::name('password.email')->post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
Route::name('password.request')->get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::name('password.reset')->get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');

Route::name('applyCoupon')->post('/applyCoupon', 'PagesController@applyCoupon');


