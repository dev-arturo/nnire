<?php

use Illuminate\Database\Seeder;

class ClientTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('client_types')->insert([
            ['name' => 'Mayorista'],
            ['name' => 'Minorista']
        ]);
    }
}
