<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku')->nullable();
            $table->string('model')->nullable();
            $table->string('eanUpc')->nullable();
            $table->string('name');
            $table->string('inpromotion');
            $table->string('description');
            $table->text('dataSheet')->nullable();
            $table->string('image')->nullable();
            $table->decimal('price', 8, 2);
            $table->integer('car_model_id')
                ->unsigned()
                ->index();
            $table->integer('year_id')
                ->unsigned()
                ->index();
            $table->integer('subcategory_id')
                ->unsigned()
                ->index();
            $table->timestamps();

            //$table->unique(['name', 'car_model_id', 'subcategory_id', 'year_id']);
            $table->foreign('car_model_id')
                ->on('car_models')
                ->references('id')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('year_id')
                ->on('years')
                ->references('id')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('subcategory_id')
                ->on('subcategories')
                ->references('id')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
