<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;
use Validator;
use Config;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\Log;

use App\Flag;
use App\Product;
use App\Subcategory;
use App\Year;
use App\CarModel;
use App\Category;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class ImportManager extends Command
{
    protected $signature = 'import:excelfile';
    protected $description = 'This imports an excel file';
    protected $chunkSize = 20000;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        ini_set('memory_limit', '-1');
        set_time_limit(0);

        // ignore_user_abort(true);

        $file = Flag::where('imported','=','0')->orderBy('created_at', 'DESC')->first();

        if ($file != null) {
            $file_path = Config::get('filesystems.disks.local.root') . '/' .$file->file_name;

            Excel::load($file_path, function($reader) use($file) {
                $objWorksheet = $reader->getActiveSheet();
                $file->total_rows = $objWorksheet->getHighestRow() - 1; //exclude the heading
                $file->save();
            });

            //now let's import the rows, one by one while keeping track of the progress
            Excel::filter('chunk')->selectSheetsByIndex(0)->load($file_path)->chunk($this->chunkSize, function($reader) use ($file) {
                $rows = $reader->toArray();
                $counter = 0;

                foreach ($reader->toArray() as $row) {
                    // dd($row);
                    DB::beginTransaction();
                    DB::connection()->disableQueryLog();
                    try {

                        if ($row['marca'] == '') {
                            $carModel = CarModel::where('name', 'NA')->first();
                        }else{
                            $carModel = CarModel::where('name', trim($row['marca']))->first();

                            if (!$carModel) {
                                $carModel = new CarModel([
                                    'name' => trim($row['marca'])
                                ]);
                                $carModel->save();
                            }
                        }

                        if ($row['sistema'] == '') {
                            $category = Category::where('name', 'NA')->first();
                        }else{
                            $category = Category::where('name', trim($row['sistema']))->first();

                            if (!$category) {
                                $category = new Category([
                                    'name' => trim($row['sistema'])
                                ]);
                                $category->save();
                            }
                        }

                        if ($row['subsistema'] == '') {
                            $subcategory = Subcategory::where('name', 'NA')->first();
                        }else{
                            $subcategory = Subcategory::where('name', trim($row['subsistema']))->where('category_id', $category->id)->first();

                            if (!$subcategory) {
                                $subcategory = new Subcategory([
                                    'name' => trim($row['subsistema'])
                                ]);
                                $subcategory->category()->associate($category);
                                $subcategory->save();
                            }
                        }


                    $product = Product::where('subcategory_id', $subcategory->id)->where('car_model_id', $carModel->id)->where('sku', $row['sku'])->first();

                    if(is_numeric($row['precio_pub'])){
                        $pricepub = $row['precio_pub'];
                    }else{
                        $pricepub = 0;
                    }

                    if(is_numeric($row['mayoreo'])){
                        $mayor = $row['mayoreo'];
                    }else{
                        $mayor = 0;
                    }

                    $arr = array();

                    if ($row['anos']!='') {
                        $years = explode(",", rtrim($row['anos'], ','));
                        foreach ($years as $key => $value) {
                            $ids = Year::where('year','=', $value)->pluck('id');
                            if (count($ids)>0) {
                                array_push($arr,Year::findOrFail($ids[0]));
                            }
                        }
                    }

                    if (is_null($product)) {
                        $product = new Product([
                            'name' => $row['nombre'],
                            'model' => $row['descr_planta'],
                            'description' => $row['descripcion'],
                            'sku' => $row['sku'],
                            'eanUpc' => $row['intercambio_anterior'],
                            'price' => $pricepub,
                            'wholesale' => $mayor,
                            'years' => serialize($arr)
                        ]);
                    } else {
                        $product->update([
                            'name' => $row['nombre'],
                            'model' => $row['descr_planta'],
                            'description' => $row['descripcion'],
                            'sku' => $row['sku'],
                            'eanUpc' => $row['intercambio_anterior'],
                            'price' => $pricepub,
                            'wholesale' => $mayor,
                            'years' => serialize($arr)
                        ]);
                    }


                    $product->carModel()->associate($carModel);
                    $product->subcategory()->associate($subcategory);
                    $product->save();

                    } catch (\Exception $error) {
                        DB::rollback();
                        throw $error;
                    }

                    DB::commit();
                    $counter++;
                    // Log::info('Producto: ' . $product->id . 'guardado y/o actualizado');
                }

                $file = $file->fresh(); //reload from the database
                $file->rows_imported = $file->rows_imported + $counter;
                $file->save();
            });
            $imported_rows = $file->rows_imported;
            $file->imported =1;
            $file->save();
            Log::info('Registros de excel importados');

            $mail = new PHPMailer(true);
            try {
                //Server settings
                $mail->isSMTP();
                $mail->CharSet = "utf-8";
                $mail->Host = 'mail.refaccionesnissan.com.mx';
                $mail->SMTPAuth = true;
                $mail->Username = 'info@refaccionesnissan.com.mx';
                $mail->Password = 'LFH@2oH_QwwQ';
                $mail->SMTPSecure = 'ssl';
                $mail->Port = 465;

                //Recipients
                $mail->setFrom('info@refaccionesnissan.com.mx', 'Informaci��n del servidor');
                $mail->addAddress('ventasweb@refaccionesnissan.com.mx', 'Informaci��n de ventas');

                $mail->Subject = "Notificaci��n de productos importados.";
                $mail->MsgHTML("El proceso de importe ha terminado correctamente y se han importado " . $imported_rows . "productos.");

                $mail->send();
                // echo 'Message has been sent';
            } catch (phpmailerException $e) {
                //return redirect()->route('checkout')->with('error', $e);
            } catch (Exception $e) {
                //return redirect()->route('checkout')->with('error', $e);
            }
        }else{
            //Log::info('No hay nada que importar.');
        }
    }
}
