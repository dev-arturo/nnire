<?php

namespace App;

use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;
class Client extends Model
{
    use FormAccessible;
    /**
 * Get the user's first name.
 *
 * @param  string  $value
 * @return string
 */
public function getDateOfBirthAttribute($value)
{
    return Carbon::parse($value)->format('m/d/Y');
}

/**
 * Get the user's first name for forms.
 *
 * @param  string  $value
 * @return string
 */
public function formDateOfBirthAttribute($value)
{
    return Carbon::parse($value)->format('Y-m-d');
}
    protected $fillable = [
        'name',
        'user_id',
        'vendor_id',
        'adress_id',
        'rfc',
        'curp',
        'payment',
        'payment_method',
        'usage_cfdi',
        'creditLimit'
    ];

    public function clientType()
    {
        return $this->belongsTo(ClientType::class);
    }

    public function adress(){
        return $this->belongsTo(Adresses::class);
    }

    public function seller()
    {
        return $this->belongsTo(User::class, 'vendor_id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id');
    }

}
