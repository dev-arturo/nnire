<?php

namespace App\Http\Middleware\Admin;

use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return auth()->check() ? $next($request) : redirect()->route('admin.loginForm');
    }
}
