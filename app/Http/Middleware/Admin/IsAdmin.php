<?php

namespace App\Http\Middleware\Admin;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(auth()->user());
        return auth()->user()->isAdmin ? $next($request) : redirect()->route('admin.dashboard');
    }
}
