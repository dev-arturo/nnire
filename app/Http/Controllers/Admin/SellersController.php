<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Sellers\CreateRequest;
use App\Http\Requests\Admin\Sellers\EditRequest;
use App\User;
use App\Client;
use App\Seller;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Notifications\UserCreated;
use App\Notifications\OrderPayed;
use App\Notifications\OrderCreated;
use Illuminate\Http\Request;

class SellersController extends Controller
{

    public $apikey = 'key_dzRyXhR3Y2pTDBqSnqxghw';

    public function __construct()
    {
        View::share('section', 'sellers');
    }

    public function checkAuth(){
        if(auth()->check()){
            $user = Auth::user();
            if ($user->isAdmin != 1) {
                auth()->logout();
                return true;
            }
        }
    }


    public function index()
    {
        if (!$this->checkAuth()) {
            // dd(User::with('clients')->WithSeller()->get());
            return view('admin.sellers.index')->with([
                'sellers' => User::with('clients')->WithSeller()->get()
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }
    }

    public function create()
    {
        if (!$this->checkAuth()) {
            return view('admin.sellers.create');
        }else{
            return redirect()->route('admin.loginForm');
        }
    }

    public function store(CreateRequest $request)
    {

        \Conekta\Conekta::setApiKey($this->apikey);
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('es');

        
        if(preg_match('/^[\pL\s]+$/u', $request->get('name')) && preg_match('/^[\pL\s]+$/u', $request->get('surname'))){
            try {
                $customer = \Conekta\Customer::create(
                  array(
                    'name'  => $request->get('name') . ' ' . $request->get('surname'),
                    'email' => $request->get('email')
                  )
                );
                $activation_token = str_random(60);
    
                $user = User::create([
                    'name' => $request->get('name'),
                    'surname' => $request->get('surname'),
                    'email' => $request->get('email'),
                    'conekta_id' => $customer->id,
                    'isAdmin' => 3,
                    'password' => $request->get('password'),
                    'activation_token' => $activation_token,
                ]);
        
                $seller = Seller::create([
                    'user_id' => $user->id
                ]);
        
                $user->notify(new UserCreated($user));
        
                return redirect()->route('admin.sellers')->with([
                    'success' => [
                        'title' => '',
                        'body' => 'Se guard�� el registro de forma correcta.'
                    ]
                ]);
                
                } catch (\Conekta\ProcessingError $e){ 
                    echo $e->getMessage();
                    return redirect()->route('admin.sellers')->with([
                        'error' => [
                            'title' => '',
                            'body' => 'Ocurri�� un problema desconocido. Intente de nuevo m��s tarde.'
                        ]
                    ]);
                } catch (\Conekta\ParameterValidationError $e){
                    echo $e->getMessage();
                    return redirect()->route('admin.sellers')->with([
                        'error' => [
                            'title' => '',
                            'body' => 'Ocurri�� un problema desconocido. Intente de nuevo m��s tarde.'
                        ]
                    ]);
                }
        }else{
            return redirect()->back()->with([
                'warning' => [
                    'title' => '',
                    'body' => 'Los nombres s��lo deben llevar letras y espacios.'
                ]
            ]);
        }
        
        

        
    }

    public function edit(User $user)
    {
        if (!$this->checkAuth()) {
            return view('admin.sellers.edit')->with([
                'seller' => $user
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }
    }

    public function update(Request $request, User $user)
    {
        //dd($request->all());
        $user->update($request->all());

        return redirect()->route('admin.sellers')->with([
            'success' => [
                'title' => '',
                'body' => 'Se actualizó el registro de forma correcta.'
            ]
        ]);
    }

    public function delete(User $user)
    {
        return view('admin.sellers.delete')->with([
            'seller' => $user
        ]);
    }

    public function destroy(User $user)
    {
        $user->activo = 0;
        $user->save();

        return redirect()->route('admin.sellers')->with([
            'success' => [
                'title' => '',
                'body' => 'Se eliminó el registro de forma correcta.'
            ]
        ]);
    }

    public function updateStatus(User $user)
    {
        if (!auth()->user()->isAdmin) {
            if ($client->vendor_id != auth()->user()->id) {
                return redirect()->route('admin.sellers')->with([
                    'success' => [
                        'title' => '',
                        'body' => 'No tiene permiso de restaurar el cliente seleccionado.'
                    ]
                ]);
            }
        }

        $user->activo = 1;
        $user->save();

        return redirect()->route('admin.sellers')->with([
            'success' => [
                'title' => '',
                'body' => 'Se restauró el registro de forma correcta.'
            ]
        ]);
    }
}
