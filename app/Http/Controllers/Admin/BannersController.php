<?php

namespace App\Http\Controllers\Admin;

use App\Banner;
use App\Http\Requests\Admin\Banners\CreateRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Banners\EditRequest;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class BannersController extends Controller
{
    public function __construct()
    {
        View::share('section', 'banners');
    }

    public function checkAuth(){
        if(auth()->check()){
            $user = Auth::user();
            if ($user->isAdmin != 1) {
                auth()->logout();
                return true;
            }
        }
    }

    public function index()
    {
        if (!$this->checkAuth()) {
            return view('admin.banners.index')->with([
                'banners' => Banner::orderBy('order', 'asc')->get()
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }
        
    }

    public function create()
    {
        if (!$this->checkAuth()) {
            return view('admin.banners.create');
        }else{
            return redirect()->route('admin.loginForm');
        }
    }

    public function store(CreateRequest $request)
    {
        $banner = new Banner([
            'order' => Banner::nextPosition(),
            'hyperlink' => $request->get('hyperlink'),
            'hyperlinkTarget' => $request->get('hyperlinkTarget')
        ]);

        $banner->updateImage($request->file('image'));
        $banner->save();

        return redirect()->route('admin.banners')->with([
            'success' => [
                'title' => '',
                'body' => 'Se guardó el registro de forma correcta.'
            ]
        ]);
    }

    public function edit(Banner $banner)
    {
        if (!$this->checkAuth()) {
            return view('admin.banners.edit')->with([
                'banner' => $banner
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }
        
    }

    public function update(EditRequest $request, Banner $banner)
    {
        $banner->update($request->all());
        $banner->updateImage($request->file('image'));
        $banner->save();

        return redirect()->route('admin.banners')->with([
            'success' => [
                'title' => '',
                'body' => 'Se actualizó el registro de forma correcta.'
            ]
        ]);
    }

    public function delete(Banner $banner)
    {
        return view('admin.banners.delete')->with([
            'banner' => $banner
        ]);
    }

    public function destroy(Banner $banner)
    {
        $banner->delete();

        return redirect()->route('admin.banners')->with([
            'success' => [
                'title' => '',
                'body' => 'Se eliminó el registro de forma correcta.'
            ]
        ]);
    }
}
