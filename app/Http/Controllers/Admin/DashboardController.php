<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Order;
use App\Adresses;
use App\Client;
use App\User;

use App\Notifications\OrderGuiaUpdated;

class DashboardController extends Controller
{

    public $apikey = 'key_dzRyXhR3Y2pTDBqSnqxghw';

    public function __construct()
    {
        View::share('section', 'dashboard');
    }

    public function index()
    {

        if(auth()->check()){
            $user = Auth::user();
            if ($user->isAdmin == 0 || $user->isAdmin == 2) {
                auth()->logout();
                return redirect()->route('admin.loginForm');
            }
        }

        if (auth()->user()->isAdmin == 1) {
            $ordersPending = DB::table('orders')
                ->select('users.*', 'orders.*', 'orders.created_at as ord_fec')
                ->join('users', 'orders.user_id', '=', 'users.id')
                // ->join('adresses', 'adresses.id', '=', 'orders.adress_id')
                ->where('orders.estado', 0)
                ->get();


            $ordersPending->transform(function($order, $key){
                $order->cart = unserialize($order->cart);
                $order->adress = unserialize($order->adress);
                // dd($order);
                $percent = $order->cart->discount / 100;
                $discount = $order->cart->totalPrice * ($percent);

                $order->descuento = $discount;
                return $order;
            });

            // dd($ordersPending);

            $ordersInProgress = DB::table('orders')
                ->select('users.*', 'orders.*', 'orders.created_at as ord_fec')
                ->join('users', 'orders.user_id', '=', 'users.id')
                // ->join('adresses', 'adresses.id', '=', 'orders.adress_id')
                ->where('orders.estado', 1)
                ->get();

            if (count($ordersInProgress) > 0) {
                $ordersInProgress->transform(function($order, $key){
                    $order->cart = unserialize($order->cart);
                    $order->adress = unserialize($order->adress);
                    $percent = $order->cart->discount / 100;
                    $discount = $order->cart->totalPrice * ($percent);

                    $order->descuento = $discount;
                    return $order;
                });
            }

            $ordersCompleted = DB::table('orders')
                ->select('users.*', 'orders.*', 'orders.created_at as ord_fec')
                ->join('users', 'orders.user_id', '=', 'users.id')
                // ->join('adresses', 'adresses.id', '=', 'orders.adress_id')
                ->where('orders.estado', 2)
                ->get();

            if (count($ordersCompleted) > 0) {
                $ordersCompleted->transform(function($order, $key){
                    $order->cart = unserialize($order->cart);
                    $order->adress = unserialize($order->adress);
                    $percent = $order->cart->discount / 100;
                    $discount = $order->cart->totalPrice * ($percent);

                    $order->descuento = $discount;
                    return $order;
                });
            }
        }else{


            $ordersPending = DB::table('orders')
                ->select('users.*', 'orders.*', 'orders.created_at as ord_fec')
                ->join('users', 'orders.user_id', '=', 'users.id')
                ->join('clients', 'orders.user_id', '=', 'clients.user_id')
                ->where('orders.estado', 0)->where('vendor_id', auth()->user()->id)
                ->get();

            $ordersPending->transform(function($order, $key){
                $order->cart = unserialize($order->cart);
                $order->adress = unserialize($order->adress);
                $percent = $order->cart->discount / 100;
                $discount = $order->cart->totalPrice * ($percent);

                $order->descuento = $discount;
                return $order;
            });

            $ordersInProgress = DB::table('orders')
                ->join('users', 'orders.user_id', '=', 'users.id')
                // ->join('adresses', 'adresses.id', '=', 'orders.adress_id')
                ->join('clients', 'orders.user_id', '=', 'clients.user_id')
                ->where('orders.estado', 1)->where('vendor_id', auth()->user()->id)
                ->get();

            if (count($ordersInProgress) > 0) {
                $ordersInProgress->transform(function($order, $key){
                    $order->cart = unserialize($order->cart);
                    $order->adress = unserialize($order->adress);
                    $percent = $order->cart->discount / 100;
                    $discount = $order->cart->totalPrice * ($percent);

                    $order->descuento = $discount;
                    return $order;
                });
            }

            $ordersCompleted = DB::table('orders')
                ->join('users', 'orders.user_id', '=', 'users.id')
                // ->join('adresses', 'adresses.id', '=', 'orders.adress_id')
                ->join('clients', 'orders.user_id', '=', 'clients.user_id')
                ->where('orders.estado', 2)->where('vendor_id', auth()->user()->id)
                ->get();

            if (count($ordersCompleted) > 0) {
                $ordersCompleted->transform(function($order, $key){
                    $order->cart = unserialize($order->cart);
                    $order->adress = unserialize($order->adress);
                    $percent = $order->cart->discount / 100;
                    $discount = $order->cart->totalPrice * ($percent);

                    $order->descuento = $discount;
                    return $order;
                });
            }
        }

        // dd($ordersPending, $ordersInProgress, $ordersCompleted);
        return view('admin.dashboard.index', ['ordersPending'=>$ordersPending, 'ordersInProgress'=>$ordersInProgress, 'ordersCompleted'=>$ordersCompleted]);
    }
    public function show($order)
    {

        \Conekta\Conekta::setApiKey($this->apikey);
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('es');

        if(auth()->check()){
            $user = Auth::user();
            if ($user->isAdmin == 3) {
                $currentOrder = Order::where('no_order', $order)->first();
                $client = Client::where('user_id', $currentOrder->user_id)->first();

                if ($client->vendor_id != $user->id) {
                    return redirect()->route('admin.dashboard')->with([
                        'success' => [
                            'title' => '',
                            'body' => 'No tiene permiso de ver el pedido seleccionado.'
                        ]
                    ]);
                }
            }else if ($user->isAdmin != 1) {
                auth()->logout();
                return redirect()->route('admin.loginForm');
            }
        }

        // dd($order);
        $orders = Order::where('no_order', $order)->first();

        $orders->cart = unserialize($orders->cart);
        $orderAdress = unserialize($orders->adress);
        $orderUser = User::where('id', $orders->user_id)->first();

        $percent = $orders->cart->discount / 100;
        $discount = $orders->cart->totalPrice * ($percent);
        $orders->descuento = $discount;
        // dd($orders);

        if ($orders->conekta == 1) {
            $orderConekta = \Conekta\Order::find($orders->payment_id);
            // dd($orderConekta);
            return view('admin.dashboard.show', [
                'title' => 'Administrador',
                'orders' => $orders,
                'adress' => $orderAdress,
                'userinfo' => $orderUser,
                'reference' => $orderConekta
            ]);
        }else{
            return view('admin.dashboard.show', [
                'title' => 'Administrador',
                'orders' => $orders,
                'adress' => $orderAdress,
                'userinfo' => $orderUser
            ]);
        }
    }

    public function updateNoGuia(Request $request)
    {
        // dd($request->all());
        if ($request->get('no_guia') == '') {
            return redirect()->back()->with([
                'warning' => [
                    'title' => '',
                    'body' => 'No se pudo actualizar el número de guia. Intente de nuevo más tarde.'
                ]
            ]);
        }else{

            $validator = Validator::make($request->all(), [
                'no_guia' => 'required|numeric',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->with([
                    'warning' => [
                        'title' => '',
                        'body' => 'El número de guía debe ser numérico.'
                    ]
                ]);
            }


            $order = Order::where('no_order', $request->get('order_id'))->first();
            $order->no_guia = $request->get('no_guia');
            $order->ship_company = $request->get('ship_company');
            $order->estado = 1;
            $order->save();

            $user = User::find($order->user_id);
            $user->notify(new OrderGuiaUpdated($user, $order));


            return redirect()->route('admin.dashboard')->with([
                'success' => [
                    'title' => '',
                    'body' => 'Se ha actualizado el número de guía de el pedido.'
                ]
            ]);
            // dd($request->all());


        }
    }

    public function updateOrderToCompleted(Request $request)
    {
        // dd($request->all());
        $order = Order::where('no_order', $request->get('order_id'))->first();
        $order->estado = 2;
        $order->save();

        return redirect()->route('admin.dashboard')->with([
            'success' => [
                'title' => '',
                'body' => 'Se ha actualizado el pedido a completado.'
            ]
        ]);
    }

    public function delete(Order $order)
    {
        if(auth()->user()->isAdmin == 1){
            return view('admin.dashboard.delete')->with([
                'order' => $order
            ]);
        }else{
            return redirect()->back();
        }
    }

    public function destroy(Order $order)
    {
        $order->delete();

        return redirect()->route('admin.dashboard')->with([
            'success' => [
                'title' => '',
                'body' => 'Se eliminó el registro de forma correcta.'
            ]
        ]);
    }
}
