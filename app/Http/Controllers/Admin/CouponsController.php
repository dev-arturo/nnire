<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Coupons\CreateRequest;
use App\Http\Requests\Admin\Coupons\EditRequest;
use App\Coupon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class CouponsController extends Controller
{
    public function __construct()
    {
        View::share('section', 'coupons');
    }

    public function checkAuth(){
        if(auth()->check()){
            $user = Auth::user();
            if ($user->isAdmin != 1) {
                auth()->logout();
                return true;
            }
        }
    }

    public function index()
    {
        // dd($this->checkAuth());
        if (!$this->checkAuth()) {
            return view('admin.coupons.index')->with([
                'coupons' => Coupon::orderBy('created_at', 'ASC')->get()
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }
        
    }

    public function create()
    {
        if (!$this->checkAuth()) {
            return view('admin.coupons.create');
        }else{
            return redirect()->route('admin.loginForm');
        }
    }

    public function store(CreateRequest $request)
    {
        //dd($request->all());
        $coupon = new Coupon($request->all());
        $coupon->save();

        return redirect()->route('admin.coupons')->with([
            'success' => [
                'title' => '',
                'body' => 'Se guardó el registro de forma correcta.'
            ]
        ]);
    }

    public function edit(Coupon $coupon)
    {
        if (!$this->checkAuth()) {
            return view('admin.coupons.edit')->with([
                'coupon' => $coupon
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }
    }

    public function update(EditRequest $request, Coupon $coupon)
    {
        $coupon->update($request->all());

        return redirect()->route('admin.coupons')->with([
            'success' => [
                'title' => '',
                'body' => 'Se actualizó el registro de forma correcta.'
            ]
        ]);
    }

    public function delete(Coupon $coupon)
    {
        return view('admin.coupons.delete')->with([
            'coupon' => $coupon
        ]);
    }

    public function destroy(Coupon $coupon)
    {
        $coupon->delete();

        return redirect()->route('admin.coupons')->with([
            'success' => [
                'title' => '',
                'body' => 'Se eliminó el registro de forma correcta.'
            ]
        ]);
    }
}
