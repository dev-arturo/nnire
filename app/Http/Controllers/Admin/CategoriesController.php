<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Categories\CreateRequest;
use App\Http\Requests\Admin\Categories\EditRequest;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class CategoriesController extends Controller
{
    public function __construct()
    {
        View::share('section', 'categories');
    }

    public function checkAuth(){
        if(auth()->check()){
            $user = Auth::user();
            if ($user->isAdmin != 1) {
                auth()->logout();
                return true;
            }
        }
    }

    public function index()
    {
        if (!$this->checkAuth()) {
            return view('admin.categories.index')->with([
                'categories' => Category::orderBy('name', 'asc')->paginate(50)
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }
    }

    public function create()
    {
        if (!$this->checkAuth()) {
            return view('admin.categories.create');
        }else{
            return redirect()->route('admin.loginForm');
        }
    }

    public function store(CreateRequest $request)
    {
        $category = new Category($request->all());
        $category->save();

        return redirect()->route('admin.categories')->with([
            'success' => [
                'title' => '',
                'body' => 'Se guardó el registro de forma correcta.'
            ]
        ]);
    }

    public function edit(Category $category)
    {
        if (!$this->checkAuth()) {
            return view('admin.categories.edit')->with([
                'category' => $category
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }
    }

    public function update(EditRequest $request, Category $category)
    {
        $category->update($request->all());

        return redirect()->route('admin.categories')->with([
            'success' => [
                'title' => '',
                'body' => 'Se actualizó el registro de forma correcta.'
            ]
        ]);
    }

    public function delete(Category $category)
    {
        return view('admin.categories.delete')->with([
            'category' => $category
        ]);
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('admin.categories')->with([
            'success' => [
                'title' => '',
                'body' => 'Se eliminó el registro de forma correcta.'
            ]
        ]);
    }
}
