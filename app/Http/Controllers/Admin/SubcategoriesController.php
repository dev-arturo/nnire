<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\Admin\Subcategories\CreateRequest;
use App\Http\Requests\Admin\Subcategories\EditRequest;
use App\Subcategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class SubcategoriesController extends Controller
{
    public function __construct()
    {
        View::share('section', 'subcategories');
    }

    public function checkAuth(){
        if(auth()->check()){
            $user = Auth::user();
            if ($user->isAdmin != 1) {
                auth()->logout();
                return true;
            }
        }
    }

    public function index()
    {
        if (!$this->checkAuth()) {
            return view('admin.subcategories.index')->with([
                'subcategories' => Subcategory::orderBy('name', 'asc')->paginate(50)
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }
    }

    public function create()
    {
        if (!$this->checkAuth()) {
            return view('admin.subcategories.create')->with([
                'categories' => ['' => ''] + Category::orderBy('name', 'asc')->pluck('name', 'id')->toArray()
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }
    }

    public function store(CreateRequest $request)
    {
        $subcategory = new Subcategory($request->all());
        $subcategory->category()->associate(Category::findOrFail($request->get('category_id')));
        $subcategory->save();

        return redirect()->route('admin.subcategories')->with([
            'success' => [
                'title' => '',
                'body' => 'Se guardó el registro de forma correcta.'
            ]
        ]);
    }

    public function edit(Subcategory $subcategory)
    {
        if (!$this->checkAuth()) {
            return view('admin.subcategories.edit')->with([
                'subcategory' => $subcategory,
                'categories' => ['' => ''] + Category::orderBy('name', 'asc')->pluck('name', 'id')->toArray()
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }
    }

    public function update(EditRequest $request, Subcategory $subcategory)
    {
        $subcategory->update($request->all());
        $subcategory->category()->associate(Category::findOrFail($request->get('category_id')));
        $subcategory->save();

        return redirect()->route('admin.subcategories')->with([
            'success' => [
                'title' => '',
                'body' => 'Se actualizó el registro de forma correcta.'
            ]
        ]);
    }

    public function delete(Subcategory $subcategory)
    {
        return view('admin.subcategories.delete')->with([
            'subcategory' => $subcategory
        ]);
    }

    public function destroy(Subcategory $subcategory)
    {
        $subcategory->delete();

        return redirect()->route('admin.subcategories')->with([
            'success' => [
                'title' => '',
                'body' => 'Se eliminó el registro de forma correcta.'
            ]
        ]);
    }
}
