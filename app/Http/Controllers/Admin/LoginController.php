<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\LoginRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        if (auth()->check()) {
            return redirect()->route('admin.dashboard');
        }

        return view('admin.login.form');
    }

    public function login(LoginRequest $request)
    {
        // dd($request->all());

        if (auth()->attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
            $user = Auth::user();
            // dd($user);
            if ($user->activo == 0) {
                auth()->logout();
                return redirect()->back()->withInput($request->except('password'))->with([
                    'feedback' => 'Credenciales incorrectas.'
                ]);
            }

            if ($user->isAdmin == 0 || $user->isAdmin == 2) {
                auth()->logout();
                return redirect()->back()->withInput($request->except('password'))->with([
                    'feedback' => 'Credenciales incorrectas.'
                ]);
            }
            return redirect()->route('admin.dashboard');
        }

        return redirect()->back()->withInput($request->except('password'))->with([
            'feedback' => 'Credenciales incorrectas.'
        ]);
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->route('admin.loginForm');
    }
}
