<?php

namespace App\Http\Controllers\Admin;

use App\ClientType;
use App\Http\Requests\Admin\Clients\CreateRequest;
use App\Http\Requests\Admin\Clients\EditRequest;
use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;
use App\Adresses;
use App\Seller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

use App\Notifications\UserCreated;
use App\Notifications\OrderPayed;
use App\Notifications\OrderCreated;

class ClientsController extends Controller
{

    public $apikey = 'key_dzRyXhR3Y2pTDBqSnqxghw';
    public $payment_methodOptions = array('Pago a una sola exhibición', 'Pago en parcialidades o diferido');
    public $paymentOptions = array('(01) efectivo', '(02) cheque nominativo', '(03) transferencia electrónica de fondos', '(04) tarjeta de crédito', '(28) tarjeta de debito', '(99) otros');
    public $usage_cfdiOptions = array('G01 adquisición de mercancías' , 'G03 gastos en general', 'I03 equipo de transporte', 'P01 por definir');

    public function __construct()
    {
        View::share('section', 'clients');
    }

    public function index()
    {
        // $users = User::with('clients')->withClient()->get();
        
        // dd(User::with('clients')->WithSeller()->where('id', Auth::user()->id)->get());

        $sellers = auth()->user()->isAdmin == 1 ? User::with('clients')->WithSeller()->get() : User::with('clients')->WithSeller()->where('id', Auth::user()->id)->get();

        $sellers->transform(function($seller, $key){
            $clientsArray = array();
            foreach ($seller->clients as $key => $client) {
                $getClient = User::where('id', $client->user_id)->first();
                $getClient->creditLimit = $client->creditLimit;
                $getClient->rfc = $client->rfc;
                $getClient->curp = $client->curp;
                $getClient->payment = $client->payment;
                $getClient->payment_method = $client->payment_method;
                $getClient->usage_cfdi = $client->usage_cfdi;
                $getClient->client_type_id = $client->client_type_id;
                $getClient->adress_id = $client->adress_id;
                $getClient->payment_method = $client->payment_method;
                $getClient->client_id = $client->id;
                // dd($getClient, $client);
                array_push($clientsArray, $getClient);
            }
            $seller->allClients = $clientsArray;
            return $seller;
        });
        
        $clientsMinor = array();
        
        if(auth()->user()->isAdmin == 1){
            $clientsMinor = User::with('clients')->WithUser()->get();
            // dd($clientsMinor);
        }

        // dd($sellers);
        return view('admin.clients.index')
        ->with([
            'sellers' => $sellers,
            'minoristas' => $clientsMinor
        ]);
    }

    public function create()
    {
        return view('admin.clients.create')->with([
            'clientTypes' => ['' => ''] + ClientType::orderBy('name', 'asc')->pluck('name', 'id')->toArray(),
            'sellers' => auth()->user()->isAdmin ? ['' => ''] + User::select(DB::raw("CONCAT(name,' ',surname) as name"), 'id')->WithSeller()->orderBy('name', 'asc')->orderBy('surname', 'asc')->pluck('name', 'id')->toArray() : [],
            'payment_methodOptions' => $this->payment_methodOptions,
            'paymentOptions' => $this->paymentOptions,
            'usage_cfdiOptions' => $this->usage_cfdiOptions
        ]);
    }

    public function store(Request $request)
    {

        // dd($request->all());

        \Conekta\Conekta::setApiKey($this->apikey);
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('es');

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'surname' => 'required|string',
            'adress' => 'required|string',
            'vendor_id' => 'required',
            'postal_code' => 'required|string',
            'colony' => 'required|string',
            'city' => 'required|string',
            'state' => 'required|string',
            'phone' => 'required|numeric',
            'email' => 'required|email|unique:users',
            'rfc' => 'required|string',
            'curp' => 'required|string',
            'payment' => 'required',
            'payment_method' => 'required',
            'usage_cfdi' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if(preg_match('/^[\pL\s]+$/u', $request->get('name')) && preg_match('/^[\pL\s]+$/u', $request->get('surname'))){
            //dd($request->all(), preg_match('/^[\pL\s]+$/u', $request->get('name')));

            try {
                $customer = \Conekta\Customer::create(
                  array(
                    'name'  => $request->get('name') . ' ' . $request->get('surname'),
                    'email' => $request->get('email')
                  )
                );
        
                $activation_token = str_random(60);
        
                $user = User::create([
                    'name' => $request->get('name'),
                    'surname' => $request->get('surname'),
                    'email' => $request->get('email'),
                    'conekta_id' => $customer->id,
                    'isAdmin' => 2,
                    'password' => $request->get('password'),
                    'activation_token' => $activation_token,
                ]);
        
                $user->notify(new UserCreated($user));
        
                $customer = \Conekta\Customer::find($user->conekta_id);
                $customer->update(
                    array(
                        'shipping_contacts' => array(array(
                          'phone' => $request->get('phone'),
                          'receiver' => $request->get('name') . " " . $request->get('surname'),
                          'address' => array(
                            'street1' => $request->get('adress'),
                            'street2' => $request->get('colony'),
                            'state' => $request->get('state'),
                            'city' => $request->get('city'),
                            'country' => "MX",
                            'postal_code' => $request->get('postal_code')
                          )
                        ))
                    )
                );
        
                $newAdress = Adresses::create([
                    'user_id' => $user->id,
                    'name' => $request->get('name'),
                    'surname' => $request->get('surname'),
                    'phone' => $request->get('phone'),
                    'postal_code' => $request->get('postal_code'),
                    'state' => $request->get('state'),
                    'city' => $request->get('city'),
                    'colony' => $request->get('colony'),
                    'adress' => $request->get('adress'),
                ]);
        
                // $seller = Seller::where('user_id', $request->get('vendor_id'))->first();
        
                $client = new Client();
                $client->clientType()->associate(ClientType::findOrFail(1));
                $client->adress_id = $newAdress->id;
                $client->user_id = $user->id;
                $client->vendor_id = $request->get('vendor_id');
                $client->rfc = $request->get('rfc');
                $client->curp = $request->get('curp');
                $client->payment = $request->get('payment');
                $client->payment_method = $request->get('payment_method');
                $client->usage_cfdi = $request->get('usage_cfdi');
                $client->creditLimit = $request->get('creditLimit');
                $client->save();
        
                return redirect()->route('admin.clients')->with([
                    'success' => [
                        'title' => '',
                        'body' => 'Se guardó el registro de forma correcta.'
                    ]
                ]);
                
            } catch (\Conekta\ProcessingError $e){ 
                echo $e->getMessage();
                return redirect()->route('admin.sellers')->with([
                    'error' => [
                        'title' => '',
                        'body' => 'Ocurrió un problema desconocido. Intente de nuevo más tarde.'
                    ]
                ]);
            } catch (\Conekta\ParameterValidationError $e){
                echo $e->getMessage();
                return redirect()->route('admin.sellers')->with([
                    'error' => [
                        'title' => '',
                        'body' => 'Ocurrió un problema desconocido. Intente de nuevo más tarde.'
                    ]
                ]);
            }
        }else{
            return redirect()->back()->with([
                'warning' => [
                    'title' => '',
                    'body' => 'Los nombres sólo deben llevar letras y espacios.'
                ]
            ]);
        }
        
    }

    public function edit(Client $client)
    {
        

        if (!auth()->user()->isAdmin == 1) {
            if ($client->vendor_id != auth()->user()->id) {
                return redirect()->route('admin.clients')->with([
                    'success' => [
                        'title' => '',
                        'body' => 'No tiene permiso de editar el cliente seleccionado.'
                    ]
                ]);
            }
        }
        $client->load('seller');
        $client->load('clientType');
        $client->load('users');
        $client->load('adress');
        // dd($client);
        return view('admin.clients.edit')->with([
            'client' => $client,
            'payment_methodOptions' => $this->payment_methodOptions,
            'paymentOptions' => $this->paymentOptions,
            'usage_cfdiOptions' => $this->usage_cfdiOptions,
            'clientTypes' => ['' => ''] + ClientType::orderBy('name', 'asc')->pluck('name', 'id')->toArray(),
            'sellers' => auth()->user()->isAdmin ? ['' => ''] + User::select(DB::raw("CONCAT(name,' ',surname) as name"), 'id')->WithSeller()->orderBy('name', 'asc')->orderBy('surname', 'asc')->pluck('name', 'id')->toArray() : [],
        ]);
        
    }

    public function update(Request $request, Client $client)
    {
        if (!auth()->user()->isAdmin == 1) {
            if ($client->vendor_id != auth()->user()->id) {
                return redirect()->route('admin.clients')->with([
                    'success' => [
                        'title' => '',
                        'body' => 'No tiene permiso de editar el cliente seleccionado.'
                    ]
                ]);
            }
        }

        // dd($request->all(), $client->adress);
        if(preg_match('/^[\pL\s]+$/u', $request->get('name')) && preg_match('/^[\pL\s]+$/u', $request->get('surname'))){
            $client->rfc = $request->get('rfc');
            $client->curp = $request->get('curp');
            $client->payment = $request->get('payment');
            $client->payment_method = $request->get('payment_method');
            $client->usage_cfdi = $request->get('usage_cfdi');
            // $client->clientType()->associate(ClientType::findOrFail($request->get('client_type_id')));
            $client->creditLimit = $request->get('creditLimit');
            $client->vendor_id = $request->get('vendor_id', auth()->user()->id);
    
            $client->save();
    
            $client->adress->name = $request->get('name');
            $client->adress->surname = $request->get('surname');
            $client->adress->adress = $request->get('adress');
            $client->adress->colony = $request->get('colony');
            $client->adress->postal_code = $request->get('postal_code');
            $client->adress->city = $request->get('city');
            $client->adress->state = $request->get('state');
            $client->adress->phone = $request->get('phone');
    
            $client->adress->save();
    
            $client->users->name = $request->get('name');
            $client->users->surname = $request->get('surname');
            
            $client->users->save();
    
            return redirect()->route('admin.clients')->with([
                'success' => [
                    'title' => '',
                    'body' => 'Se actualizó el registro de forma correcta.'
                ]
            ]);
        }else{
            return redirect()->back()->with([
                'warning' => [
                    'title' => '',
                    'body' => 'Los nombres sólo deben llevar letras y espacios.'
                ]
            ]);
        }
        
    }

    public function delete(Client $client)
    {
        if (!auth()->user()->isAdmin) {
            if ($client->user_id != auth()->user()->id) {
                return redirect()->route('admin.clients')->with([
                    'success' => [
                        'title' => '',
                        'body' => 'No tiene permiso de eliminar el cliente seleccionado.'
                    ]
                ]);
            }
        }

        return view('admin.clients.delete')->with([
            'client' => $client
        ]);
    }

    public function destroy(Client $client)
    {
        if (!auth()->user()->isAdmin) {
            if ($client->vendor_id != auth()->user()->id) {
                return redirect()->route('admin.clients')->with([
                    'success' => [
                        'title' => '',
                        'body' => 'No tiene permiso de eliminar el cliente seleccionado.'
                    ]
                ]);
            }
        }

        $client->load('users');

        $client->users->activo = 0;
        $client->users->save();

        return redirect()->route('admin.clients')->with([
            'success' => [
                'title' => '',
                'body' => 'Se eliminó el registro de forma correcta.'
            ]
        ]);
    }

    public function updateStatus(Client $client)
    {
        if (!auth()->user()->isAdmin) {
            if ($client->vendor_id != auth()->user()->id) {
                return redirect()->route('admin.clients')->with([
                    'success' => [
                        'title' => '',
                        'body' => 'No tiene permiso de restaurar el cliente seleccionado.'
                    ]
                ]);
            }
        }

        $client->load('users');

        $client->users->activo = 1;
        $client->users->save();

        $client->save();

        return redirect()->route('admin.clients')->with([
            'success' => [
                'title' => '',
                'body' => 'Se restauró el registro de forma correcta.'
            ]
        ]);
    }

    public function show(Client $client)
    {
        $client->load('seller');
        $client->load('clientType');
        $client->load('users');
        $client->load('adress');
        // dd($client);
        if (auth()->user()->isAdmin != 1) {
            if ($client->vendor_id != auth()->user()->id) {
                return redirect()->route('admin.clients')->with([
                    'success' => [
                        'title' => '',
                        'body' => 'No tiene permiso de ver el cliente seleccionado.'
                    ]
                ]);
            }
        }
        return view('admin.clients.show')->with([
            'client' => $client,
            'payment_methodOptions' => $this->payment_methodOptions,
            'paymentOptions' => $this->paymentOptions,
            'usage_cfdiOptions' => $this->usage_cfdiOptions
        ]);
    }
    
    public function ver(User $user){
        
        if (auth()->user()->isAdmin != 1) {
            return redirect()->route('admin.clients')->with([
                'success' => [
                    'title' => '',
                    'body' => 'No tiene permiso de ver el cliente seleccionado.'
                ]
            ]);
        }
        $user->load('adress');
        // dd($user);
        return view('admin.clients.show')->with([
            'user' => $user
        ]);
    }
    
    public function updateStatusUser(User $user){
        if (auth()->user()->isAdmin != 1) {
            return redirect()->route('admin.clients')->with([
                'success' => [
                    'title' => '',
                    'body' => 'No tiene permiso de ver el cliente seleccionado.'
                ]
            ]);
        }

        $user->activo = 1;
        $user->save();

        return redirect()->route('admin.clients')->with([
            'success' => [
                'title' => '',
                'body' => 'Se restauró el registro de forma correcta.'
            ]
        ]);
    }
    
    public function eliminarUser(User $user)
    {
        if (auth()->user()->isAdmin != 1) {
            if ($client->vendor_id != auth()->user()->id) {
                return redirect()->route('admin.clients')->with([
                    'success' => [
                        'title' => '',
                        'body' => 'No tiene permiso de ver el cliente seleccionado.'
                    ]
                ]);
            }
        }

        return view('admin.clients.delete')->with([
            'user' => $user
        ]);
    }
    
    public function deleteUser(User $user){
        
        if (auth()->user()->isAdmin != 1) {
            if ($client->vendor_id != auth()->user()->id) {
                return redirect()->route('admin.clients')->with([
                    'success' => [
                        'title' => '',
                        'body' => 'No tiene permiso de ver el cliente seleccionado.'
                    ]
                ]);
            }
        }
        
        // dd($user);

        $user->activo = 0;
        $user->save();

        return redirect()->route('admin.clients')->with([
            'success' => [
                'title' => '',
                'body' => 'Se eliminó el registro de forma correcta.'
            ]
        ]);
    }
}
