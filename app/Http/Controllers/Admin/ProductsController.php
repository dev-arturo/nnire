<?php

namespace App\Http\Controllers\Admin;

use App\CarModel;
use App\Category;
use App\Http\Requests\Admin\Products\CreateRequest;
use App\Http\Requests\Admin\Products\EditRequest;
use App\Http\Controllers\Controller;
use App\Product;
use App\Subcategory;
use App\Year;
use App\Flag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
// use Maatwebsite\Excel\Facades\Excel;

use Excel;
use Illuminate\Support\Facades\Input;
use File;

class ProductsController extends Controller
{

    protected $chunkSize = 100;
    public $arr = array();
    public $arr1 = array();

    public function __construct()
    {
        View::share('section', 'products');
    }

    public function checkAuth(){
        if(auth()->check()){
            $user = Auth::user();
            if ($user->isAdmin != 1) {
                auth()->logout();
                return true;
            }
        }
    }

    public function index()
    {

        if (!$this->checkAuth()) {
            $prod = Product::orderBy('name', 'asc')->paginate(50);
            
            $prod->transform(function($prod, $key){
                    $prod->years = unserialize($prod->years);
                    return $prod;
                });

            return view('admin.products.index')->with([
                'inPromotion' => Product::inRandomOrder()->where('inPromotion', '!=', 0)->paginate(10),
                'products' => $prod
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }

    }

    public function addToPromo(Product $product)
    {
        // dd($product);
        $product->inPromotion = 1;
        $product->save();

        return redirect()->route('admin.products')->with([
            'success' => [
                'title' => '',
                'body' => 'El producto se ha agregado a la lista de promoción.'
            ]
        ]);
    }

    public function removeOffPromo(Product $product)
    {
        $product->inPromotion = 0;
        $product->save();

        return redirect()->route('admin.products')->with([
            'success' => [
                'title' => '',
                'body' => 'El producto se ha quitado de la lista de promoción.'
            ]
        ]);
    }

    public function create()
    {
        if (!$this->checkAuth()) {
            return view('admin.products.create')->with([
                'carModels' => ['' => ''] + CarModel::orderBy('name', 'asc')->pluck('name', 'id')->toArray(),
                'categories' => ['' => ''] + Category::orderBy('name', 'asc')->pluck('name', 'id')->toArray(),
                'subcategories' => ['' => ''],
                'years' => ['' => '']
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }
    }

    public function uploadExcel(Request $request)
    {
        set_time_limit(0);

        if($request->file('archivo-excel')){
            $path = $request->file('archivo-excel')->getRealPath();
            $extension = $request->file('archivo-excel')->getClientOriginalExtension();


            if ($extension == 'xlsx' || $extension == 'xls') {

                $data = [];

                Excel::filter('chunk')->load($path)->chunk(1000, function ($results) use (&$data) {
                    foreach ($results as $row) {
                        $val = $row->toArray();
                        dd($val);
                        foreach ($val as $key => $value) {
                            $producto = Product::where('sku', '=', $value['sku'])->first();
                            if ($producto) {
                                $producto->sku = $value['sku'];
                                $producto->model = $value['descr_planta'];
                                $producto->eanUpc = $value['intercambio_anterior'];
                                $producto->name = $value['nombre'];
                                $producto->description = $value['descripcion'];
                                $producto->price = $value['precio_pub'];
                                if ($value['mayoreo'] == '#N/A') {
                                    $producto->wholesale = 0;
                                }else{
                                    $producto->wholesale = $value['mayoreo'];
                                }
                                $producto->years = serialize(explode(",",$value['anos']));

                                $carMod = CarModel::orderBy('id', 'asc')->where('name', '=', $value['marca'])->first();
                                $producto->car_model_id = $carMod->id;

                                $subcategory = Subcategory::orderBy('id', 'asc')->where('name', '=', $value['subsistema'])->first();
                                $producto->subcategory_id = $subcategory->id;
                                $producto->save();
                                // dd("Productos SKU repetido" , $producto);
                            }else{
                                $product = new Product();
                                $product->sku = $value['sku'];
                                $product->model = $value['descr_planta'];
                                $product->eanUpc = $value['intercambio_anterior'];
                                $product->name = $value['nombre'];
                                $product->description = $value['descripcion'];
                                $product->price = $value['precio_pub'];
                                if ($value['mayoreo'] == '#N/A') {
                                    $product->wholesale = 0;
                                }else{
                                    $product->wholesale = $value['mayoreo'];
                                }
                                $product->years = serialize(explode(",",$value['anos']));

                                $carMod = CarModel::orderBy('id', 'asc')->where('name', '=', $value['marca'])->first();
                                $product->car_model_id = $carMod->id;

                                $subcategory = Subcategory::orderBy('id', 'asc')->where('name', '=', $value['subsistema'])->first();

                                $product->subcategory_id = $subcategory->id;
                                $product->save();
                            }
                        }
                    }
                }, $shouldQueue = false);

                return redirect()->route('admin.products')->with([
                    'success' => [
                        'title' => '',
                        'body' => 'Se completo la importación de forma correcta.'
                    ]
                ]);

            }
        }else{
            return redirect()->route('admin.products')->with([
                'warning' => [
                    'title' => '',
                    'body' => 'No se encontró ningún archivo excel importado. Intente de nuevo más tarde.'
                ]
            ]);
        }
    }

    public function store(CreateRequest $request)
    {
        $arr = array();

        foreach ($request->years as $key => $value) {
            // dd($value);
            array_push($arr,Year::findOrFail($value));
        }
        // dd($arr);
        $product = new Product($request->all());
        $product->carModel()->associate(CarModel::findOrFail($request->get('car_model_id')));
        $product->years = serialize($arr);
        $product->subcategory()->associate(Subcategory::findOrFail($request->get('subcategory_id')));
        $product->save();
        $product->updateImages($request->file('images'));

        return redirect()->route('admin.products')->with([
            'success' => [
                'title' => '',
                'body' => 'Se guardó el registro de forma correcta.'
            ]
        ]);
    }

    public function edit(Product $product)
    {

        if (!$this->checkAuth()) {
            $product->image = unserialize($product->image);
            return view('admin.products.edit')->with([
                'product' => $product,
                'carModels' => ['' => ''] + CarModel::orderBy('name', 'asc')->pluck('name', 'id')->toArray(),
                'categories' => ['' => ''] + Category::orderBy('name', 'asc')->pluck('name', 'id')->toArray(),
                'subcategories' => ['' => ''] + Subcategory::where('id', $product->subcategory_id)->orderBy('name', 'asc')->pluck('name', 'id')->toArray()
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }
       
    }

    public function update(EditRequest $request, Product $product)
    {

        
        $arr = array();

        $product->update($request->except('years'));
        

        if ($request->get('years')) {
            foreach ($request->years as $key => $value) {
                array_push($arr,Year::findOrFail($value));
            }
            $product->years = serialize($arr);
        }
        // dd($request->all());
        
        $product->carModel()->associate(CarModel::findOrFail($request->get('car_model_id')));
        $product->subcategory()->associate(Subcategory::findOrFail($request->get('subcategory_id')));
        $product->save();

        $newProduct = Product::where('id', $product->id)->first();
        

        $newProduct->image = unserialize($newProduct->image);

        //dd($product, $newProduct);

        $arrImages = array();
        $counter = 0;

        if ($newProduct->image) {
            foreach ($newProduct->image as $key => $value) {
                $str = 'removeImage';
                $str = $str . ($counter);
                if ($request->get($str)) {
                    array_push($arrImages, $request->get($str));
                }
                $counter++;
            }

            // dd($arrImages);
            if (count($arrImages) > 0) {
                $product->deleteImage($arrImages);
            }
        }

        if ($request->file('images')) {
            $product->updateImages($request->file('images'));
        }        

        return redirect()->route('admin.products')->with([
            'success' => [
                'title' => '',
                'body' => 'Se actualizó el registro de forma correcta.'
            ]
        ]);
    }

    public function delete(Product $product)
    {
        return view('admin.products.delete')->with([
            'product' => $product
        ]);
    }

    public function destroy(Product $product)
    {
        // dd(unserialize($product->image));
        $arrImages = unserialize($product->image);
        // dd($arrImages);
        if ($arrImages != false) {
            $product->deleteImage($arrImages);    
        }
        
        $product->delete();

        return redirect()->route('admin.products')->with([
            'success' => [
                'title' => '',
                'body' => 'Se eliminó el registro de forma correcta.'
            ]
        ]);
    }

    public function show(Product $product)
    {
        // $product->transform(function($product, $key){
        //     $product->image = unserialize($product->image);
        //     return $product;
        // });

        // dd($product);
        if (!$this->checkAuth()) {
            return view('admin.products.show')->with([
                'product' => $product
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }

    }

    public function search(Request $request)
    {
        if ($request->has('criteria')) {
            return view('admin.products.index')->with([
                'products' => Product::whereRaw('name LIKE \'%' . $request->get('criteria') . '%\' or sku LIKE \'%' . $request->get('criteria') . '%\'')->paginate(50),
                'inPromotion' => Product::inRandomOrder()->where('inPromotion', '!=', 0)->paginate(10)
            ]);
        } else{
            return view('admin.products.index')->with([
                'products' => Product::orderBy('name', 'asc')->paginate(50),
                'inPromotion' => Product::inRandomOrder()->where('inPromotion', '!=', 0)->paginate(10)
            ]);
        }
    }

    public function importByFile(Request $request)
    {
        
        ini_set("memory_limit","7G");
        ini_set('max_execution_time', '0');
        ini_set('max_input_time', '0');
        set_time_limit(0);

                Excel::filter('chunk')->selectSheetsByIndex(0)->load(public_path('products_final.xlsx'))->chunk(400, function($reader) {
                    $rows = $reader->toArray();
                    $counter = 0;

                        // dd($arreglo[356], $arreglo[357], $arreglo[358]);

                        // dd(rtrim($arreglo[324]['anos'], ','));
                    foreach ($reader->toArray() as $row) {
                        
                        // dd($row);
                        DB::beginTransaction();
                        DB::connection()->disableQueryLog();
                        try {
                            $carModel = CarModel::where('name', trim($row['marca']))->first();
                            // dd($carModel);
                            if (!$carModel) {
                                $carModel = new CarModel([
                                    'name' => trim($row['marca'])
                                ]);
                            $carModel->save();
                        }

                        $category = Category::where('name', trim($row['sistema']))->first();

                        if (!$category) {
                            $category = new Category([
                                'name' => trim($row['sistema'])
                            ]);
                            $category->save();
                        }

                        $subcategory = Subcategory::where('name', trim($row['subsistema']))->where('category_id', $category->id)->first();

                        if (!$subcategory) {
                            $subcategory = new Subcategory([
                                'name' => trim($row['subsistema'])
                            ]);
                            $subcategory->category()->associate($category);
                            $subcategory->save();
                        }

                        $product = Product::where('sku', $row['sku'])->where('name', $row['nombre'])->where('description', $row['descripcion'])->where('subcategory_id', $subcategory->id)->where('car_model_id', $carModel->id)->first();

                        if(is_numeric($row['precio_pub'])){
                            $pricepub = $row['precio_pub'];
                        }else{
                            $pricepub = 0;
                        }

                        if(is_numeric($row['mayoreo'])){
                            $mayor = $row['mayoreo'];
                        }else{
                            $mayor = 0;
                        }

                        $arr = array();


                        if ($row['anos']!='') {
                            $years = explode(",", rtrim($row['anos'], ','));
                            foreach ($years as $key => $value) {
                                $ids = Year::where('year','=', $value)->pluck('id');
                                if (count($ids)>0) {
                                    array_push($arr,Year::findOrFail($ids[0]));
                                }
                            }
                        }

                        if ($product) {

                            // dd($product);
                            $product->update([
                                'name' => $row['nombre'],
                                'model' => $row['descr_planta'],
                                'description' => $row['descripcion'],
                                'sku' => $row['sku'],
                                'eanUpc' => $row['intercambio_anterior'],
                                'price' => $pricepub,
                                'wholesale' => $mayor,
                                'years' => serialize($arr)
                            ]);
                        } else {

                            $product = new Product([
                                'name' => $row['nombre'],
                                'model' => $row['descr_planta'],
                                'description' => $row['descripcion'],
                                'sku' => $row['sku'],
                                'eanUpc' => $row['intercambio_anterior'],
                                'price' => $pricepub,
                                'wholesale' => $mayor,
                                'years' => serialize($arr)
                            ]);

                            // dd($product);
                        }

                        $product->carModel()->associate($carModel);
                        $product->subcategory()->associate($subcategory);
                        // $product->year()->associate($carModel->years()->first());
                        $product->save();

                        } catch (\Exception $error) {
                            DB::rollback();
                            throw $error;
                        }

                        DB::commit();
                        $counter++;
                    }

                    // $file = $file->fresh(); //reload from the database
                    // $file->rows_imported = $file->rows_imported + $counter;
                    // $file->save();
                });
                
            
    }
}
