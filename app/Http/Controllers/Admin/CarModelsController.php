<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CarModels\CreateRequest;
use App\Http\Requests\Admin\CarModels\EditRequest;
use App\CarModel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class CarModelsController extends Controller
{
    public function __construct()
    {
        View::share('section', 'carModels');
    }

    public function checkAuth(){
        if(auth()->check()){
            $user = Auth::user();
            if ($user->isAdmin != 1) {
                auth()->logout();
                return true;
            }
        }
    }

    public function index()
    {
        if (!$this->checkAuth()) {
            return view('admin.car-models.index')->with([
                'carModels' => CarModel::orderBy('name', 'asc')->paginate(50)
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }
    }

    public function create()
    {
        if (!$this->checkAuth()) {
            return view('admin.car-models.create');
        }else{
            return redirect()->route('admin.loginForm');
        }
    }

    public function store(CreateRequest $request)
    {
        DB::beginTransaction();

        try {
            $carModel = new CarModel($request->all());
            $carModel->save();
            $carModel->syncYears($request->get('years', []));
        } catch (\Exception $error) {
            DB::rollback();

            throw $error;
        }

        DB::commit();

        return redirect()->route('admin.carModels')->with([
            'success' => [
                'title' => '',
                'body' => 'Se guardó el registro de forma correcta.'
            ]
        ]);
    }

    public function edit(CarModel $carModel)
    {
        if (!$this->checkAuth()) {
            return view('admin.car-models.edit')->with([
                'carModel' => $carModel
            ]);
        }else{
            return redirect()->route('admin.loginForm');
        }
    }

    public function update(EditRequest $request, CarModel $carModel)
    {
        DB::beginTransaction();

        try {
            $carModel->update($request->all());
            $carModel->syncYears($request->get('years', []));
        } catch (\Exception $error) {
            DB::rollback();

            throw $error;
        }
        
        DB::commit();

        return redirect()->route('admin.carModels')->with([
            'success' => [
                'title' => '',
                'body' => 'Se actualizó el registro de forma correcta.'
            ]
        ]);
    }

    public function delete(CarModel $carModel)
    {
        return view('admin.car-models.delete')->with([
            'carModel' => $carModel
        ]);
    }

    public function destroy(CarModel $carModel)
    {
        $carModel->delete();

        return redirect()->route('admin.carModels')->with([
            'success' => [
                'title' => '',
                'body' => 'Se eliminó el registro de forma correcta.'
            ]
        ]);
    }
}
