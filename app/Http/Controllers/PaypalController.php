<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PayPal\Rest\ApiContext;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Auth\OAuthTokenCredential;
use Session;
use Auth;
use App\Cart;
use App\Order;
use App\Adresses;
use App\Coupon;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

use App\Notifications\OrderPayed;

class PaypalController extends Controller
{
    private $_api_context;
    // public $adress_id;

  public function __construct()
  {
    // setup PayPal api context
    $paypal_conf = config('paypal');
    $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
    $this->_api_context->setConfig($paypal_conf['settings']);
  }

  // show paypal form view
  public function showForm(Request $request)
  {
    // return view('pages.checkout');
  }

  public function store(Request $request)
  {

    // $this->adress_id = intval($request->get('adress_id'));
    Session::put('payer_adress_id', intval($request->get('adress_id')));
  	// dd($this->adress_id);
    // $validator = Validator::make($request->all(), [
    //     'amount' => 'required|numeric'
    // ]);
  
    // if ($validator->fails()) {
    //   return redirect('payment/add-funds/paypal')
    //     ->withErrors($validator)
    //     ->withInput();
    // }
    
    if (!Session::has('cart')) {
      Session::flash('alert', 'Something Went wrong, funds could not be loaded');
      Session::flash('alertClass', 'danger no-auto-close');
      return redirect()->route('checkout')->with('error', 'No hay productos en el carrito.');
    }

    $oldCart = Session::get('cart');
    $cart = new Cart($oldCart);
    $arr = array();

    $date = date('Y-m-d');
    $coupon = Coupon::where('code', $cart->coupon->code)->first();
    if (isset($coupon)) {
        if ($coupon->limitDate <= $date) {
            return redirect()->route('checkout')->with([
                'warning' => [
                    'title' => '',
                    'body' => 'El cupón es inválido. Intente de nuevo más tarde.'
                ]
            ]);
        }else{
            if ($coupon->uses >= $coupon->usageLimit) {
                $cart->discount = 0;
                $cart->coupon = '';
                $request->session()->put('cart', $cart);
                return redirect()->route('checkout')->with([
                    'warning' => [
                        'title' => '',
                        'body' => 'Ha ocurrido un error al realizar el pedido. Intente de nuevo más tarde.'
                    ]
                ]);
            }else{
                $coupon->uses++;
                $coupon->save();
            }
        }
    }

    // dd($cart->items);
    
    foreach ($cart->items as $product => $value) {
    // print_r($value['item']['qty']);
      $item = new Item();
      $item->setName($value['item']['name'])// item name
        ->setCurrency('MXN')
        ->setQuantity($value['qty'])
        ->setPrice($value['item']['price']); // unit price

      array_push($arr, $item);
    }
    
    if ($cart->discount != 0) {
      $percent = $cart->discount / 100;
      $discount = $cart->totalPrice * ($percent);

      $discount = -1 * abs($discount);

      $item = new Item();
      $item->setName('Descuento por cupón')// item name
        ->setCurrency('MXN')
        ->setQuantity(1)
        ->setPrice($discount);
      array_push($arr, $item);

      // dd("Discount ", $cart, $arr);
      $amount = new Amount();
      $amount->setCurrency('MXN')->setTotal(floatval($request->get('amount') + $discount));
    }else{
      $amount = new Amount();
      $amount->setCurrency('MXN')->setTotal(floatval($request->get('amount')));
    }

    // dd($arr);
    
    $payer = new Payer();
    $payer->setPaymentMethod('paypal');
  
    
    // add item to list
    $item_list = new ItemList();
    $item_list->setItems($arr);
    
    
    
    $transaction = new Transaction();
    $transaction->setAmount($amount)
      ->setItemList($item_list)
      ->setDescription('Total a pagar');

    // $transaction = new Transaction();
    // $transaction->setAmount($amount)
    //   ->setItemList($item_list)
    //   ->setDescription('Total a pagar');
    
    $redirect_urls = new RedirectUrls();
    // Specify return & cancel URL
    $redirect_urls->setReturnUrl(url('/payment/add-funds/paypal/status'))
      ->setCancelUrl(url('/payment/add-funds/paypal/status'));
  
    $payment = new Payment();
    $payment->setIntent('Sale')
      ->setPayer($payer)
      ->setRedirectUrls($redirect_urls)
      ->setTransactions(array($transaction));
  
    try {
      $payment->create($this->_api_context);
    } catch (\PayPal\Exception\PayPalConnectionException $ex) {
      // dd($ex);
      Session::flash('alert', 'Something Went wrong, funds could not be loaded');
      Session::flash('alertClass', 'danger no-auto-close');
      $coupon->uses--;
      $coupon->save();
      return redirect()->route('checkout')->with('error', 'No se pudo completar el pago. Intente más tarde.');
    }
  
    foreach ($payment->getLinks() as $link) {
      if ($link->getRel() == 'approval_url') {
        $redirect_url = $link->getHref();
        break;
      }
    }
  
    // add payment ID to session
    Session::put('paypal_payment_id', $payment->getId());
  
    if (isset($redirect_url)) {
      // redirect to paypal
      return redirect($redirect_url);
    }
  
    $coupon->uses--;
    $coupon->save();
    Session::flash('alert', 'Unknown error occurred');
    Session::flash('alertClass', 'danger no-auto-close');
    return redirect()->route('checkout')->with('error', 'No se pudo completar el pago. Intente más tarde.');
  }



  // Paypal process payment after it is done
  public function getPaymentStatus(Request $request)
  {

    $adress_id = Session::get('payer_adress_id');
    // dd($adress_id);

    $adress = Adresses::findOrFail($adress_id);


    $oldCart = Session::get('cart');
    $cart = new Cart($oldCart);
    // Get the payment ID before session clear
    $payment_id = Session::get('paypal_payment_id');

    $coupon = Coupon::where('code', $cart->coupon->code)->first();

    // clear the session payment ID
    Session::forget('paypal_payment_id');
    
    if (empty($request->input('PayerID')) || empty($request->input('token'))) {
      Session::flash('alert', 'Payment failed');
      Session::flash('alertClass', 'danger no-auto-close');
      $coupon->uses--;
      $coupon->save();
      return redirect()->route('checkout')->with('error', 'No se pudo completar el pago. Intente más tarde.');
    }
    
    $payment = Payment::get($payment_id, $this->_api_context);
    
    // PaymentExecution object includes information necessary
    // to execute a PayPal account payment.
    // The payer_id is added to the request query parameters
    // when the user is redirected from paypal back to your site
    $execution = new PaymentExecution();
    $execution->setPayerId($request->input('PayerID'));
    
    //Execute the payment
    $result = $payment->execute($execution, $this->_api_context);
    
    if ($result->getState() == 'approved') { // payment made
      // Payment is successful do your business logic here
      //dd($result); 
      
      $check = 0;
        do {
            $digits = 8;
            $randn = 'RN' . str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
            // 'RN00000001'
            $order = Order::where('no_order', $randn)->get();
            if (count($order) > 0) {
                $check = 1;
            }
        } while ($check > 0);

        // dd($order, $check, count($order), $randn);


      Session::flash('alert', 'Funds Loaded Successfully!');
      Session::flash('alertClass', 'success');
      $order = new Order();
      $order->adress = serialize($adress);
      $order->cart = serialize($cart);
      $order->payment_id = $payment_id;
      $order->no_order = $randn;
      $order->paypal = 1;
      $order->conekta = 0;
      Auth::user()->orders()->save($order);
      Session::forget('cart');
      $user = Auth::user();

      $mail = new PHPMailer(true);                              
        try {
            //Server settings
            $mail->isSMTP();
            $mail->CharSet = "utf-8";
            $mail->Host = 'mail.refaccionesnissan.com.mx';
            $mail->SMTPAuth = true;
            $mail->Username = 'info@refaccionesnissan.com.mx';
            $mail->Password = 'LFH@2oH_QwwQ';
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;

            //Recipients
            $mail->setFrom('info@refaccionesnissan.com.mx', 'Información del servidor');
            $mail->addAddress('ventasweb@refaccionesnissan.com.mx', 'Información de ventas');
            
            $mail->Subject = "Nueva orden " . $order->no_order;
            $mail->MsgHTML("Se ha generado una nueva venta con el número de orden " . $order->no_order);

            $mail->send();
            // echo 'Message has been sent';
        } catch (phpmailerException $e) {
            return redirect()->route('checkout')->with('error', $e);
        } catch (Exception $e) {
            return redirect()->route('checkout')->with('error', $e);
        }

      $user->notify(new OrderPayed($user, $order));
      return redirect()->route('home')->with('success', 'Se ha completado su pedido satisfactoriamente.');
    }
    
    
    Session::flash('alert', 'Unexpected error occurred & payment has been failed.');
    Session::flash('alertClass', 'danger no-auto-close');
    return redirect()->route('checkout')->with('error', 'No se pudo completar el pago. Intente más tarde.');
  }
}
