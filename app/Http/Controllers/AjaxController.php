<?php

namespace App\Http\Controllers;

use App\CarModel;
use App\Category;
use App\Subcategory;
use App\Adresses;
use App\User;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function subcategories(Category $category)
    {
        return view('ajax.subcategories')->with([
            'subcategories' => Subcategory::where('category_id', $category->id)->orderBy('name', 'asc')->get()
        ]);
    }

    public function years(CarModel $carModel)
    {
        return view('ajax.years')->with([
            'years' => $carModel->years()->orderBy('year', 'asc')->get()
        ]);
    }

    public function adress(User $user)
    {
        return view('ajax.adress')->with([
            'adresses' => Adresses::where('user_id', $user->id)->where('active', 1)->get()
        ]);
    }

}
