<?php

namespace App\Http\Controllers;

use App\Banner;
use App\CarModel;
use App\Category;
use App\Product;
use App\Subcategory;
use App\User;
use App\Adresses;
use App\Cart;
use App\Order;
use App\Year;
use App\Client;
use App\Coupon;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

use Dompdf\Dompdf;

use Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Notifications\UserCreated;
use App\Notifications\OrderPayed;
use App\Notifications\OrderCreated;
use App\Notifications\OrderCreation;
use App\Notifications\ResendEmailActivation;
use Illuminate\Support\Str;
use Session;

class PagesController extends Controller
{

    //public $apikey = 'key_dzRyXhR3Y2pTDBqSnqxghw';

    public function __construct()
    {
        View::share('searcherCarModels', CarModel::orderBy('name', 'asc')->get());
        View::share('searcherCategories', Category::orderBy('name', 'asc')->get());
        View::share('searcherSubcategories', \request()->get('category') ? Subcategory::orderBy('name', 'asc')->get() : []);
        View::share('searcherYears', \request()->get('carModel') ? CarModel::findOrFail(\request()->get('carModel'))->first()->years()->orderBy('year', 'asc')->get() : []);
    }

    public function index()
    {

        if(auth()->check()){
            $user = Auth::user();
            if ($user->isAdmin == 1) {
                auth()->logout();
            }
        }

        $promotionItems = Product::inRandomOrder()->where('inPromotion', '!=', 0)->get();

        $promotionItems->transform(function($promotionItems, $key){
            $promotionItems->image = unserialize($promotionItems->image);
            return $promotionItems;
        });

        return view('pages.home', [
            'title' => 'Home',
            'banners' => Banner::orderBy('order', 'asc')->get(),
            'products' => $promotionItems
        ]);
    }
    public function login()
    {
        if (auth()->check()) {
            // $user = Auth::user();
            // return response()->json(['success'=>true, 'data'=>$user]);
            return redirect()->route('my-account');
        }

        return view('pages.login', [
            'title' => 'Login'
        ]);
    }

    public function datas()
    {

        $user = auth()->user();

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $arr = array();

        // dd($cart);
        //
        $percent = $cart->discount / 100;
        $discount = $cart->totalPrice * ($percent);
        $totalWDiscount = $cart->totalPrice - $discount;

        $productCells = "";
        if ($user->isAdmin == 2 || $user->isAdmin == 3) {
            foreach ($cart->items as $key => $value) {
                // dd($value);
                $productCells .= "<tr><td class='service'>".$value['item']->name."</td><td class='desc'>".$value['item']->description."</td><td class='unit'>$ ".$value['item']->wholesale."</td><td class='qty'>".$value['qty']."</td><td class='total'>$ ".$value['price']."</td></tr>";
            }
        }else{
            foreach ($cart->items as $key => $value) {
                // dd($value);
                $productCells .= "<tr><td class='service'>".$value['item']->name."</td><td class='desc'>".$value['item']->description."</td><td class='unit'>$ ".$value['item']->wholesale."</td><td class='qty'>".$value['qty']."</td><td class='total'>$ ".$value['price']."</td></tr>";
            }
        }

        // dd($productCells);

    	$html = "<!DOCTYPE html><html lang='en'><head> <meta charset='utf-8'> <title>Example 1</title> <style>.clearfix:after{content: ''; display: table; clear: both;}a{color: #5D6975; text-decoration: underline;}body{position: relative; width: 21cm; height: 29.7cm; margin: 0 auto; color: #001028; background: #FFFFFF; font-family: Arial, sans-serif; font-size: 12px; font-family: Arial;}header{width: 90%; padding: 10px 0; margin-bottom: 30px;}#logo{text-align: center; margin-bottom: 10px;}#logo img{width: 90px;}h1{border-top: 1px solid #5D6975; border-bottom: 1px solid #5D6975; color: #5D6975; font-size: 2.4em; line-height: 1.4em; font-weight: normal; text-align: center; margin: 0 0 20px 0; background: url(dimension.png);}#project{}#project span{color: #5D6975; text-align: left; width: 52px; margin-right: 10px; display: inline-block; font-size: 0.8em;}#company{float: right; text-align: right;}#project div, #company div{white-space: nowrap;}table{width: 90%; border-collapse: collapse; border-spacing: 0; margin-bottom: 20px;}table tr:nth-child(2n-1) td{background: #F5F5F5;}table th, table td{text-align: center;}table th{padding: 5px 20px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal;}table .service, table .desc{text-align: left;}table td{padding: 20px; text-align: right;}table td.service, table td.desc{vertical-align: top;}table td.unit, table td.qty, table td.total{font-size: 1.2em;}table td.grand{border-top: 1px solid #5D6975; ;}#notices .notice{color: #5D6975; font-size: 1.2em;}footer{color: #5D6975; width: 90%; height: 30px; position: absolute; bottom: 0; border-top: 1px solid #C1CED9; padding: 8px 0; text-align: center;}</style></head><body> <header class='clearfix'> <div id='logo'> </div><h1>INVOICE 3-2-1</h1> <div id='project'> <div><span>Cliente</span> Daniel Garate</div><div><span>Dirección</span> Hacienda del Seminario, Privada San Miguel #4959, Mazatlan, Sinaloa, 82129</div><div><span>Código Postal</span> 82129</div><div><span>EMAIL</span> <a>dmadridgarate@gmail.com</a></div><div><span>DATE</span> August 17, 2015</div></div><br><h3>Realize su pago en cualquiera de las siguientes cuentas.</h3> </header> <main> <table> <thead> <tr> <th class='service'>BANCO</th> <th class='desc'>CUENTA</th> <th>SUC</th> <th> </th> <th>CLABE INTERBANCARIA</th> </tr></thead> <tbody> <tr> <td class='service'>BANCOMER</td><td class='desc'>0446893678</td><td class='unit'>723</td><td class='qty'>GDL</td><td class='total'>012320004468936781</td></tr><tr> <td class='service'>BANAMEX</td><td class='desc'>5536955</td><td class='unit'>567</td><td class='qty'>GDL</td><td class='total'>002320056755369553</td></tr><tr> <td class='service'>HSBC</td><td class='desc'>4057392698</td><td class='unit'>129</td><td class='qty'>GDL</td><td class='total'>021320040573926987</td></tr><tr> <td class='service'>BANREGIO</td><td class='desc'>131022340015</td><td class='unit'></td><td class='qty'>GDL</td><td class='total'>058320000001017986</td></tr></tbody> </table> <br><table> <thead> <tr> <th class='service'>SERVICE</th> <th class='desc'>DESCRIPTION</th> <th>PRICE</th> <th>QTY</th> <th>TOTAL</th> </tr></thead> <tbody>".$productCells."<tr> <td colspan='4'>SUBTOTAL</td><td class='total'>$ ".$cart->totalPrice."</td></tr><tr> <td colspan='4'>DESCUENTO</td><td class='total'>$ ".$discount."</td></tr><tr> <td colspan='4' class='grand total'>TOTAL</td><td class='grand total'>$ ".$totalWDiscount."</td></tr></tbody> </table> </main> <footer> </footer></body></html>";

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();
        $dompdf->stream('my.pdf',array('Attachment'=>0));
        // $output = $dompdf->output();
        // file_put_contents('Brochure.pdf', $output);

        // dd(number_format($discount , 2, '.', ','));

        dd("SEND");
        // dd($var);

        // dd($customer);

        // $customer = \Conekta\Customer::find("cus_2iAZ29AqEcGGJvCou");
        // $customer->delete();

        // $customer = \Conekta\Customer::find("cus_2iAZ2NcduUjsfsfna");
        // $customer->delete();

        // $customer = \Conekta\Customer::find("cus_2iAZ3Tx3PLoQUthxQ");
        // $customer->delete();

        // dd('Done');


        // cus_2iAZ3Tx3PLoQUthxQ
        // set_time_limit(0);

        // $dir = public_path('storage/images');
        // $dh  = opendir($dir);
        // while (false !== ($filename = readdir($dh))) {
        //     $files[] = $filename;
        // }
        // $images=preg_grep ('/\.jpg$/i', $files);

        // dd($images);

        // foreach ($images as $key => $value) {
        //     // dd($value);
        //     if (strpos($value, '-') !== false) {
        //         // dd($key, $value, 'Tiene esa verga');
        //         // dd($value, str_replace(".jpg", "", str_replace("-", "", $value)));
        //         $product = Product::where('sku', str_replace(".jpg", "", str_replace("-", "", $value)))->first();
        //         if ($product) {
        //             if ($product->image == null) {
        //                 $arrImg = array();
        //                 array_push($arrImg, $value);
        //                 $product->image = serialize($arrImg);
        //                 $product->save();
        //             }else{
        //                 $arrImg = array();
        //                 array_push($arrImg, $value);
        //                 $product->image = serialize($arrImg);
        //                 $product->save();
        //             }
        //         }
        //     }else{
        //         $product = Product::where('sku', str_replace(".jpg", "", $value))->first();
        //         if ($product) {
        //             if ($product->image == null) {

        //                 $arrImg = array();
        //                 array_push($arrImg, $value);
        //                 $product->image = serialize($arrImg);
        //                 $product->save();
        //             }else{
        //                 $arrImg = array();
        //                 array_push($arrImg, $value);
        //                 $product->image = serialize($arrImg);
        //                 $product->save();
        //             }
        //         }
        //     }
        // }
        // return redirect()->route('login');
    }

    public function addToClient(Request $request)
    {
        // dd($request->all());

        $user = User::find($request->get('clients'));

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $arr = array();

        $adress = Adresses::find($request->get('adresses'));

        $check = 0;
        do {
            $digits = 8;
            $randn = 'RN' . str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
            // 'RN00000001'
            $order = Order::where('no_order', $randn)->get();
            if (count($order) > 0) {
                $check = 1;
            }
        } while ($check > 0);

        $newOrder = new Order();
        $newOrder->adress = serialize($adress);
        $newOrder->cart = serialize($cart);
        $newOrder->payment_id = 1;
        $newOrder->no_order = $randn;
        $newOrder->conekta = 0;
        $newOrder->paypal = 0;
        $user->orders()->save($newOrder);

        $mail = new PHPMailer(true);
        try {
            //Server settings
            $mail->isSMTP();
            $mail->CharSet = "utf-8";
            $mail->Host = 'mail.refaccionesnissan.com.mx';
            $mail->SMTPAuth = true;
            $mail->Username = 'info@refaccionesnissan.com.mx';
            $mail->Password = 'LFH@2oH_QwwQ';
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;

            //Recipients
            $mail->setFrom('info@refaccionesnissan.com.mx', 'Información del servidor');
            $mail->addAddress('ventasweb@refaccionesnissan.com.mx', 'Información de ventas');

            $mail->Subject = "Nueva orden " . $newOrder->no_order;
            $mail->MsgHTML("Se ha generado una nueva venta con el número de orden " . $newOrder->no_order);

            $mail->send();
            // echo 'Message has been sent';
        } catch (phpmailerException $e) {
            return redirect()->route('checkout')->with('error', $e);
        } catch (Exception $e) {
            return redirect()->route('checkout')->with('error', $e);
        }

        Session::forget('cart');

        $user->notify(new OrderCreation($user, $newOrder));

        return redirect()->route('home')->with('success', 'El pedido se ha asignado correctamente.');



    }

    public function logout(Request $request)
    {
        Session::forget('cart');
        auth()->logout();
        return redirect()->route('login');
    }

    public function log(Request $request)
    {
      $attempt = auth()->attempt(['email' => $request->get('email'), 'password' => $request->get('password')]);
      $user = Auth::user();
      // dd($attempt, $user->activation_token);

      if ($attempt && !$user->activation_token) {

        if ($user->isAdmin == 1 || $user->activo == 0) {
          auth()->logout();
          return redirect()->back()->withInput($request->except('password'))->with([
              'feedback' => 'Credenciales incorrectas.'
          ]);
        }
        return redirect()->route('my-account');
      }else{
        if ($attempt) {
            return redirect()->route('resend');
        }
        // Auth::logout();
        // dd($user);
      }
      Auth::logout();
      return redirect()->back()->withInput($request->except('password'))->with([
          'feedback' => 'Credenciales incorrectas'
      ]);
    }

    public function register()
    {
        return view('pages.register', [
            'title' => 'Register'
        ]);
    }



    public function reg(Request $request)
    {

        // dd($request->all());
        //key_dzRyXhR3Y2pTDBqSnqxghw
        \Conekta\Conekta::setApiKey($this->apikey);
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('es');

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'g-recaptcha-response' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect('registro')
                        ->withErrors($validator)
                        ->withInput();
        }

        try {
           $customer = \Conekta\Customer::create(
              array(
                'name'  => $request->get('name') . ' ' . $request->get('last-name'),
                'email' => $request->get('email')
              )
            );

            $activation_token = str_random(60);
            $user = User::create([
                'name' => $request->get('name'),
                'surname' => $request->get('last-name'),
                'email' => $request->get('email'),
                'conekta_id' => $customer->id,
                'isAdmin' => 0,
                'password' => $request->get('password'),
                'activation_token' => $activation_token,
            ]);
            $user->notify(new UserCreated($user));
            return redirect()->route('home')->with('success', 'Se ha enviado un correo de activación necesario para su ingreso.');
        } catch (\Conekta\ProcessingError $e){
          echo $e->getMessage();
          return redirect()->back()->with('warning', 'Ocurrió un problema desconocido. Intente de nuevo más tarde.');
        } catch (\Conekta\ParameterValidationError $e){
          echo $e->getMessage();
          return redirect()->back()->with('warning', 'Ocurrió un problema desconocido. Intente de nuevo más tarde.');
        }
    }

    public function activarUsuario($token)
    {
      $user = User::where('activation_token', $token)->first();
      if ($user) {
          $user->activation_token = null;
          $user->save();
          auth()->login($user);
          return redirect()->route('my-account');
      }else{
          return redirect()->route('home')->with('warning', 'Ya se ha confirmado el usuario. Favor de iniciar sesión.');
      }
    }

    public function recover()
    {
        return view('pages.recover', [
            'title' => 'Recover Password'
        ]);
    }
    public function myAccount()
    {
        if (auth()->check()) {
            $user = Auth::user();
            if ($user->isAdmin == 1) {
                Session::forget('cart');
                auth()->logout();
                return redirect()->route('login');
            }else{
                return view('pages.my-account', [
                    'title' => 'My account'
                ]);
            }
        }
    }

    public function adresses()
    {
        if (auth()->check()) {
            $user = Auth::user();
            if ($user->isAdmin == 1) {
                Session::forget('cart');
                auth()->logout();
            }
            $direccion = Adresses::where('user_id', $user->id)
            ->where('active', 1)->get();
            // dd($direccion);
            // $phone = $direccion->phone;
            // return response()->json(['success'=>true, 'data'=>$user]);

            if (count($direccion) > 0) {
                return view('pages.adresses', [
                    'title' => 'Adresses',
                    'adresses' => $direccion,
                    'arr' => ''
                ]);
            }else{
                return view('pages.adresses', [
                    'title' => 'Adresses',
                    'adresses' => [],
                    'arr' => ''
                ]);
            }

        }

        return redirect()->route('login');

        // return view('pages.login', [
        //     'title' => 'Login'
        // ]);
    }

    public function myAdress(Request $request)
    {
        if (auth()->check()) {
            $user = Auth::user();
            if ($user->isAdmin == 1) {
                Session::forget('cart');
                auth()->logout();
            }


            $direccion = Adresses::where('id', $request->get('adress'))->where('active', 1)->first();
            // dd($request->all(), $direccion);

            return view('pages.my-adress', [
                'title' => 'Adresses',
                'adress' => $direccion
            ]);


        }

        return redirect()->route('login');
    }

    public function updateAdress(Request $request)
    {
        // dd($request->all());
        $adress = Adresses::find($request->get('id'));
        $adress->name = $request->get('name');
        $adress->surname = $request->get('surname');
        $adress->phone = $request->get('phone');
        $adress->postal_code = $request->get('postal_code');
        $adress->state = $request->get('state');
        $adress->city = $request->get('city');
        $adress->colony = $request->get('colony');
        $adress->adress = $request->get('adress');

        $adress->save();

        return redirect()->route('adresses')->with('success', 'Se ha actualizado la dirección.');
    }


    public function rmAdress(Request $request)
    {
        $adress_id = $request->get('id');
        $allAdress = Adresses::orderBy('id', 'asc')->where('user_id', auth()->user()->id)->where('active', 1)->get();

        if (count($allAdress) == 1 && auth()->user()->isAdmin == 2) {
            return response()->json(["success" => false, 'data'=>'No es posible eliminar la dirección. Intenta de nuevo más tarde.']);
        }else{
            $response = Adresses::where('id', $adress_id)->where('active', 1)->update(['active' => 0]);
        }
        // dd($response);
        // $response = Adresses::where('id', $adress_id)->where('active', 1)->update(['active' => 0]);
        // $response = Adresses::where('id', $adress_id)->where('active', 1)->get();

        if($response > 0){
            return response()->json(["success" => true, 'data'=>$allAdress]);
        }else{
            return response()->json(["success" => false]);
        }
    }

    public function createAdress(Request $request)
    {
        // dd($request);

        \Conekta\Conekta::setApiKey($this->apikey);
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('es');

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'surname' => 'required|string',
            'phone' => 'required|numeric',
            'postal_code' => 'required|numeric',
            'state' => 'required|string|max:255|',
            'city' => 'required|string|max:255|',
            'colony' => 'required|string|max:255|',
            'adress' => 'required|string|max:255|',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = Auth::user();

        $customer = \Conekta\Customer::find($user->conekta_id);
        // dd($customer);
        $customer->update(
            array(
                'shipping_contacts' => array(array(
                  'phone' => $request->get('phone'),
                  'receiver' => $request->get('name') . " " . $request->get('surname'),
                  'address' => array(
                    'street1' => $request->get('adress'),
                    'street2' => $request->get('colony'),
                    'state' => $request->get('state'),
                    'city' => $request->get('city'),
                    'country' => "MX",
                    'postal_code' => $request->get('postal_code')
                  )
                ))
            )
        );

        Adresses::create([
            'user_id' => Auth::user()->id,
            'name' => $request->get('name'),
            'surname' => $request->get('surname'),
            'phone' => $request->get('phone'),
            'postal_code' => $request->get('postal_code'),
            'state' => $request->get('state'),
            'city' => $request->get('city'),
            'colony' => $request->get('colony'),
            'adress' => $request->get('adress'),
        ]);

        return redirect()->back()->with('success', 'Se ha añadido una nueva dirección.');

    }

    public function webhooks()
    {
        $body = @file_get_contents('php://input');
        $webhook = json_decode($body);
        http_response_code(200); // Return 200 OK



        if ($webhook->type == 'charge.paid'){
            $order = Order::where('payment_id', $webhook->data->object->order_id)->first();
            // Log::info('Log message', array('data' => $order));
            $user = User::find($order->user_id);
            $user->notify(new OrderPayed($user, $order));
        }else if($webhook->type == 'order.paid'){

        }
    }

    public function products()
    {

        if (auth()->check()) {
            $user = Auth::user();
            if ($user->isAdmin == 1) {
                Session::forget('cart');
                auth()->logout();
            }
        }

        $promotionItems = Product::inRandomOrder()->where('inPromotion', '!=', 0)->get();

        $promotionItems->transform(function($promotionItems, $key){
            $promotionItems->image = unserialize($promotionItems->image);
            return $promotionItems;
        });

        return view('pages.products', [
            'title' => 'Products',
            'products' => Product::orderBy('name', 'asc')->paginate(48),
            'allProducts' => 1,
            'promotionItems' => $promotionItems
        ]);
    }
    public function product(Product $product, $slug)
    {

        if (auth()->check()) {
            $user = Auth::user();
            if ($user->isAdmin == 1) {
                auth()->logout();
            }
        }

        if (str_slug($product->name) == $slug) {
            $carModel = CarModel::where('id', $product->car_model_id)->first();

            $product->image = unserialize($product->image);

            // dd($carModel);
            return view('pages.product', [
                'title' => 'Product Name',
                'product' => $product,
                'carModel' => $carModel->name,
                'relatedProducts' => Product::inRandomOrder()->where('id', '!=', $product->id)->where('car_model_id', $product->car_model_id)->take(6)->get(),
                'quantity' => 1
            ]);
        } else {
            abort(404);
        }
    }

    public function checkout()
    {

        if (!auth()->check()) {
            return redirect()->route('login');
        }

        if (auth()->check()) {
            $user = Auth::user();
            if ($user->isAdmin == 1) {
                auth()->logout();
                return redirect()->route('login');
            }
        }

        $productos = Product::inRandomOrder()->take(6)->get();

        $productos->transform(function($productos, $key){
            $productos->image = unserialize($productos->image);
            return $productos;
        });

        $arrClients = array();

        if (auth()->user()->isAdmin == 3) {
            $clients = Client::where('vendor_id', auth()->user()->id)->get();
            $clients->load('seller');
            $clients->load('clientType');
            $clients->load('users');
            $clients->load('adress');
            foreach ($clients as $key => $value) {
                array_push($arrClients, $value->users);
            }
            // dd($arrClients);
        }

        if (!Session::has('cart')) {
            return view('pages.checkout', [
                'title' => 'Checkout',
                'totalPrice' => 0,
                'clients' => $arrClients,
                'discount' => 0,
                'relatedProducts' => $productos
            ]);
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);

        $updatedCart = null;
        $newCart = new Cart([$updatedCart]);

        // dd($cart->items);

        foreach ($cart->items as $key => $value) {
            // dd($value['qty']);
            for ($i=0; $i < $value['qty']; $i++) {
                $prod = Product::where('id', $value['item']->id)->first();
                $newCart->add($prod, $prod->id);
                // dd($prod, $newCart);
            }
        }
        // dd($newCart, $cart);
        $percent = $newCart->discount / 100;
        $discount = $newCart->totalPrice * ($percent);

        Session::forget('cart');
        Session::put('cart', $newCart);

        return view('pages.checkout', [
            'title'=>'Checkout',
            'products' => $newCart->items,
            'clients' => $arrClients,
            'totalPrice' => $newCart->totalPrice,
            'discount' => $discount,
            'relatedProducts' => $productos
        ]);
    }

    public function summary(Request $request)
    {
        // dd($request->all());
        if (!auth()->check()) {
            return redirect()->route('login');
        }

        if (auth()->check()) {
            $user = Auth::user();
            if ($user->isAdmin == 1) {
                auth()->logout();
                return redirect()->route('login');
            }
        }

        if (!Session::has('cart')) {
            return redirect()->route('checkout');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $adresses = Adresses::where('user_id', auth()->user()->id)->get();

        $percent = $cart->discount / 100;
        $discount = $cart->totalPrice * ($percent);

        // dd($productos);
        return view('pages.client-pay-sumary', [
            'title'=>'Detalles del pedido',
            'products' => $cart->items,
            'adresses' => $adresses,
            'discount' => $discount,
            'totalPrice' => $cart->totalPrice
        ]);
    }

    public function confirmOrder(Request $request){

        $date = date('Y-m-d');
        $user = auth()->user();

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $arr = array();
        $adress = Adresses::find($request->get('adress'));

        // dd($cart->coupon->code);
        $url = url('/compras');


        if($cart->coupon != ''){
            $coupon = Coupon::where('code', $cart->coupon->code)->first();
            if (isset($coupon)) {
                if ($coupon->limitDate <= $date) {
                    return redirect()->route('checkout')->with([
                        'warning' => [
                            'title' => '',
                            'body' => 'El cupón es inválido. Intente de nuevo más tarde.'
                        ]
                    ]);
                }else{
                    if ($coupon->uses >= $coupon->usageLimit) {
                        $cart->discount = 0;
                        $cart->coupon = '';
                        $request->session()->put('cart', $cart);
                        return redirect()->route('checkout')->with([
                            'warning' => [
                                'title' => '',
                                'body' => 'Ha ocurrido un error al realizar el pedido. Intente de nuevo más tarde.'
                            ]
                        ]);
                    }else{
                        $coupon->uses++;
                        $coupon->save();
                    }
                }
            }
        }


        $check = 0;
        do {
            $digits = 8;
            $randn = 'RN' . str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
            // 'RN00000001'
            $order = Order::where('no_order', $randn)->get();
            if (count($order) > 0) {
                $check = 1;
            }
        } while ($check > 0);

        // dd("ORDER CREATED");

        $newOrder = new Order();
        $newOrder->adress = serialize($adress);
        $newOrder->cart = serialize($cart);
        $newOrder->payment_id = 2;
        $newOrder->no_order = $randn;
        $newOrder->conekta = 0;
        $newOrder->paypal = 0;
        $user->orders()->save($newOrder);

        $mail = new PHPMailer(true);
        try {
            //Server settings
            $mail->isSMTP();
            $mail->CharSet = "utf-8";
            $mail->Host = 'mail.refaccionesnissan.com.mx';
            $mail->SMTPAuth = true;
            $mail->Username = 'info@refaccionesnissan.com.mx';
            $mail->Password = 'LFH@2oH_QwwQ';
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;

            //Recipients
            $mail->setFrom('info@refaccionesnissan.com.mx', 'Información de ventas');
            $mail->addAddress('ventasweb@refaccionesnissan.com.mx', 'Información de ventas');

            $mail->Subject = "Nueva orden " . $newOrder->no_order;
            $mail->MsgHTML("Se ha generado una nueva venta con el número de orden " . $newOrder->no_order);

            $mail->send();
            // echo 'Message has been sent';
        } catch (phpmailerException $e) {
            return redirect()->route('checkout')->with('error', $e);
        } catch (Exception $e) {
            return redirect()->route('checkout')->with('error', $e);
        }


        $mail = new PHPMailer(true);
        try {
            //Server settings
            $mail->isSMTP();
            $mail->CharSet = "utf-8";
            $mail->Host = 'mail.refaccionesnissan.com.mx';
            $mail->SMTPAuth = true;
            $mail->Username = 'info@refaccionesnissan.com.mx';
            $mail->Password = 'LFH@2oH_QwwQ';
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;
            $mail->IsHTML(true);

            //Recipients
            $mail->setFrom('info@refaccionesnissan.com.mx', 'Información de ventas');
            $mail->addAddress($user->email, 'Información de ventas');

            $mail->Subject = "Nuevo pedido";

            $productCells = "";
            if ($user->isAdmin == 2 || $user->isAdmin == 3) {
                foreach ($cart->items as $key => $value) {
                    // dd($value);
                    $productCells .= "<tr><td><b>".$value['item']->name."</b> x ".$value['qty']."</td><td>$ ".$value['item']->wholesale."</td></tr>";
                }
            }else{
                foreach ($cart->items as $key => $value) {
                    // dd($value);
                    $productCells .= "<tr><td><b>".$value['item']->name."</b> x ".$value['qty']."</td><td>$ ".$value['item']->price."</td></tr>";
                }
            }

            $adressFactura = "<tr><td>".$adress->name . " " .$adress->surname."</td></tr><tr><td>".$adress->colony."</td></tr><tr><td>".$adress->adress."</td></tr><tr><td>".$adress->city."</td></tr><tr><td>".$adress->state."</td></tr><tr><td>".$adress->postal_code."</td></tr><tr><td>".$adress->phone."</td></tr><tr><td>".$user->email."</td></tr>";

            $adressEnvio = "<tr><td>".$adress->name . " " .$adress->surname."</td></tr><tr><td>".$adress->colony."</td></tr><tr><td>".$adress->adress."</td></tr><tr><td>".$adress->city."</td></tr><tr><td>".$adress->state."</td></tr><tr><td>".$adress->postal_code."</td></tr>";

            $percent = $cart->discount / 100;
            $discount = $cart->totalPrice * ($percent);
            $totalWDiscount = $cart->totalPrice - $discount;
            // $productCells = ;

            $message = '<html><body style="text-decoration: none;color: black; margin: 0; padding: 0; font-family: "Roboto", sans-serif;"><link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td> <table align="center" border="" cellpadding="0" cellspacing="0" width="1000px"> <tr> <td align="center" bgcolor="#2E2E2E" style="padding: 40px 0 30px 0;"><img src="http://store.refaccionesnissan.com.mx/images/nissancom.png" alt="Creating Email Magic" width="194" height="78" style="display: block;"/> </td></tr><tr> <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;"><table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr><td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px; text-align: center; padding: 10px;"><b>Nueva orden ' . $newOrder->no_order . '</b></td></tr><tr><td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px; text-align: center; padding: 10px;"><b>Hemos recibido tu nuevo pedido.</b></td></tr><tr><td style="color: #153643; font-family: Arial, sans-serif; font-size: 16px; text-align: center; padding: 10px;"><table border="0" cellpadding="0" style="border-collapse: collapse; width: 100%; padding: 10px;"><tr><td>Número de pedido:</td><td>Fecha:</td><td>Email:</td><td>Total:</td><td>Método de pago:</td></tr><tr><td>'.$newOrder->no_order.'</td><td>'.$newOrder->created_at.'</td><td>'.$user->email.'</td><td>$ '.number_format($cart->totalPrice , 2, '.', ',').'</td><td>Transferencia bancaria directa</td></tr></table></td></tr><tr><td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px; text-align: center; padding: 10px;"></td></tr><tr> <td align="center"> <b>Realiza tu pago a cualquiera de las siguientes cuentas:</b> </td></tr><tr> <td style="padding: 20px 0 30px 0;"> <table style="border-collapse: collapse; width: 100%; padding: 10px; text-align: center;"> <tr> <th>BANCO</th> <th>CUENTA</th> <th>SUC</th> <th></th> <th>CLABE INTERBANCARIA</th> </tr><tr> <td>BANCOMER</td><td>0446893678</td><td>723</td><td>GDL</td><td>012320004468936781</td></tr><tr> <td>BANAMEX</td><td>5536955</td><td>567</td><td>GDL</td><td>002320056755369553</td></tr><tr> <td>HSBC</td><td>4057392698</td><td>129</td><td>GDL</td><td>021320040573926987</td></tr><tr> <td>BANREGIO</td><td>131022340015</td><td></td><td>GDL</td><td>058320000001017986</td></tr></table> </td></tr><tr> <td> <b>Detalles del pedido</b> </td></tr><tr> <td><hr></td></tr><tr> <td style="padding: 20px 0 30px 0;"> <table style="border-collapse: collapse; width: 100%; padding: 10px;"> <tr> <td>Productos</td><td>Total</td></tr>'.$productCells.'<tr> <td>Subtotal:</td><td>$ '.number_format($cart->totalPrice , 2, '.', ',').'</td></tr><tr> <td>Envio:</td><td>$ 0.00</td></tr><tr> <td>Descuento:</td><td>$ '.number_format($discount , 2, '.', ',').'</td></tr><tr> <td>Método de pago:</td><td>Transferencia bancaria directa</td></tr><tr> <td>Total:</td><td>$ '.number_format($totalWDiscount , 2, '.', ',').'</td></tr></table> </td></tr><tr> <td> <hr> </td></tr><tr> <td> <table style="border-collapse: collapse; width: 100%; padding: 10px;"> <tr> <td><b>Dirección de facturación</b></td><td><b>Dirección de envío</b></td></tr><tr> <td> <table>'.$adressFactura.'</table> </td><td> <table>'.$adressEnvio.'</table> </td></tr></table> </td></tr></table></td></tr><tr> <td bgcolor="#EE2631" style="padding: 30px 30px 30px 30px;"> <table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr> <td> <td width="75%" style="color: #FFF;"> &reg; 2018 © Nissan Refacciones<br/></td></td><td> <td align="right"> <table border="0" cellpadding="0" cellspacing="0"> <tr> <td> <a href="https://www.facebook.com/Refacciones-Nissan-1961708180730803/"> <img src="http://store.refaccionesnissan.com.mx/images/facebook.png" alt="Facebook" width="38" height="38" style="display: block;" border="0"/> </a> </td><td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td></tr></table></td></td></tr></table></td></tr></table> </td></tr></table></body></html>';

            $mail->Body = $message;

            $productCells = "";
            if ($user->isAdmin == 2 || $user->isAdmin == 3) {
                foreach ($cart->items as $key => $value) {
                    // dd($value);
                    $productCells .= "<tr><td class='service'>".$value['item']->name."</td><td class='desc'>".$value['item']->description."</td><td class='unit'>$ ".$value['item']->wholesale."</td><td class='qty'>".$value['qty']."</td><td class='total'>$ ".$value['price']."</td></tr>";
                }
            }else{
                foreach ($cart->items as $key => $value) {
                    // dd($value);
                    $productCells .= "<tr><td class='service'>".$value['item']->name."</td><td class='desc'>".$value['item']->description."</td><td class='unit'>$ ".$value['item']->wholesale."</td><td class='qty'>".$value['qty']."</td><td class='total'>$ ".$value['price']."</td></tr>";
                }
            }

            // dd($productCells);

            $html = "<!DOCTYPE html><html lang='en'><head> <meta charset='utf-8'> <title>Example 1</title> <style>.clearfix:after{content: ''; display: table; clear: both;}a{color: #5D6975; text-decoration: underline;}body{position: relative; width: 21cm; height: 29.7cm; margin: 0 auto; color: #001028; background: #FFFFFF; font-family: Arial, sans-serif; font-size: 12px; font-family: Arial;}header{width: 90%; padding: 10px 0; margin-bottom: 30px;}#logo{text-align: center; margin-bottom: 10px;}#logo img{width: 90px;}h1{border-top: 1px solid #5D6975; border-bottom: 1px solid #5D6975; color: #5D6975; font-size: 2.4em; line-height: 1.4em; font-weight: normal; text-align: center; margin: 0 0 20px 0; background: url(dimension.png);}#project{}#project span{color: #5D6975; text-align: left; width: 52px; margin-right: 10px; display: inline-block; font-size: 0.8em;}#company{float: right; text-align: right;}#project div, #company div{white-space: nowrap;}table{width: 90%; border-collapse: collapse; border-spacing: 0; margin-bottom: 20px;}table tr:nth-child(2n-1) td{background: #F5F5F5;}table th, table td{text-align: center;}table th{padding: 5px 20px; color: #5D6975; border-bottom: 1px solid #C1CED9; white-space: nowrap; font-weight: normal;}table .service, table .desc{text-align: left;}table td{padding: 20px; text-align: right;}table td.service, table td.desc{vertical-align: top;}table td.unit, table td.qty, table td.total{font-size: 1.2em;}table td.grand{border-top: 1px solid #5D6975; ;}#notices .notice{color: #5D6975; font-size: 1.2em;}footer{color: #5D6975; width: 90%; height: 30px; position: absolute; bottom: 0; border-top: 1px solid #C1CED9; padding: 8px 0; text-align: center;}</style></head><body> <header class='clearfix'> <div id='logo'> </div><h1>".$newOrder->no_order."</h1> <div id='project'> <div><span>Cliente</span> ".$adress->name." ".$adress->surname."</div><div><span>Dirección</span> ".$adress->colony.", ".$adress->adress.", ".$adress->city.", ".$adress->state.", ".$adress->postal_code."</div><div><span>EMAIL</span> <a>".$user->email."</a></div><div><span>DATE</span> ".$newOrder->created_at."</div></div><br><h3>Realize su pago en cualquiera de las siguientes cuentas.</h3> </header> <main> <table> <thead> <tr> <th class='service'>BANCO</th> <th class='desc'>CUENTA</th> <th>SUC</th> <th> </th> <th>CLABE INTERBANCARIA</th> </tr></thead> <tbody> <tr> <td class='service'>BANCOMER</td><td class='desc'>0446893678</td><td class='unit'>723</td><td class='qty'>GDL</td><td class='total'>012320004468936781</td></tr><tr> <td class='service'>BANAMEX</td><td class='desc'>5536955</td><td class='unit'>567</td><td class='qty'>GDL</td><td class='total'>002320056755369553</td></tr><tr> <td class='service'>HSBC</td><td class='desc'>4057392698</td><td class='unit'>129</td><td class='qty'>GDL</td><td class='total'>021320040573926987</td></tr><tr> <td class='service'>BANREGIO</td><td class='desc'>131022340015</td><td class='unit'></td><td class='qty'>GDL</td><td class='total'>058320000001017986</td></tr></tbody> </table> <br><table> <thead> <tr> <th class='service'>SERVICE</th> <th class='desc'>DESCRIPTION</th> <th>PRICE</th> <th>QTY</th> <th>TOTAL</th> </tr></thead> <tbody>".$productCells."<tr> <td colspan='4'>SUBTOTAL</td><td class='total'>$ ".$cart->totalPrice."</td></tr><tr> <td colspan='4'>DESCUENTO</td><td class='total'>$ ".$discount."</td></tr><tr> <td colspan='4' class='grand total'>TOTAL</td><td class='grand total'>$ ".$totalWDiscount."</td></tr></tbody> </table> </main> <footer> </footer></body></html>";

            $dompdf = new DOMPDF();
            $dompdf->load_html($html);
            $dompdf->render();
            $output = $dompdf->output();

            file_put_contents(base_path() ."/storage/app/". $newOrder->no_order.".pdf", $output);
            // $dompdf->stream('my.pdf',array('Attachment'=>0));
            $dir = base_path() ."/storage/app/". $newOrder->no_order.".pdf";
            // $mail->MsgHTML("Se ha generado una nueva venta con el número de orden ");
            $mail->addAttachment($dir);
            $mail->send();

            Session::forget('cart');
        } catch (phpmailerException $e) {
            return redirect()->route('checkout')->with('error', $e);
        } catch (Exception $e) {
            return redirect()->route('checkout')->with('error', $e);
        }

        return redirect()->route('home')->with('success', 'El pedido se ha creado correctamente.');
    }

    public function addToCart(Request $request)
    {
        // dd($request);
        if(auth()->check()){
            if ($request->get('quantity') > 0) {
                $product = Product::find($request->get('id'));
                $product->image = unserialize($product->image);
                // dd($product);

                $oldCart = Session::has('cart') ? Session::get('cart') : null;
                $cart = new Cart($oldCart);
                for ($i=0; $i < $request->get('quantity'); $i++) {
                    $cart->add($product, $product->id);
                    $request->session()->put('cart', $cart);
                }
                // dd($request->session()->get('cart'));
                return redirect()->route('checkout');
            }else{
                return redirect()->back()->with('error', 'Debe seleccionar al menos 1 producto.');
            }
        }else{
            return redirect()->route('login');
        }

    }

    public function changeQtyOnCart(Request $request)
    {
        // return response()->json(["success" => true, 'data'=>$request->all()]);

        if ($request->get('quantity') > 0) {
            $product = Product::find($request->get('id'));
            $oldCart = Session::has('cart') ? Session::get('cart') : null;
            $cart = new Cart($oldCart);

            // return response()->json(["success" => true, 'data'=>$cart->items[$product->id]['qty'] ]);

            if ($request->get('quantity') > $cart->items[$product->id]['qty']) {
                $newQty = $request->get('quantity') - $cart->items[$product->id]['qty'];
                for ($i=0; $i < $newQty; $i++) {
                    $cart->add($product, $product->id);
                }
                $request->session()->put('cart', $cart);
            }else if ($request->get('quantity') < $cart->items[$product->id]['qty']) {
                $newQty = $cart->items[$product->id]['qty'] - $request->get('quantity');
                for ($i=0; $i < $newQty; $i++) {
                    $cart->reduceBy($product, $product->id);
                }
                $request->session()->put('cart', $cart);
            }

            // dd($request->session()->get('cart'));
            return redirect()->route('checkout')->with('sucess', 'Se ha actualizado el carrito de compra.');;
        }else{
            return redirect()->route('checkout')->with('error', 'No se pudo actualizar. Intenta de nuevo más tarde.');;
        }
    }

    public function removeItem($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        Session::put('cart', $cart);

        $newCart = Session::get('cart');
        if($newCart->totalQty == 0){
            Session::forget('cart');
        }
        return redirect()->route('checkout');
    }

    public function pay()
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        // dd($cart);
        if (auth()->check()) {
            $user = Auth::user();
            if ($user->isAdmin == 1) {
                auth()->logout();
                return redirect()->route('login');
            }

            if($cart->totalQty == 0){
                return redirect()->route('checkout')->with('error', 'No hay productos en el carrito de compra.');
            }else{
                $adresses = Adresses::where('user_id', $user->id)->where('active', 1)->get();
                return view('pages.pay', [
                    'title' => 'Pay',
                    'adresses' => $adresses
                ]);
            }

        }


    }

    public function paycheck(Request $request)
    {
        if (auth()->check()) {
            $user = Auth::user();
            if ($user->isAdmin == 1) {
                auth()->logout();
                return redirect()->route('login');
            }

            if($request->get('adress')){
                $adress = $request->get('adress');
                $oldCart = Session::get('cart');
                $cart = new Cart($oldCart);
                // dd($cart);
                return view('pages.paycheck', [
                    'title'=>'Pay',
                    // 'products' => $cart->items,
                    'totalPrice' => $cart->totalPrice,
                    'adress' => $request->adress
                ]);
            }else{
                return redirect()->route('login');
            }

        }


    }

    public function payConekta(Request $request)
    {

        // dd($request->all());

        \Conekta\Conekta::setApiKey($this->apikey);
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('es');
        //cus_2i2NFz73AY241pN4F

        $user = Auth::user();
        $customer = \Conekta\Customer::find($user->conekta_id);

        $adress = Adresses::findOrFail($request->get('adress_id'));

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $arr = array();
        $date = date('Y-m-d');

        if ($cart->coupon != '') {
            $coupon = Coupon::where('code', $cart->coupon->code)->first();
            if (isset($coupon)) {
                if ($coupon->limitDate <= $date) {
                    return redirect()->route('checkout')->with([
                        'warning' => [
                            'title' => '',
                            'body' => 'El cupón es inválido. Intente de nuevo más tarde.'
                        ]
                    ]);
                }elseif ($coupon->uses >= $coupon->usageLimit) {
                    return redirect()->route('checkout')->with([
                        'warning' => [
                            'title' => '',
                            'body' => 'El cupón es inválido. Intente de nuevo más tarde.'
                        ]
                    ]);
                }
            }
        }

        $percent = $cart->discount / 100;
        $discount = $cart->totalPrice * ($percent);
        $discount = intval($discount*100);

        foreach ($cart->items as $key => $value) {
            // dd($value['item']->name);
            array_push($arr, array(
                "description" => $value['item']->description,
                "name" => $value['item']->name,
                "quantity" => intval($value['qty']),
                "unit_price" => intval(strval($value['item']->price*100))
            ));
        }

        try{

            if ($cart->coupon != '') {
                $order = \Conekta\Order::create(
                    array(
                        "line_items" => $arr,
                        "shipping_lines" => array(
                            array(
                                "amount" => 0,
                                "carrier" => "FEDEX"
                            )
                        ),
                        "customer_info" => array(
                            "customer_id" => $user->conekta_id
                        ),
                        "discount_lines" => array(
                            array(
                                'code'   => 'Cupón de descuento',
                                'amount' => $discount,
                                'type'   => 'coupon'
                            ),
                        ),
                        "shipping_contact" => array(
                            'address' => array(
                                "country" => "MX",
                                "city" => $adress->city,
                                "state" => $adress->state,
                                "street1" => $adress->colony,
                                "street2" => $adress->adress,
                                "postal_code" => "".$adress->postal_code
                            )
                        ),
                        "currency" => "mxn",
                        "charges" => array(
                            array(
                                "payment_method" => array(
                                    "type" => "card",
                                    "token_id" => $request->get('conektaTokenId')
                                )
                            )
                        )
                    )
                  );
            }else{
                $order = \Conekta\Order::create(
                    array(
                        "line_items" => $arr,
                        "shipping_lines" => array(
                            array(
                                "amount" => 0,
                                "carrier" => "FEDEX"
                            )
                        ),
                        "customer_info" => array(
                            "customer_id" => $user->conekta_id
                        ),
                        "shipping_contact" => array(
                            'address' => array(
                                "country" => "MX",
                                "city" => $adress->city,
                                "state" => $adress->state,
                                "street1" => $adress->colony,
                                "street2" => $adress->adress,
                                "postal_code" => "".$adress->postal_code
                            )
                        ),
                        "currency" => "mxn",
                        "charges" => array(
                            array(
                                "payment_method" => array(
                                    "type" => "card",
                                    "token_id" => $request->get('conektaTokenId')
                                )
                            )
                        )
                    )
                  );
            }

        } catch (\Conekta\ProcessingError $error){
            Session::flash('alert', 'Unexpected error occurred & payment has been failed.');
            Session::flash('alertClass', 'danger no-auto-close');
            return redirect()->route('checkout')->with('error', $error->getMesage());
        } catch (\Conekta\ParameterValidationError $error){
          echo $error->getMessage();
        } catch (\Conekta\Handler $error){
          echo $error->getMessage();
        }

        if (isset($order)) {

            $order_id = $order->_values['id'];

            $check = 0;
            do {
                $digits = 8;
                $randn = 'RN' . str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
                // 'RN00000001'
                $order = Order::where('no_order', $randn)->get();
                if (count($order) > 0) {
                    $check = 1;
                }
            } while ($check > 0);

            $newOrder = new Order();
            $newOrder->adress = serialize($adress);
            $newOrder->cart = serialize($cart);
            $newOrder->payment_id = $order_id;
            $newOrder->no_order = $randn;
            $newOrder->conekta = 1;
            $newOrder->paypal = 0;
            Auth::user()->orders()->save($newOrder);

            if ($cart->coupon != '') {
                $coupon->uses++;
                $coupon->save();
            }

            $mail = new PHPMailer(true);
            try {
                //Server settings
                $mail->isSMTP();
                $mail->CharSet = "utf-8";
                $mail->Host = 'mail.refaccionesnissan.com.mx';
                $mail->SMTPAuth = true;
                $mail->Username = 'info@refaccionesnissan.com.mx';
                $mail->Password = 'LFH@2oH_QwwQ';
                $mail->SMTPSecure = 'ssl';
                $mail->Port = 465;

                //Recipients
                $mail->setFrom('info@refaccionesnissan.com.mx', 'Información del servidor');
                $mail->addAddress('ventasweb@refaccionesnissan.com.mx', 'Información de ventas');

                $mail->Subject = "Nueva orden " . $newOrder->no_order;
                $mail->MsgHTML("Se ha generado una nueva venta con el número de orden " . $newOrder->no_order);

                $mail->send();
                // echo 'Message has been sent';
            } catch (phpmailerException $e) {
                return redirect()->route('checkout')->with('error', $e);
            } catch (Exception $e) {
                return redirect()->route('checkout')->with('error', $e);
            }

            Session::forget('cart');

            $user->notify(new OrderPayed($user, $newOrder));

            return redirect()->route('home')->with('success', 'El pago se ha comprabado correctamente. Para más detalles revise su correo electrónico.');
        }else{
            return redirect()->route('checkout')->with('error', $error->getMessage());
        }

    }

    public function payOxxoConekta(Request $request)
    {

        \Conekta\Conekta::setApiKey($this->apikey);
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('es');

        // dd($request->all());

        $user = Auth::user();
        $customer = \Conekta\Customer::find($user->conekta_id);

        $adress = Adresses::findOrFail($request->get('adress_id'));

        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $arr = array();
        $date = date('Y-m-d');

        if ($cart->coupon != '') {
            $coupon = Coupon::where('code', $cart->coupon->code)->first();
            if (isset($coupon)) {
                if ($coupon->limitDate <= $date) {
                    return redirect()->route('checkout')->with([
                        'warning' => [
                            'title' => '',
                            'body' => 'El cupón es inválido. Intente de nuevo más tarde.'
                        ]
                    ]);
                }elseif ($coupon->uses >= $coupon->usageLimit) {
                    return redirect()->route('checkout')->with([
                        'warning' => [
                            'title' => '',
                            'body' => 'El cupón es inválido. Intente de nuevo más tarde.'
                        ]
                    ]);
                }
            }
        }


        $percent = $cart->discount / 100;
        $discount = $cart->totalPrice * ($percent);
        $discount = intval($discount*100);

        foreach ($cart->items as $key => $value) {
            // dd($value['item']->name);
            array_push($arr, array(
                "description" => $value['item']->description,
                "name" => $value['item']->name,
                "quantity" => intval($value['qty']),
                "unit_price" => intval(strval($value['item']->price*100))
            ));
        }



        try{

            if ($cart->coupon != '') {
                $order = \Conekta\Order::create(
                    array(
                        "line_items" => $arr, //line_items
                        "shipping_lines" => array(
                            array(
                                "amount" => 0,
                                "carrier" => "FEDEX"
                            )
                    ),
                    "currency" => "MXN",
                    "customer_info" => array(
                        "customer_id" => $user->conekta_id
                    ),
                    "discount_lines" => array(
                        array(
                            'code'   => 'Cupón de descuento',
                            'amount' => $discount,
                            'type'   => 'coupon'
                        ),
                    ),
                    "shipping_contact" => array(
                        'address' => array(
                            "country" => "MX",
                            "city" => $adress->city,
                            "state" => $adress->state,
                            "street1" => $adress->colony,
                            "street2" => $adress->adress,
                            "postal_code" => "".$adress->postal_code
                        )
                    ),
                    "charges" => array(
                        array(
                            "payment_method" => array(
                            "type" => "oxxo_cash"
                        )
                    )
                    )
                    )
                );
            }else{
                $order = \Conekta\Order::create(
                    array(
                      "line_items" => $arr, //line_items
                      "shipping_lines" => array(
                        array(
                          "amount" => 0,
                          "carrier" => "FEDEX"
                        )
                      ), //shipping_lines - physical goods only
                      "currency" => "MXN",
                      "customer_info" => array(
                        "customer_id" => $user->conekta_id
                    ),
                    "shipping_contact" => array(
                        'address' => array(
                            "country" => "MX",
                            "city" => $adress->city,
                            "state" => $adress->state,
                            "street1" => $adress->colony,
                            "street2" => $adress->adress,
                            "postal_code" => "".$adress->postal_code
                        )
                    ),
                    "charges" => array(
                      array(
                          "payment_method" => array(
                            "type" => "oxxo_cash"
                          )
                      )
                    )
                )
              );
            }


              if (isset($order)) {
                $order_id = $order->_values['id'];

                $check = 0;
                do {
                    $digits = 8;
                    $randn = 'RN' . str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
                    // 'RN00000001'
                    $order = Order::where('no_order', $randn)->get();
                    if (count($order) > 0) {
                        $check = 1;
                    }
                } while ($check > 0);

                $newOrder = new Order();
                $newOrder->adress = serialize($adress);
                $newOrder->cart = serialize($cart);
                $newOrder->payment_id = $order_id;
                $newOrder->no_order = $randn;
                $newOrder->conekta = 1;
                $newOrder->paypal = 0;
                Auth::user()->orders()->save($newOrder);

                if ($cart->coupon != '') {
                    $coupon->uses++;
                    $coupon->save();
                }

                $mail = new PHPMailer(true);
                try {
                    //Server settings
                    $mail->isSMTP();
                    $mail->CharSet = "utf-8";
                    $mail->Host = 'mail.refaccionesnissan.com.mx';
                    $mail->SMTPAuth = true;
                    $mail->Username = 'info@refaccionesnissan.com.mx';
                    $mail->Password = 'LFH@2oH_QwwQ';
                    $mail->SMTPSecure = 'ssl';
                    $mail->Port = 465;
                    $mail->setFrom('info@refaccionesnissan.com.mx', 'Información del servidor');
                    $mail->addAddress('ventasweb@refaccionesnissan.com.mx', 'Información de ventas');
                    $mail->Subject = "Nueva orden " . $newOrder->no_order;
                    $mail->MsgHTML("Se ha generado una nueva venta con el número de orden " . $newOrder->no_order);
                    $mail->send();
                } catch (phpmailerException $e) {
                    return redirect()->route('checkout')->with('error', $e);
                } catch (Exception $e) {
                    return redirect()->route('checkout')->with('error', $e);
                }

                Session::forget('cart');
                $user->notify(new OrderCreated($user, $newOrder));
                return redirect()->route('home')->with('success', 'Se ha enviado un correo con los detalles de su compra.');
            }else{
                return redirect()->route('checkout')->with('error', 'Ha ocurrido un error desconocido. Intente de nuevo más tarde.');
            }
        } catch (\Conekta\ParameterValidationError $error){
            dd($error->getMesage());
            return redirect()->route('checkout')->with('error', $error->getMesage());
        } catch (\Conekta\Handler $error){
            return redirect()->route('checkout')->with('error', $error->getMesage());
        }

    }

    public function oxxoPayInvoice($order)
    {
        \Conekta\Conekta::setApiKey($this->apikey);
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('es');

        // dd($order);
        $currentOrder = Order::where('no_order', $order)->first();
        $currentOrder->cart = unserialize($currentOrder->cart);

        $orderConekta = \Conekta\Order::find($currentOrder->payment_id);
        // dd($orderConekta->charges[0]->payment_method->reference, $currentOrder);

        return view('partials.oxxopay', [
                'title' => 'My Orders',
                'order' => $currentOrder,
                'reference' => $orderConekta->charges[0]->payment_method->reference
            ]);
    }


    public function orders()
    {

        if (auth()->check()) {
            $user = Auth::user();
            if ($user->isAdmin == 1) {
                auth()->logout();
                return redirect()->route('login');
            }

            $orders = Auth::user()->orders()->orderBy('created_at', 'DESC')->get();
            $orders->transform(function($order, $key){
                $order->cart = unserialize($order->cart);
                $percent = $order->cart->discount / 100;
                $discount = $order->cart->totalPrice * ($percent);
                $order->descuento = $discount;
                return $order;
            });

            // dd($orders);

            return view('pages.orders', [
                'title' => 'My Orders',
                'orders' => $orders
            ]);

        }
    }

    public function changePassword(Request $request){
        if (Hash::check($request->get('actualPassword'), auth()->user()->password)){
            // dd($request->all());
            $user = auth()->user();
            $user->forceFill([
                'password' => $request->get('password'),
                'remember_token' => Str::random(60),
            ])->save();
            auth()->logout();
            return redirect()->route('login');
        }else{
            return redirect()->back()->with([
                'error' => [
                    'title' => '',
                    'body' => 'La contraseña actual ingresada es incorrecta.'
                ]
            ]);
        }
    }

    public function infoOrder($order)
    {
        \Conekta\Conekta::setApiKey($this->apikey);
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('es');

        //->charges[0]->payment_method->reference
        // dd($order);
        if (auth()->check()) {
            $user = Auth::user();
            $currentOrder = Order::where('no_order', $order)->where('user_id', $user->id)->first();
            if ($currentOrder) {
                $orderAdress = unserialize($currentOrder->adress);
                $currentOrder->cart = unserialize($currentOrder->cart);

                $percent = $currentOrder->cart->discount / 100;
                $discount = $currentOrder->cart->totalPrice * ($percent);
                $currentOrder->descuento = $discount;

                // dd($currentOrder, $orderAdress);
                if ($currentOrder->conekta == 1) {
                    $orderConekta = \Conekta\Order::find($currentOrder->payment_id);
                    // dd($orderConekta);
                    return view('pages.order-info', [
                        'title' => 'My Order',
                        'order' => $currentOrder,
                        'adress' => $orderAdress,
                        'userinfo' => $user,
                        'reference' => $orderConekta
                    ]);
                }else{
                    return view('pages.order-info', [
                        'title' => 'My Order',
                        'order' => $currentOrder,
                        'adress' => $orderAdress,
                        'userinfo' => $user
                    ]);
                }

            }else{
                abort(404);
            }
        }
    }

    public function terms()
    {
        if (auth()->check()) {
            $user = Auth::user();
            if ($user->isAdmin == 1) {
                auth()->logout();
                //return redirect()->route('login');
            }
        }
        return view('pages.terms', [
            'title' => 'Terms and Conditions'
        ]);
    }
    public function questions()
    {
        if (auth()->check()) {
            $user = Auth::user();
            if ($user->isAdmin == 1) {
                auth()->logout();
                //return redirect()->route('login');
            }
        }
        return view('pages.questions', [
            'title' => 'Frequent Questions'
        ]);
    }
    public function contact()
    {
        if (auth()->check()) {
            $user = Auth::user();
            if ($user->isAdmin == 1) {
                auth()->logout();
                //return redirect()->route('login');
            }
        }
        return view('pages.contact', [
            'title' => 'Contact'
        ]);
    }

    public function search(Request $request)
    {
        // dd($request->all());
        $promotionItems = Product::inRandomOrder()->where('inPromotion', '!=', 0)->get();

        $promotionItems->transform(function($promotionItems, $key){
            $promotionItems->image = unserialize($promotionItems->image);
            return $promotionItems;
        });


        if ($request->has('criteria')) {

            $productos = Product::whereRaw('name LIKE \'%' . $request->get('criteria') . '%\' or sku LIKE \'%' . $request->get('criteria') . '%\'')->paginate(48);

            $productos->transform(function($productos, $key){
                $productos->image = unserialize($productos->image);
                return $productos;
            });

            return view('pages.search', [
                'title' => 'Products',
                'products' => $productos,
                'promotionItems' => $promotionItems
            ]);
        } else{

            if ($request->get('carModel') && is_null($request->get('year')) && is_null($request->get('category')) && is_null($request->get('subcategory'))) {

                $products = Product::where('car_model_id', $request->get('carModel'))->orderBy('name', 'asc')->get();

                if (count($products)>0) {
                    $products->transform(function($products, $key){
                        $products->image = unserialize($products->image);
                        $products->years = unserialize($products->years);
                        return $products;
                    });

                    return view('pages.search', [
                        'title' => 'Products',
                        'products' => $products,
                        'promotionItems' => $promotionItems
                    ]);

                }else{
                    return view('pages.search', [
                        'title' => 'Products',
                        'products' => [],
                        'promotionItems' => $promotionItems
                    ]);
                }

            }else if(is_null($request->get('carModel')) && is_null($request->get('year')) && $request->get('category') && is_null($request->get('subcategory'))){

            	$products = Product::byCategory($request->get('category'))->orderBy('name', 'asc')->get();
            // 	dd(count($products), $products);
                $products->transform(function($product, $key){
                    $product->image = unserialize($product->image);
                    $product->years = unserialize($product->years);
                    // dd($product);
                    return $product;
                });

                if (count($products)>0) {

                    return view('pages.search', [
                        'title' => 'Products',
                        'products' => $products,
                        'promotionItems' => $promotionItems
                    ]);
                }else{
                    return view('pages.search', [
                        'title' => 'Products',
                        'products' => [],
                        'promotionItems' => $promotionItems
                    ]);
                }

            	// dd($request->all(), $products);
            }else if(is_null($request->get('carModel')) && is_null($request->get('year')) && $request->get('category') && $request->get('subcategory')){

                // dd("test");
            	$products = Product::byCategory($request->get('category'))
                    ->bySubcategory($request->get('subcategory'))
                    ->orderBy('name', 'asc')->get();

                $products->transform(function($products, $key){
                    $products->image = unserialize($products->image);
                    $products->years = unserialize($products->years);
                    return $products;
                });

                if (count($products)>0) {
                    return view('pages.search', [
                        'title' => 'Products',
                        'products' => $products,
                        'promotionItems' => $promotionItems
                    ]);
                }else{
                    return view('pages.search', [
                        'title' => 'Products',
                        'products' => [],
                        'promotionItems' => $promotionItems
                    ]);
                }

            	// dd($request->all(), $products);
            }else if($request->get('carModel') && $request->get('year') && is_null($request->get('category')) && is_null($request->get('subcategory'))){

                $products = Product::where('car_model_id', $request->get('carModel'))->orderBy('name', 'asc')->get();

                if (count($products)>0) {
                    $queryYear = Year::findOrFail($request->get('year'));

                    $products->transform(function($products, $key){
                        $products->image = unserialize($products->image);
                        $products->years = unserialize($products->years);
                        return $products;
                    });

                    $arrayName = array();

                    foreach ($products as $key => $value) {
                        foreach ($value->years as $k => $year) {
                            if ($year->id == $queryYear->id) {
                                array_push($arrayName, $value);
                            }
                        }
                    }

                    if (count($arrayName)>0) {
                        return view('pages.search', [
                            'title' => 'Products',
                            'products' => $products,
                            'promotionItems' => $promotionItems
                        ]);
                    }else{
                        return view('pages.search', [
                            'title' => 'Products',
                            'products' => [],
                            'promotionItems' => $promotionItems
                        ]);
                    }
                }
            }else if($request->get('carModel') && is_null($request->get('year')) && $request->get('category') && is_null($request->get('subcategory'))){



                $products = Product::byCarModel($request->get('carModel'))
                    ->byCategory($request->get('category'))
                    ->orderBy('name', 'asc')->get();

                // dd($request->all(), $products);

                if (count($products)>0) {

                    $products->transform(function($products, $key){
                        $products->image = unserialize($products->image);
                        $products->years = unserialize($products->years);
                        return $products;
                    });

                    return view('pages.search', [
                        'title' => 'Products',
                        'products' => $products,
                        'promotionItems' => $promotionItems
                    ]);

                }else{
                    return view('pages.search', [
                        'title' => 'Products',
                        'products' => [],
                        'promotionItems' => $promotionItems
                    ]);
                }

            }else if ($request->get('carModel') && $request->get('year') && $request->get('category') && is_null($request->get('subcategory'))) {

                $products = Product::byCarModel($request->get('carModel'))
                    ->byCategory($request->get('category'))
                    // ->bySubcategory($request->get('subcategory'))
                    ->orderBy('name', 'asc')->get();

                // dd($request->all(), $products);

                if (count($products)>0) {

                    $queryYear = Year::findOrFail($request->get('year'));

                    $products->transform(function($products, $key){
                        $products->image = unserialize($products->image);
                        $products->years = unserialize($products->years);
                        return $products;
                    });


                    $arrayName = array();

                    foreach ($products as $key => $value) {
                        foreach ($value->years as $k => $year) {
                            if ($year->id == $queryYear->id) {
                                array_push($arrayName, $value);
                            }
                        }
                    }

                    if (count($arrayName)>0) {
                        return view('pages.search', [
                            'title' => 'Products',
                            'products' => $products,
                            'promotionItems' => $promotionItems
                        ]);
                    }else{
                        return view('pages.search', [
                            'title' => 'Products',
                            'products' => [],
                            'promotionItems' => $promotionItems
                        ]);
                    }
                }

            }else if ($request->get('carModel') && is_null($request->get('year')) && $request->get('category') && $request->get('subcategory')) {

                $products = Product::byCarModel($request->get('carModel'))->byCategory($request->get('category'))->bySubcategory($request->get('subcategory'))->orderBy('name', 'asc')->get();

                if (count($products)>0) {


                    $products->transform(function($products, $key){
                        $products->image = unserialize($products->image);
                        $products->years = unserialize($products->years);
                        return $products;
                    });
                    // dd($products);
                    return view('pages.search', [
                        'title' => 'Products',
                        'products' => $products,
                        'promotionItems' => $promotionItems
                    ]);
                }else{
                    return view('pages.search', [
                        'title' => 'Products',
                        'products' => [],
                        'promotionItems' => $promotionItems
                    ]);
                }

            }else if ($request->get('carModel') && $request->get('year') && $request->get('category') && $request->get('subcategory')) {

                $queryYear = Year::findOrFail($request->get('year'));
                // dd($year);

                $products = Product::where('car_model_id', $request->get('carModel'))->where('subcategory_id', $request->get('subcategory'))->orderBy('name', 'asc')->get();

                $products->transform(function($products, $key){
                    $products->image = unserialize($products->image);
                    $products->years = unserialize($products->years);
                    return $products;
                });

                $arrayName = array();

                foreach ($products as $key => $value) {
                    foreach ($value->years as $k => $year) {
                        if ($year->id == $queryYear->id) {
                            array_push($arrayName, $value);
                        }
                    }
                }

                if (count($arrayName)>0) {
                    return view('pages.search', [
                        'title' => 'Products',
                        'products' => $products,
                        'promotionItems' => $promotionItems
                    ]);
                }else{
                    return view('pages.search', [
                        'title' => 'Products',
                        'products' => [],
                        'promotionItems' => $promotionItems
                    ]);
                }
            }else{
                return view('pages.search', [
                    'title' => 'Products',
                    'products' => [],
                    'promotionItems' => $promotionItems
                ]);
            }
        }
    }

    public function resend()
    {
        if (auth()->check()) {
            // dd("Loged");
            if (is_null(auth()->user()->activation_token)) {
                return redirect()->route('my-account');
            }else{
                $user = Auth::user();
                Auth::logout();
                return view('pages.resend-email', [
                    'title' => 'Reenvio correo',
                    'user' => $user
                ]);
            }
        }else{
            return redirect()->route('login');
        }
    }

    public function resendActivation(Request $request)
    {
        $user = User::findOrFail($request->get('user_id'));
        $user->notify(new ResendEmailActivation($user));
        return redirect()->route('home')->with('success', 'Se ha enviado un correo de activación necesario para su ingreso.');
    }

    public function about(){
        return view('pages.who-are-we', [
            'title' => 'Nosotros'
        ]);
    }

    public function applyCoupon(Request $request)
    {
        $date = date("Y-m-d");
        $coupon = Coupon::where('code', $request->get('coupon'))->first();
        //dd($request->get('coupon'), $coupon);
        if (isset($coupon)) {
            if ($coupon->limitDate <= $date) {
                return redirect()->route('checkout')->with([
                    'warning' => [
                        'title' => '',
                        'body' => 'El cupón es inválido. Intente de nuevo más tarde.'
                    ]
                ]);
            }elseif ($coupon->uses >= $coupon->usageLimit) {
                return redirect()->route('checkout')->with([
                    'warning' => [
                        'title' => '',
                        'body' => 'El cupón es inválido. Intente de nuevo más tarde.'
                    ]
                ]);
            }

            if (!Session::has('cart')) {
                return redirect()->route('checkout')->with([
                    'warning' => [
                        'title' => '',
                        'body' => 'No se encuentran productos en el carrito. Intente de nuevo más tarde.'
                    ]
                ]);
            }

            $oldCart = Session::get('cart');
            $cart = new Cart($oldCart);
            $cart->discount = $coupon->percent;
            $cart->coupon = $coupon;


            $request->session()->put('cart', $cart);
            // dd($cart);
            return redirect()->route('checkout')->with([
                'success' => [
                    'title' => '',
                    'body' => 'Se aplicó el cupón correctamente.'
                ]
            ]);

        }else{
            return redirect()->route('checkout')->with([
                'warning' => [
                    'title' => '',
                    'body' => 'No se ha encontrado el cupón. Intente de nuevo.'
                ]
            ]);
        }

    }
}
