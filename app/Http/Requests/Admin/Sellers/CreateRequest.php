<?php

namespace App\Http\Requests\Admin\Sellers;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() and auth()->user()->isAdmin ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|email|confirmed|unique:users,email',
            'password' => 'required|confirmed|min:6'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nombre',
            'surname' => 'apellidos',
            'email' => 'correo',
            'email_confirmation' => 'confirmación de correo',
            'password' => 'clave',
            'password_confirmation' => 'confirmación de clave'
        ];
    }
}
