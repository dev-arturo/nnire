<?php

namespace App\Http\Requests\Admin\Clients;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'client_type_id' => 'required|exists:client_types,id',
            'creditLimit' => 'required_if:client_type_id,1',
            'name' => 'required|string',
            'surname' => 'required|string',
            'adress' => 'required|string',
            'vendor_id' => 'required',
            'postal_code' => 'required|string',
            'colony' => 'required|string',
            'city' => 'required|string',
            'state' => 'required|string',
            'phone' => 'required|numeric',
            'email' => 'required|email',
            'rfc' => 'required|string',
            'curp' => 'required|string',
            'payment' => 'required',
            'payment_method' => 'required',
            'usage_cfdi' => 'required'
        ];

        if (auth()->user()->isAdmin) {
            $rules['user_id'] = 'required|exists:users,id';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'name' => 'nombre',
            'client_type_id' => 'tipo de cliente',
            'creditLimit' => 'límite de crédito',
            'user_id' => 'vendedor'
        ];
    }

    public function messages()
    {
        return [
            'creditLimit.required_if' => 'El campo límite de crédito es obligatorio cuando cliente es mayorista.'
        ];
    }
}
