<?php

namespace App\Http\Requests\Admin\Subcategories;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() and auth()->user()->isAdmin ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:subcategories,name',
            'category_id' => 'required|exists:categories,id'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nombre',
            'category_id' => 'categoría'
        ];
    }
}
