<?php

namespace App\Http\Requests\Admin\Products;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() and auth()->user()->isAdmin ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'sku' => 'required',
            'model' => 'required',
            // 'eanUpc' => 'required',
            'description' => 'required',
            'images.*' => 'mimes:png,jpg,jpeg,gif',
            'price' => 'required|numeric',
            'wholesale' => 'required|numeric',
            'car_model_id' => 'required|exists:car_models,id',
            'years' => 'required|exists:years,id',
            'category_id' => 'required|exists:categories,id',
            'subcategory_id' => 'required|exists:subcategories,id'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nombre',
            'sku' => 'sku',
            'model' => 'modelo',
            'eanUpc' => 'EAN / UPC',
            'description' => 'descripción',
            'image' => 'imagen',
            'price' => 'precio',
            'wholesale' => 'precio mayoreo',
            'car_model_id' => 'modelo del carro',
            'year_id' => 'año',
            'category_id' => 'categoría',
            'subcategory_id' => 'subcategoría'
        ];
    }
}
