<?php

namespace App\Http\Requests\Admin\Banners;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() and auth()->user()->isAdmin ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hyperlink' => 'url|nullable',
            'hyperlinkTarget' => 'required|in:_self,_blank',
            'image' => 'required|mimes:jpg,jpeg,png,gif'
        ];
    }

    public function attributes()
    {
        return [
            'hyperlink' => 'enlace',
            'hyperlinkTarget' => 'target del enlace',
            'image' => 'imagen'
        ];
    }
}
