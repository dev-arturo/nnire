<?php

namespace App\Http\Requests\Admin\Coupons;

use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() and auth()->user()->isAdmin ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|alpha_num|unique:coupons,code,' . $this->coupon->id,
            'usageLimit' => 'required|numeric|min:1',
            'limitDate' => 'required|date|after_or_equal:' . date('Y-m-d')
        ];
    }

    public function attributes()
    {
        return [
            'code' => 'código',
            'usageLimit' => 'usos permitidos',
            'limitDate' => 'fecha límite'
        ];
    }
}
