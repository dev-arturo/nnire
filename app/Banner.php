<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
class Banner extends Model
{
    use FormAccessible;
    /**
 * Get the user's first name.
 *
 * @param  string  $value
 * @return string
 */
public function getDateOfBirthAttribute($value)
{
    return Carbon::parse($value)->format('m/d/Y');
}

/**
 * Get the user's first name for forms.
 *
 * @param  string  $value
 * @return string
 */
public function formDateOfBirthAttribute($value)
{
    return Carbon::parse($value)->format('Y-m-d');
}
    protected $fillable = [
        'order',
        'hyperlink',
        'hyperlinkTarget'
    ];

    public function updateImage($image)
    {
        if (!File::isDirectory(public_path($this->storagePath()))) {
            File::makeDirectory(public_path($this->storagePath()), 0777, true);
        }

        if ($image) {
            $this->deleteImage();
            $this->image = md5(microtime()) . '.' . $image->getClientOriginalExtension();
            $image->move(public_path($this->storagePath()), $this->image);
            $this->save();
        }
    }

    public function deleteImage()
    {
        if ($this->hasImage()) {
            File::delete(public_path($this->imagePath()));
        }

        $this->image = null;

        return $this;
    }

    public function hasImage()
    {
        return $this->image and File::exists($this->imagePath()) ? true : false;
    }

    public function storagePath()
    {
        return 'storage/banners';
    }

    public function imagePath()
    {
        return 'storage/banners/' . $this->image;
    }

    public static function nextPosition()
    {
        $lastBanner = self::orderBy('order', 'desc')->first();

        return $lastBanner ? $lastBanner->order + 1 : 1;
    }
}
