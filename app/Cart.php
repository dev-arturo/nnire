<?php

namespace App;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;
class Cart extends Model
{
    use FormAccessible;
    /**
 * Get the user's first name.
 *
 * @param  string  $value
 * @return string
 */
public function getDateOfBirthAttribute($value)
{
    return Carbon::parse($value)->format('m/d/Y');
}

/**
 * Get the user's first name for forms.
 *
 * @param  string  $value
 * @return string
 */
public function formDateOfBirthAttribute($value)
{
    return Carbon::parse($value)->format('Y-m-d');
}


	public $totalQty = 0;
	public $totalPrice = 0;
	public $discount = 0;
	public $coupon = '';

	public function __construct($oldCart)
	{
		if ($oldCart) {
			$this->items = $oldCart->items;
			$this->totalQty = $oldCart->totalQty;
			$this->totalPrice = $oldCart->totalPrice;
			$this->discount = $oldCart->discount;
			$this->coupon = $oldCart->coupon;
		}
	}

	public function add($item, $id)
	{
		// dd($item);

        if (auth()->user()->isAdmin == 3) {
    	    $storedItem = ['qty'=> 0, 'price'=>$item->wholesale, 'item'=> $item];
    	}elseif(auth()->user()->isAdmin == 2){
    		$storedItem = ['qty'=> 0, 'price'=>$item->wholesale, 'item'=> $item];
    	}else{
		    $storedItem = ['qty'=> 0, 'price'=>$item->price, 'item'=> $item];
		}

		if ($this->items) {
			if (array_key_exists($id, $this->items)) {
				$storedItem = $this->items[$id];
			}
		}

	    if (auth()->user()->isAdmin == 3) {
			$storedItem['qty']++;
			$storedItem['price'] = $item->wholesale * $storedItem['qty'];
			$this->items[$id] = $storedItem;
			$this->totalQty++;
			$this->totalPrice += $item->wholesale;
		}elseif(auth()->user()->isAdmin == 2){
			$storedItem['qty']++;
			$storedItem['price'] = $item->wholesale * $storedItem['qty'];
			$this->items[$id] = $storedItem;
			$this->totalQty++;
			$this->totalPrice += $item->wholesale;
		}else{
		    $storedItem['qty']++;
			$storedItem['price'] = $item->price * $storedItem['qty'];
			$this->items[$id] = $storedItem;
			$this->totalQty++;
			$this->totalPrice += $item->price;
		}

	}

	public function reduceBy($item, $id)
	{
		$this->items[$id]['qty']--;
		$this->items[$id]['price'] -= $this->items[$id]['item']['price'];
		$this->totalQty--;
		$this->totalPrice -= $this->items[$id]['item']['price'];

		if ($this->items[$id]['qty'] <= 0) {
			unset($this->items[$id]);
		}
	}

	public function removeItem($id)
	{
		$this->totalQty -= $this->items[$id]['qty'];
		$this->totalPrice -= $this->items[$id]['price'];
		unset($this->items[$id]);
		// if (!$this->totalQty > 1) {
		// 	Session::forget('cart');
		// 	// dd('Borrado');
		// }else{
		// 	// dd('Hay más de uno.');
		// }
	}

	public function addDiscount()
	{
		dd($this->discount);
	}

	public function updateCart()
	{
		dd($this);
	}

}
