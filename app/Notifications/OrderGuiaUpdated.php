<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrderGuiaUpdated extends Notification
{
    use Queueable;

    protected $user;
    protected $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->from(config('mail.username', 'Nissan Refacciones'))
                    ->subject('Pago confirmado')
                    ->greeting('Hola '. $this->user->name . ' ' . $this->user->surname)
                    ->line('El pago de la orden ' . $this->order->no_order . ' ha sido comprobado.')
                    ->line('Se ha actualizado el número de guía a ' . $this->order->no_guia . ' por la compañía ' . $this->order->ship_company . '.')
                    ->line('Para más detalles accede a tu cuenta y mira el apartado de "Tus compras".')
                    ->line('Gracias por usar nuestra aplicación!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
