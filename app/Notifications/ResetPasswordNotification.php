<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPasswordNotification extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->from(config('mail.username', 'Nissan Refacciones'))
            ->subject('Reestablecer contrase���a')
            ->line('Si quieres reiniciar tu contraseña da clic en el siguiente botón.')
            ->action('Reiniciar contraseña', url(config('app.url').route('password.reset', $this->token, false)))
            ->line('Si no deseas hacer un reinicio de tu contraseña no es necesario dar clic.');
    }
}
