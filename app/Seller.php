<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $fillable = [
        'user_id'
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'id');
    }

    public function clients(){
    	return $this->belongsTo(Client::class, 'vendor_id');	
    }
}
