<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'surname',
        'email',
        'conekta_id',
        'activation_token',
        'isAdmin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function rol(){
        return $this->belongsTo('App\Rol');
    }

    public function persona(){
        return $this->belongsTo('App\Persona');
    }



    public function clients()
    {
        return $this->hasMany(Client::class, 'vendor_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function sellers()
    {
        return $this->hasMany(Seller::class, 'id');
    }


    public function scopeWithoutAdmin($query)
    {
        return $query->where('isAdmin', '!=', true);
    }

    public function adress(){
        return $this->hasMany(Adresses::class, 'user_id');
    }

    public function scopeWithSeller($query)
    {
        return $query->where('isAdmin', '=', 3);
    }

    public function scopeWithClient($query)
    {
        return $query->where('isAdmin', '=', 2);
    }

    public function scopeWithUser($query)
    {
        return $query->where('isAdmin', '=', 0);
    }

    public function setPasswordAttribute($value)
    {
        return $this->attributes['password'] = bcrypt($value);
    }

    public function fullName()
    {
        return $this->name . ' ' . $this->surname;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

}
