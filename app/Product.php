<?php

namespace App;

use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Product extends Model
{
    use FormAccessible;
    /**
 * Get the user's first name.
 *
 * @param  string  $value
 * @return string
 */
public function getDateOfBirthAttribute($value)
{
    return Carbon::parse($value)->format('m/d/Y');
}

/**
 * Get the user's first name for forms.
 *
 * @param  string  $value
 * @return string
 */
public function formDateOfBirthAttribute($value)
{
    return Carbon::parse($value)->format('Y-m-d');
}
    protected $fillable = [
        'sku',
        'model',
        'eanUpc',
        'name',
        'description',
        'dataSheet',
        'years',
        'price',
        'wholesale',
        'car_model_id',
        'subcategory_id'
    ];

    public function carModel()
    {
        return $this->belongsTo(CarModel::class);
    }

    public function year()
    {
        return $this->belongsTo(Year::class);
    }

    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class);
    }

    public function scopeByCarModel($query, $carModelId)
    {
        if ($carModelId) {
            $query->where('car_model_id', $carModelId);
        }

        return $query;
    }

    public function scopeByYear($query, $yearId)
    {
        if ($yearId) {
            $query->where('year_id', $yearId);
        }

        return $query;
    }

    public function scopeByCategory($query, $categoryId)
    {
        if ($categoryId) {
            $query->whereHas('subcategory', function ($query) use ($categoryId) {
                $query->where('category_id', $categoryId);
            });
        }

        return $query;
    }

    public function scopeBySubcategory($query, $subcategoryId)
    {
        if ($subcategoryId) {
            $query->where('subcategory_id', $subcategoryId);
        }

        return $query;
    }

    public function updateImages($images)
    {
        if (!File::isDirectory(public_path($this->storagePath()))) {
            File::makeDirectory(public_path($this->storagePath()), 0777, true);
        }
        if ($images) {
            $arrayImages = $this->image ? unserialize($this->image) : [];
            foreach ($images as $key => $image) {
                $imageName = md5(microtime()) . '.' . $image->getClientOriginalExtension();
                $image->move(public_path($this->storagePath()), $imageName);
                array_push($arrayImages, $imageName);
            }
            $this->image = serialize($arrayImages);
            //$this->deleteImage();
            $this->save();
            return false;
        }

    }

    public function deleteImage($images)
    {
        $img = unserialize($this->image);
        // dd($images, $img);
        if (count($img)>0) {
            $result = array_values(array_diff($img, $images));
            $intersection = array_intersect($img, $images);
            // dd($intersection);
            if (count($result)>0 && count($intersection) > 0) {
                foreach ($intersection as $key => $value) {
                    File::delete(public_path('storage/products/'.$value));
                }
                $this->image = serialize($result);
                $this->save();
            }else{
                foreach ($img as $key => $value) {
                    File::delete(public_path('storage/products/'.$value));
                }
                $this->image = null;
                $this->save();
            }
        }


    }

    public function hasImage()
    {
        //dd($this->image[0]);
        if ($this->image) {
            return $this->image[0] and File::exists($this->imagePath()) ? true : false;
        }
    }

    public function storagePath()
    {
        return 'storage/products';
    }

    public function imagePath()
    {
        return 'storage/products/' . $this->image[0];
    }

    public function placeholderImage()
    {
        return 'images/placeholder.jpg';
    }
}
