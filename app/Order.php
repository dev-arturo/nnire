<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

	protected $fillable = [
        'cart',
        'payment_id',
        'no_order',
        'user_id',
        'adress'
    ];

    public function user(){
    	return $this->belongsTo(Order::class);
    }
}
