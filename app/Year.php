<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $fillable = [
        'year'
    ];

    public function carModels()
    {
        return $this->belongsToMany(CarModel::class, 'car_model_year', 'year_id', 'car_model_id');
    }
}
