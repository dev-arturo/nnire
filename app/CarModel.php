<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
class CarModel extends Model
{
    use FormAccessible;
    /**
 * Get the user's first name.
 *
 * @param  string  $value
 * @return string
 */
public function getDateOfBirthAttribute($value)
{
    return Carbon::parse($value)->format('m/d/Y');
}

/**
 * Get the user's first name for forms.
 *
 * @param  string  $value
 * @return string
 */
public function formDateOfBirthAttribute($value)
{
    return Carbon::parse($value)->format('Y-m-d');
}
    protected $fillable = [
        'name'
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function years()
    {
        return $this->belongsToMany(Year::class, 'car_model_year', 'car_model_id', 'year_id');
    }

    public function syncYears($yearsCommaSeparated)
    {
        $this->years()->detach();

        if ($yearsCommaSeparated != '') {
            foreach (explode(',', $yearsCommaSeparated) as $inputYear) {
                $year = Year::where('year', $inputYear)->first();

                if (!$year) {
                    $year = Year::create(['year' => $inputYear]);
                }

                $this->years()->attach($year->id);
            }
        }

        return $this;
    }
}
