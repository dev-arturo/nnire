@extends('layouts.app')

@section('content')
	
	<section class="checkout-info">
		<div class="nav-section-checkout">
			<span>Detalles de compra</span>
		</div>
		
			@if(Session::has('error'))
				<div class="">
					<div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
						<div id="charge-message" class="alert alert-warning">
							{{Session::get('error')}}
						</div>
					</div>
				</div>
			@endif
	
		<div class="table-products">
			<div class="header-table">
				<div class="photo-nav"></div>
				<div class="code-nav">Código</div>
				<div class="product-nav">Producto</div>
				<div class="count-nav">Cantidad</div>
				<div class="price-nav">P/U</div>
				<div class="subtotal-nav">Subtotal</div>
				<div class="actions-nav"></div>
			</div>
			<div class="items-products">
				@if(Session::has('cart'))
					@foreach($products as $product)
						<div class="items-product">
							<div class="photo-item">
								{{-- {{dd($product['item']['image'][0])}} --}}
								@if($product['item']['image'] != null)
									<img src="{{ asset('storage/products/'. $product['item']['image'][0]) }}" alt="">
								@else
									<img src="{{ asset('images/placeholder.jpg') }}" alt="placeholderImage">
								@endif
							</div>
							<div class="code-item">
								<span>{{$product['item']['sku']}}</span>
							</div>
							<div class="product-item">
								<p>{{$product['item']['name']}}</p>
							</div>
							<div class="count-item" >
								{{-- <button style="width: 20%;">-</button> --}}
								
								
								{{ Form::open(['route' => 'change-item-cart', 'style' => 'padding-top: 10px;']) }}
									<input id="{{$product['item']['sku']}}" name="id" type="text" value="{{$product['item']['id']}}" hidden style="display: none;">
									<input disabled="true" class="qtyItems" name="quantity" type="number" min="1" max="99" value="{{$product['qty']}}" style="width: 60%; height: 10px;" required="true">
								{{ Form::close() }}
								{{-- <select name="quantity" id="{{$product['item']['id']}}" class="item_qty">
									@for ($i = 1; $i <= 10; $i++)
										@if($product['qty'] == $i)
								  			<option value="{{ $i }}" selected>{{$i}}</option>
										@else
											<option value="{{ $i }}">{{$i}}</option>
										@endif
									  
									@endfor
								</select> --}}
								{{-- <button style="width: 20%;">+</button> --}}
							</div>
							@if(auth()->user()->isAdmin == 3)
    							<div class="price-item">
    								<span>$ {{ number_format($product['item']['wholesale'], 2, '.', ',') }}</span>
    							</div>
    						@elseif(auth()->user()->isAdmin == 2)
    						    <div class="price-item">
    								<span>$ {{ number_format($product['item']['wholesale'], 2, '.', ',') }}</span>
    							</div>
    						@else
    						    <div class="price-item">
    								<span>$ {{ number_format($product['item']['price'], 2, '.', ',') }}</span>
    							</div>
    						@endif
							@if(auth()->user()->isAdmin == 3)
    							<div class="subtotal-item">
    								<span>$ {{ number_format($product['item']['wholesale'] * $product['qty'], 2, '.', ',') }}</span>
    							</div>
							@elseif(auth()->user()->isAdmin == 2)
    							<div class="subtotal-item">
    								<span>$ {{ number_format($product['item']['wholesale'] * $product['qty'], 2, '.', ',') }}</span>
    							</div>
    						@else
        						<div class="subtotal-item">
    								<span>$ {{ number_format($product['item']['price'] * $product['qty'], 2, '.', ',') }}</span>
    							</div>
							@endif
							<div class="actions-item">
								<span class="delete">
									
								</span>
							</div>
						</div>
					@endforeach
				@else
					<div class="items-product">
						<div class="photo-item">
							
						</div>
						<div class="code-item">
							
						</div>
						<div class="product-item">
							<p>No items in this cart!</p>
						</div>
						<div class="count-item">
							
						</div>
						<div class="price-item">
							
						</div>
						<div class="subtotal-item">
							
						</div>
						<div class="actions-item">
							<span class="delete">
								
							</span>
						</div>
					</div>
				@endif
			<div class="sub-table-items">
				<div class="checkout-total">
					<div class="title-header-checkout">
						<span>SHOPPING CART TOTAL</span>
					</div>
					<div class="cont-info-checkout">
						<div class="subtotal-checkout">
							<span>Subtotal</span> <span class="price">$ {{ number_format($totalPrice, 2, '.', ',') }}
							</span>
						</div>
						<div class="discount-checkout">
							@if(isset($discount))
								<span>Descuento</span> <span class="price">$ {{ number_format($discount , 2, '.', ',') }}</span>
							@else
								<span>Descuento</span> <span class="price">$ 0.00</span>
							@endif
						</div>
						<div class="total-checkout">
							<span>Envio </span> <span class="price" id="">$ 0.00</span>
						</div>
						<div class="total-checkout">
							@if(isset($discount))
								<span>Total</span> <span class="price" id="totalprice">$ {{ number_format($totalPrice - $discount, 2, '.', ',') }}</span>
							@else
								<span>Total</span> <span class="price" id="totalprice">$ {{ number_format($totalPrice, 2, '.', ',') }}</span>
							@endif
						</div>
						
							<!-- <input type="text" name="amount" value="{{$totalPrice}}" hidden> -->
							@if(auth()->user()->isAdmin == 3)
							@elseif(auth()->user()->isAdmin == 2)
							    {{ Form::open(['route' => 'confirmOrder', 'style' => 'padding-top: 10px;']) }}
							        <select name="adress" id="adresses" class="form-control selectpicker select2" required="true">
					                	<option value=""></option>
					                	@foreach($adresses as $adress)
					                		<option value="{{$adress->id}}">{{$adress->adress}}</option>
					                	@endforeach
					                </select>
    							    <button class="uHover" type="submit">
        								<img src="{{ asset('images/correct-symbol.png') }}">
        								<span class="checkoutToPay">Confirmar</span>
        							</button>
    							{{ Form::close() }}
							@else
    							<a class="uHover" href="{{route('pay')}}">
    								<img src="{{ asset('images/correct-symbol.png') }}">
    								<span class="checkoutToPay">Comprar ahora</span>
    							</a>
							@endif
						

						<!-- <div>
							<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" >
							<input type="hidden" name="cmd" value="_s-xclick">
							<input type="hidden" name="hosted_button_id" value="X9PFKZ7J4SN8L">
							<input type="image" src="https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea." style="cursor: pointer;">
							<img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1">
							</form>
						</div> -->

					</div>
				</div>
			</div>
			
			<div class="table-products">
			    <div class="nav-section-checkout">
        			<span>Cuentas bancarias</span>
        		</div>
            	<div class="header-table">
            		<div class="code-nav">BANCO</div>
            		<div class="product-nav">CUENTA</div>
            		<div class="count-nav">SUC</div>
            		<div class="price-nav"></div>
            		<div class="subtotal-nav">CLABE INTERBANCARIA</div>
            	</div>
            	<div class="items-products">
            		<div class="items-product">
            			<div class="code-item">
            				BANCOMER
            			</div>
            			<div class="product-item">
            				0446893678
            			</div>
            			<div class="count-item">
            				723
            			</div>
            			<div class="price-item">
            				GDL
            			</div>
            			<div class="subtotal-item">
            				012320004468936781
            			</div>
            		</div>
            		<div class="items-product">
            			<div class="code-item">
            				BANAMEX
            			</div>
            			<div class="product-item">
            				5536955
            			</div>
            			<div class="count-item">
            				567
            			</div>
            			<div class="price-item">
            				GDL
            			</div>
            			<div class="subtotal-item">
            				002320056755369553
            			</div>
            		</div>
            		<div class="items-product">
            			<div class="code-item">
            				HSBC
            			</div>
            			<div class="product-item">
            				4057392698
            			</div>
            			<div class="count-item">
            				129
            			</div>
            			<div class="price-item">
            				GDL
            			</div>
            			<div class="subtotal-item">
            				021320040573926987
            			</div>
            		</div>
            		<div class="items-product">
            			<div class="code-item">
            				BANREGIO
            			</div>
            			<div class="product-item">
            				131022340015
            			</div>
            			<div class="count-item">
            				
            			</div>
            			<div class="price-item">
            				GDL
            			</div>
            			<div class="subtotal-item">
            				058320000001017986
            			</div>
            		</div>
            	</div>
            </div>
		</div>
	</section>

	@include('partials.need-help')

	@include('partials.ways-to-pay')

@endsection

@section('js')

	<script>

		jQuery(document).ready(function () {
	        $('.select2').select2();

	        $("#adresses").select2({
			    placeholder: "Selecciona una dirección:",
			    allowClear: true
			});

		    // $(".qtyItems").popover({
		    //     placement: 'bottom',
		    //     html: 'true',
		    //     title : '<span class="text-info"><strong>Actualizar</strong></span>'+
		    //             '<button type="button" id="close" class="close" onclick="$(&quot;.qtyItems&quot;).popover(&quot;hide&quot;);">&times;</button>',
		    //     content : '<div class="row"><div class="col-md-12"><button type="submit" class="btn btn-success">Aceptar</button></div></div>'
		    // });
	    });

       function updateAdresses(value, selector)
        {
        	console.log(value);
            $.ajax({
                type: 'get',
                url: '{{ route('ajax.loadAdress') }}/' + value,
                success: function (response) {
                	console.log(response);
                    $(selector).html(response);
                }
            });
        }

	</script>

	

@endsection