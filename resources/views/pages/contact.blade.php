@extends('layouts.app')

@section('content')

	<section id="contact-header">
		<div class="container-contact-info">
			<div class="infor-contact">
				<h2>Refaccionesnissan.com.mx</h2>
				<p>Refaccionesnissan.com.mx</p>
				<p>Dirección: Av. Adolfo López Mateos Sur 1460, Colonia Chapalita</p>
				<p>C.P. 44500 Guadalajara, Jal.</p>
				<p>Teléfono: 01 33 3122 6677</p>
				<p>Whatsapp: (33) 38006129</p>
			</div>
			<div class="image-contact">
				<img src="{{asset('images/contact.png')}}">
			</div>
		</div>
	</section>

	<section id="contact-form">
		<div class="container-form">
			<div class="title-container-contact">
				<h2>Contáctanos</h2>
				<p>Para contactarnos por favor llena este sencillo formulario y no olvides explicar el motivo antes de hacer clic en el botón de "Enviar". Pronto nos pondremos en contacto contigo para hacer un oportuno seguimiento de tu caso.</p>
			</div>
			<div class="form-container-contact">
				<form>
					<div class="title-form border-container-form">
						<span class="title-spn">Información de Contacto</span>
						<span class="float-required" style="float: right;"><span class="required">*</span> Campos Obligatorios</span>
					</div>
					<div class="border-container-form">
						<label class="form-label-short">
							<span>Nombre <span class="required">*</span></span>
							<div>
								<input type="text" name="name">
								<i class="fa fa-user" aria-hidden="true"></i>
							</div>
						</label>
						<label class="form-label-short">
							<span>Email <span class="required">*</span></span>
							<div>
								<input type="text" name="mail">
								<i class="fa fa-envelope" aria-hidden="true"></i>
							</div>
						</label>
						<label class="form-label-short">
							<span>Teléfono <span class="required">*</span></span>
							<div>
								<input type="text" name="phone">
								<i class="fa fa-phone" aria-hidden="true"></i>
							</div>
						</label>
						<label class="form-label-large">
							<span>Mensaje</span>
							<div>
								<textarea></textarea>
							</div>
						</label>
						<label class="form-label-large">
							<span></span>
							<div class="checkboxItem">
								<input type="checkbox" name="terms" > <span>He leído y acepto el <a href="{{route('terms')}}" target="_blank">aviso de privacidad</a></span>
							</div>
						</label>
						<label class="form-label-large">
							<button class="sendForm">Enviar</button>
						</label>
					</div>
				</form>
			</div>
		</div>
	</section>

	<section id="content-map">
		<div id="map">

		</div>
	</section>

@endsection

@section('js')
	
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDczyNzJHzR_cxuXk8_k2-RouvFz3GWUTU&callback=initMap"></script>

	<script>
      function initMap() {
        var uluru = { lat: 20.663378, lng: -103.395151 };
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 17,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>

@endsection
