@extends('layouts.app')

@section('content')
	
	<section id="loginContainer">

		<div class="login-user-title">
			¿Olvidaste tu Contraseña?
		</div>

		<div class="container-register register-box">
			<div class="login-section">
				<div class="title-register">
					<span>Por favor ingresa tu direcciòn de correo electrònico. Recibirás un link para cambiar tu contraseña. </span>
				</div>	
				<div class="desc-container">
					<form method="POST" action="{{ route('password.email') }}" class="login-form">
                        {{ csrf_field() }}
						<label class="form-type-1">
							<span>Correo electronico *</span>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
						</label>
						<div class="container-button btn-register">
						<button type="submit" class="button-login uHover">
							Enviar
						</button>
					</div>
					</form>
				</div>
			</div>
		</div>

	</section>

@endsection

@section('js')

@endsection