@extends('layouts.app')

@section('content')
	@if(Session::has('success'))
		<div class="container">
			<div class="row" style="margin-top: 15px;">
				<div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
					<div id="charge-message" class="alert alert-success">
						{{Session::get('success')}}
					</div>
				</div>
			</div>
		</div>
	@endif

	@if(Session::has('warning'))
		<div class="container">
			<div class="row" style="margin-top: 15px;">
				<div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
					<div id="charge-message" class="alert alert-danger">
						{{Session::get('warning')}}
					</div>
				</div>
			</div>
		</div>
	@endif
{{--
	@if(count($banners))
		<section class="header-menu">
			<div class="slider-container">
				<ul class="slideHome">
					@foreach($banners as $banner)
						<li>
							<a href="{{ $banner->hyperlink or 'javascript:void(0);' }}" target="{{ $banner->hyperlinkTarget }}" style="cursor: {{ $banner->hyperlink ? 'pointer' : 'default'}};">
								<img src="{{ asset($banner->imagePath()) }}" alt="BANNER IMAGE">
							</a>
						</li>
					@endforeach
				</ul>
			</div>
		</section>
	@endif --}}

	@if(count($products))
		<section class="product-promo">
			<div class="list-products">
				<div class="nav-slide-product">
					<div class="el-title-products">
						REFACCIONES EN <span>PROMOCIÓN</span>
					</div>
					<div  class="itemsPromo">

					</div>
				</div>
				<ul class="slideProducts">
					<li>
						@for($i = 1; $i <= count($products); $i++)
							<?php $product = $products[$i - 1]; ?>
							@include('partials.product', [ 'name' => $product->name ])

							@if($i == 6)
								@break;
							@endif
						@endfor
					</li>
					@if(count($products) > 6)
						<li>
                            <?php $product = $products[$i - 1]; ?>
							@for($i = 7; $i <= count($products); $i++)
								@include('partials.product', [ 'name' => $product->name ])
							@endfor
						</li>
					@endif
				</ul>
			</div>
		</section>
	@endif

	<section class="best-option">
		<div class="image-option">
			<img src="{{ asset('images/mechanic.png') }}">
		</div>
		<div class="detail-options">
			<span class="titleDetails">¿Por qué somos <br><strong>la mejor opción</strong>?</span>
			<ul>
				<li>
					<span class="icon"><img src="{{ asset('images/pays.png') }}"></span>
					<span>Múltiples formas de pago</span>
				</li>
				<li>
					<span class="icon"><img src="{{ asset('images/seends.png') }}"></span>
					<span>Envios a todo México</span>
				</li>
				<li>
					<span class="icon"><img src="{{ asset('images/atention.png') }}"></span>
					<span>Atención personalizada</span>
				</li>
			</ul>
		</div>
	</section>

	<section class="newsletter">
		<div class="msg-newsletter">
			Recibe ofertas y promociones <span>Suscribete a nuestro boletín</span>
		</div>
		<form>
			<label>
				<input type="text" placeholder="Escribe tu e-mail">
			</label>
			<button class="uHover">
				SUSCRIBIRME
			</button>
		</form>
	</section>

	@include('partials.ways-to-pay')

@endsection

@section('js')

	<script type="text/javascript">
		$(document).on('ready', initSliders);

		function initSliders()
		{
		    var touchEnabled = false;

            var md = new MobileDetect(window.navigator.userAgent);
            if (md.mobile()) {
                touchEnabled = true;
            }

			$('.slideHome').bxSlider({
                adaptiveHeight: true,
				auto: true,
                autoHover: true,
                hideControlOnEnd: true,
				pause: 4000,
                touchEnabled: touchEnabled
			});

			$('.slideProducts').bxSlider({
				speed: 1000
			});
		}
	</script>

@endsection
