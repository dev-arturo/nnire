@section('css')
	<link rel="stylesheet" href="{{asset('css/hiddenSearch.css')}}">
@endsection

@extends('layouts.app')

@section('content')
	
	<section id="loginContainer">

        @if(Session::has('warning'))
    		<div class="container">
    			<div class="row" style="margin-top: 15px;">
    				<div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
    					<div id="charge-message" class="alert alert-danger">
    						{{Session::get('warning')}}
    					</div>
    				</div>
    			</div>
    		</div>
    	@endif

		<div class="login-user-title">
			Crear Cuenta
		</div>

		<div class="container-register register-box">
			<div class="login-section">
				<div class="title-register">
					<span>Por favor, introduce la siguiente información para crear tu cuenta</span>
				</div>	
				
				<div class="desc-container">
					{{ Form::open(['route' => 'reg', 'class' => 'login-form', 'id' => 'card-form']) }}
					{!! csrf_field() !!}
						<label class="form-type-1">
							<span>Nombre *</span>
							<input name="name" required>
						</label>
						<label class="form-type-1">
							<span>Apellidos *</span>
							<input name="last-name" required>
						</label>
						<label class="form-type-1">
							<span>Correo electronico *</span>
							{{ Form::email('email', null, ['class' => $errors->has('email') ? 'parsley-error' : '', 'required'=>true])}}
							@if($errors->has('email'))
                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('email') }}</li></ul>
                            @endif
						</label>
						<label class="form-type-1">
							<span>Contraseña *</span>
							{{ Form::password('password', ['class' => $errors->has('password') ? 'parsley-error' : '', 'required'=>true])}}
							@if($errors->has('password'))
                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('password') }}</li></ul>
                            @endif
						</label>
						<label class="form-type-1">
							<span>Confirmar contraseña *</span>
							{{ Form::password('password_confirmation', ['class' => $errors->has('password_confirmation') ? 'parsley-error' : '', 'required'=>true])}}
							@if($errors->has('password_confirmation'))
                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('password_confirmation') }}</li></ul>
                            @endif
						</label>
						<br>
						{{-- <div class="paymentcards" >
							<div id="card-success" class="hidden">
							  <i class="fa fa-check"></i>
							  <p>Payment Successful!</p>
							</div>
							<div id="form-errors" class="hidden">
							  <i class="fa fa-exclamation-triangle"></i>
							  <p id="card-error">Card error</p>
							</div>
							<div id="form-container">

							  <div id="card-front">
							    <div id="shadow"></div>
							    <div id="image-container">
							      <span id="card-image">
							      
							        </span>
							    </div>

							    <label for="card-number">
							        Card Number
							      </label>
							    <input type="text" id="card-number" size="20" placeholder="1234 5678 9101 1112" data-conekta="card[number]" required>
							    <div id="cardholder-container">
							      <label for="card-holder">Card Holder
							      </label>
							      <input type="text" id="card-holder" size="20" placeholder="e.g John Doe" data-conekta="card[name]" required>
							    </div>
							    <div id="exp-container">
							      <label for="card-exp">
							          Expiration
							        </label>
							      <input id="card-month" type="text" placeholder="MM" length="2" data-conekta="card[exp_month]" required>
							      <input id="card-year" type="text" placeholder="YY" length="2" data-conekta="card[exp_year]" required>
							    </div>
							        <div id="cvc-container">
							      <label for="card-cvc"> CVC/CVV</label>
							      <input id="card-cvc" placeholder="XXX-X" type="text" min-length="3" max-length="4" data-conekta="card[cvc]" required>
							    </div>
							    
							  </div>
							  
							  <div id="card-back">
							    <div id="card-stripe">
							    </div>

							  </div>
							  
							  <input type="text" id="card-token" />

							</div>
						</div> --}}

						<br>
						<div class="row" >
						  <div class="col-md-6 col-md-offset-3">
						  	<div class="g-recaptcha" data-sitekey="6Lc9OkMUAAAAAM75Tb5FGWR6gfkE2XbUfDjT4FTN"></div>
						  	@if($errors->has('g-recaptcha-response'))
                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('g-recaptcha-response') }}</li></ul>
                            @endif
						  </div>
						</div>
						<br>
						<label class="form-type-2">
							<input type="checkbox" name="terms" required>
							<span>Aceptas Términos y Condiciones y Aviso de privacidad</span>
						</label>
						<label class="form-type-2">
							<input type="checkbox" name="news">
							<span>Suscribete al Newsletter</span>
						</label>
						<div class="container-button btn-register">
							<button class="button-login uHover" type="submit">
								Crear mi cuenta
							</button>
						</div>
					<!-- </form> -->
					{{ Form::close() }}
				</div>
			</div>
		</div>

	</section>

@endsection

<script src="{{asset('js/card.js')}}"></script>

@section('js')
	{{-- <script type="text/javascript" src="https://cdn.conekta.io/js/latest/conekta.js"></script>

	<script>
		Conekta.setPublicKey('key_DyHVRaTynnJseqPSLsPtgxA');

        var conektaSuccessResponseHandler = function(token) {
	    var $form = $("#card-form");
	    //Inserta el token_id en la forma para que se envíe al servidor
	    $form.append($('<input type="hidden" name="conektaTokenId" id="conektaTokenId">').val(token.id));
	    $form.get(0).submit(); //Hace submit
	  };
	  var conektaErrorResponseHandler = function(response) {
	    var $form = $("#card-form");
	    $form.find(".card-errors").text(response.message_to_purchaser);
	    $form.find("button").prop("disabled", false);
	  };

	  //jQuery para que genere el token después de dar click en submit
	  $(function () {
	    $("#card-form").submit(function(event) {
	      var $form = $(this);
	      // Previene hacer submit más de una vez
	      $form.find("button").prop("disabled", true);
	      Conekta.Token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);
	      return false;
	    });
	  });
	</script> --}}
@endsection