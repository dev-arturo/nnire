@section('css')
	<link rel="stylesheet" href="{{asset('css/hiddenSearch.css')}}">
@endsection

@extends('layouts.app')

@section('content')

	<section id="textDescriptionContent">
		<p><strong> &iquest;C&oacute;mo compro a trav&eacute;s de refaccionesnissan.com.mx?</strong></p>
		<p><span style="font-weight: 400;">Si es la primera vez que compras con nosotros, deber&aacute;s de seguir los siguientes pasos: </span></p>
		<p><span style="font-weight: 400;">1. Crea una cuenta con tus datos.</span></p>
		<p><span style="font-weight: 400;">2. Busca y agrega productos a tu carrito d&aacute;ndole clic en el bot&oacute;n de Agregar.</span></p>
		<p><span style="font-weight: 400;">3. Finaliza tu orden en "Procesar Compra".</span></p>
		<p><span style="font-weight: 400;">4. Selecciona tu forma de pago.</span></p>
		<p><span style="font-weight: 400;">5. Paga en el lugar de tu elecci&oacute;n (Banco, Oxxo o 7-eleven)</span></p>
		<p><span style="font-weight: 400;">6. Realiza tu pago.</span></p>
		<p><span style="font-weight: 400;">7. Informa tu pago recibe tu producto en tu domicilio.</span></p>
		<p>&nbsp;</p>
		<p><strong> &iquest;C&oacute;mo creo una cuenta o me registro en refaccionesnissan.com.mx?</strong></p>
		<p><span style="font-weight: 400;">Haz clic en el bot&oacute;n &ldquo;reg&iacute;strate&rdquo; ubicado en la parte superior derecha de nuestro portal. Completa todos los campos con tus datos y haz clic en &ldquo;registrarme&rdquo; o si lo prefieres, puedes registrarte dando clic en el bot&oacute;n Registrarme con Facebook y &iexcl;listo! ya tienes una cuenta en refaccionesnissan.com.mx &nbsp;Recibir&aacute;s un correo electr&oacute;nico con un link que confirma tu registro, y deber&aacute;s dar clic en el link, para confirmar que eres t&uacute; el usuario y habilitar tu cuenta. (Esto es por protecci&oacute;n a cuentas o usuarios falsos). Una vez despu&eacute;s de crear tu cuenta, es muy f&aacute;cil regresar a nuestro portal y volver a entrar, solo tienes que introducir tu nombre de usuario y contrase&ntilde;a &iexcl;y listo! &iexcl;Bienvenido!, ya eres parte de la comunidad de</span> <span style="font-weight: 400;">refaccionesnissan.com.mx &nbsp;&nbsp;&nbsp;&nbsp;</span></p>
		<p>&nbsp;</p>
		<p><strong> &iquest;Puedo comprar sin estar registrado?</strong></p>
		<p><span style="font-weight: 400;">Por seguridad, debes registrarte al momento de procesar tu compra. Una vez que realices tu registro podr&aacute;s proporcionarnos tus datos completos para enviar tu producto de manera &aacute;gil y al mismo tiempo generas un acceso r&aacute;pido para futuras compras ya que tu registro solo lo tienes que realizar en tu primer compra con nosotros.&nbsp; La informaci&oacute;n que proporcionas en tu registro es para uso exclusivo del env&iacute;o, no compartimos nuestra base de datos con ninguna instituci&oacute;n p&uacute;blica o privada.</span></p>
		<p>&nbsp;</p>
		<p><strong> &iquest;C&oacute;mo se que Refacciones nissan recibi&oacute; mi pedido?</strong></p>
		<p><span style="font-weight: 400;">Cuando se procesa una compra, en autom&aacute;tico recibes un correo electr&oacute;nico que confirma tu orden, esto te garantiza que has realizado correctamente el proceso, en cuanto se vea reflejado tu pago, tu producto ser&aacute; enviado al domicilio que nos indicaste.</span></p>
		<p>&nbsp;</p>
		<p><strong> &iquest;En d&oacute;nde puedo pagar?</strong></p>
		<p><span style="font-weight: 400;">Pensando en tu comodidad&nbsp; tenemos la facilidad de que puedas pagar tus compras en cualquiera de los siguientes medios, t&uacute; podr&aacute;s elegir el que sea de tu conveniencia:&nbsp; </span></p>
		<ul>
		<li style="font-weight: 400;"><span style="font-weight: 400;">Dep&oacute;sitos y/o Transferencias. (Scotiabank, Bancomer, HSBC, IXE, Banorte,Santander).</span></li>
		<li style="font-weight: 400;"><span style="font-weight: 400;">Tiendas de Conveniencia. (Oxxo, 7-eleven).</span></li>
		<li style="font-weight: 400;"><span style="font-weight: 400;">Paypal para pago con tarjeta de cr&eacute;dito o d&eacute;bito</span></li>
		</ul>
		<p>&nbsp;</p>
		<p><strong> &iquest;C&oacute;mo puedo pagar en Oxxo y 7- eleven?</strong></p>
		<p><span style="font-weight: 400;">Es muy sencillo, mientras est&eacute;s procesando tu pedido, solo tienes que seleccionar en forma de pago "Oxxo y/o 7-eleven" se te proporcionar&aacute;n datos para realizar el d&eacute;posito como: </span></p>
		<ul>
		<li style="font-weight: 400;"><span style="font-weight: 400;">Nombre del titular</span></li>
		<li style="font-weight: 400;"><span style="font-weight: 400;">N&uacute;mero de tarjeta </span></li>
		<li style="font-weight: 400;"><span style="font-weight: 400;">N&uacute;mero de cuenta</span></li>
		</ul>
		<p><span style="font-weight: 400;">que tendr&aacute;s que imprimir y presentar en la tienda de tu conveniencia. Una vez pagado para nosotros es importante que nos informes de tu pago y as&iacute; poderlo confirmar en nuestro sistema y hacer el env&iacute;o de tu pedido.</span></p>
		<p>&nbsp;</p>
		<p><strong> &iquest;Cu&aacute;nto es el costo de env&iacute;o y tiempo de entrega?</strong></p>
		<p><span style="font-weight: 400;">Si necesitas un env&iacute;o el costo es de $100.00 pesos (siempre y cuando tengamos cobertura con paqueteexpress).</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Se realiza el cargo si es que as&iacute; lo seleccionas dentro de tu proceso de compra. </span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Nuestro compromiso es enviar tu paquete en menos de 48 horas una vez confirmado y recibido el pago, sin embargo, la paqueter&iacute;a puede tardar hasta 7 d&iacute;as en entregar tu orden. &nbsp;o bien, si lo prefieres, puedes pasar a recoger tu pedido directamente a nuestra oficinas ubicadas en: Av. Lopez Mateos #1460 Col. Chaplita C.P. 44500, Guadalajara, Jalisco. Horario de atenci&oacute;n: 9:00 a 19.30 hrs Tel.33-3880-6644/19/18 Whatsapp:33-3800-6129.</span></p>
		<p><span style="font-weight: 400;">&nbsp;</span></p>
		<p><strong> &iquest;Cu&aacute;l es la empresa responsable del env&iacute;o de productos?</strong></p>
		<p><span style="font-weight: 400;">&nbsp; En todos nuestros env&iacute;os nos respalda la empresa Paqueteexpress.</span></p>
		<p>&nbsp;</p>
		<p><strong> &iquest;C&oacute;mo s&eacute; el estatus de mi pedido?</strong></p>
		<p><span style="font-weight: 400;">Para revisar el estatus de la entrega de tu pedido es necesario solicitar tu n&uacute;mero de gu&iacute;a al &aacute;rea de atenci&oacute;n a clientes, para poder rastrear el paquete directamente con la paqueter&iacute;a en su sistema de rastreo y localizaci&oacute;n visitando su p&aacute;gina web: </span><a href="http://www.paquetexpress.com.mx"><span style="font-weight: 400;">www.paquetexpress.com.mx</span></a></p>
		<p>&nbsp;</p>
		<p><strong> &iquest;Puedo pasar a recoger mis productos a alg&uacute;n lugar?</strong></p>
		<p><span style="font-weight: 400;">Claro, puedes pasar por tu producto a nuestras oficinas, solo que para esto debes de informar al &aacute;rea de atenci&oacute;n a clientes que d&iacute;a pasar&iacute;as para que tengan la pieza en nuestra oficina y evitar contratiempos.</span></p>
		<p>&nbsp;</p>
		<p><strong> &iquest;Es seguro comprar en Autoenpartes.mx?</strong></p>
		<p><span style="font-weight: 400;">Comprar en Autoenpartes.mx es totalmente seguro ya que nos apoyamos en la compa&ntilde;&iacute;a Paypal la cual garantiza y protege todas las transacciones que hagas con nosotros&nbsp;mediante un sistema encriptado de datos, y te permite diferentes opciones de pago.</span></p>
		<p>&nbsp;</p>
		<p><strong> &iquest;Cu&aacute;l es el horario de atenci&oacute;n a clientes?</strong></p>
		<p><span style="font-weight: 400;">Nuestro horario telef&oacute;nico es de Lunes a Viernes de 9:00 am a 7:30 pm s&aacute;bado de 9:00 am a 14:00 pm, sin embargo nuestro portal funciona las 24 horas del d&iacute;a los 365 d&iacute;as del a&ntilde;o y los env&iacute;os se realizan de lunes a S&aacute;bado</span></p>
		<p>&nbsp;</p>
		<p><strong> &iquest;Pueden facturarme los productos?</strong></p>
		<p><span style="font-weight: 400;">En caso de requerir factura de su compra, antes de pagar el importe de su orden, deber&aacute; de mandar un correo con sus datos de facturaci&oacute;n solicit&aacute;ndola a Jorge.duenas@nissanchapalita.com.mx&nbsp; en donde se le dar&aacute; un n&uacute;mero de cuenta al cual ser&iacute;a el dep&oacute;sito. Y posteriormente enviarnos un correo con su factura.</span></p>
		<p>&nbsp;</p>
		<p><strong> &iquest;Cu&aacute;les son las pol&iacute;ticas de devoluci&oacute;n?</strong></p>
		<p><span style="font-weight: 400;">Podr&aacute;s devolver cualquier art&iacute;culo comprado con nosotros por las siguientes causas:</span></p>
		<ul>
		<li style="font-weight: 400;"><span style="font-weight: 400;">Si alguno de los art&iacute;culos presenta defectos de fabricaci&oacute;n.</span></li>
		<li style="font-weight: 400;"><span style="font-weight: 400;">Si existe equivocaci&oacute;n en el producto enviado, siempre y cuando conserve su envoltura o empaque.</span></li>
		</ul>
		<p><span style="font-weight: 400;">Aseg&uacute;rese que el producto que ha seleccionado es para su veh&iacute;culo o es lo que usted desea, si usted cuenta con alguna duda favor de contactarnos en Jorge.duenas@nissanchapalita.com.mx, ya que no se aceptan devoluciones por alguna otra causa a las antes mencionadas.</span></p>
	</section>

@endsection

@section('js')

@endsection
