@extends('layouts.app')

@section('content')

	<section id="loginContainer">





			<div class="container">

				<div class="row form-group">
			        <div class="col-xs-12">
			            <ul class="nav nav-pills nav-justified thumbnail setup-panel">
			                <li class="">
			                	<a href="{{route('pay')}}">
				                    <h4 class="list-group-item-heading">Dirección</h4>
				                    <p class="list-group-item-text">Seleccionar dirección de envio</p>
			                	</a>
			           		</li>
			                <li class="active">
			                	<a href="#">
				                    <h4 class="list-group-item-heading">Pago</h4>
				                    <p class="list-group-item-text">Paga con tú método de preferencia</p>
			                	</a>
			            	</li>
			            </ul>
			        </div>
				</div>


			    <div class="row setup-content" id="step-1">

			    </div>
			    <div class="row setup-content" id="step-2">
			        <div class="col-xs-12">
			            <div class="col-md-12 well">
			                <div class="login-section">
			                	<div class="pay-section">
								<span class="title">MÉTODO DE PAGO</span>
								<div class="desc-container">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                      <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                          <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                              <i class="fa fa-angle-down"></i>Pagar con Oxxo Pay
                                            </a>
                                          </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                          <div class="panel-body">
                                            {{ Form::open(['route' => 'payOxxoConekta', 'class' => '']) }}
                                                <input type="text" name="amount" value="{{$totalPrice}}" hidden style="display: none;">
                                            	<input type="text" name="adress_id" value="{{$adress}}" hidden style="display: none;">
                                    			<input type="image" src="{{asset('images/oxxo_pay_Grande.png')}}" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea." style="cursor: pointer;" width="200px">
                                    		{{ Form::close() }}
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingTwo">
                                          <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                              Paypal (Tarjeta de débito/crédito)
                                            </a>
                                          </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                          <div class="panel-body">
                                            <div class="option-choice-paypal">
                                                <div class="element-choice desc-container">
                                                    <div class="form-paypal">
                                                        {{ Form::open(['route' => 'paypalp', 'class' => '']) }}
                                                        <input type="text" name="amount" value="{{$totalPrice}}" hidden style="display: none;">
                                                        <input type="text" name="adress_id" value="{{$adress}}" hidden style="display: none;">
                                                        <input type="image" src="https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea." style="cursor: pointer;">
                                                        <img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="30" height="30">
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingThree">
                                          <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                              Tarjeta de crédito (Conekta)
                                            </a>
                                          </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                          <div class="panel-body">
                                            <div style="margin: 20px;">
                                    	        <div class='row'>
                                    	            <div class='col-md-6 col-md-offset-3'>
                                    	                <form action="{{route('payConekta')}}" method="POST" id="card-form">
                                    	                    {{ csrf_field() }}
                                    	                    <input type="text" name="adress_id" value="{{$adress}}" hidden style="display: none;">
                                    	                    <div class='form-row'>
                                    	                        <div class='col-xs-12 form-group required'>
                                    	                            <label class='control-label'>Nombre</label>
                                    	                            <input class='form-control' size='4' type='text' data-conekta="card[name]" required>
                                    	                        </div>
                                    	                    </div>
                                    	                    <div class='form-row'>
                                    	                        <div class='col-xs-12 form-group card required'>
                                    	                            <label class='control-label'>Número de tarjeta</label>
                                    	                            <input autocomplete='on' class='form-control card-number' size='20' type='text' data-conekta="card[number]" required>
                                    	                        </div>
                                    	                    </div>
                                    	                    <div class='form-row'>
                                    	                        <div class='col-xs-4 form-group cvc required'>
                                    	                            <label class='control-label'>CVC</label>
                                    	                            <input type="password" autocomplete='off' class='form-control card-cvc' placeholder='ex. 311' size='4' type='text' data-conekta="card[cvc]" required>
                                    	                        </div>
                                    	                        <div class='col-xs-4 form-group expiration required'>
                                    	                            <label class='control-label'>Expiración</label>
                                    	                            <input class='form-control card-expiry-month' placeholder='MM' size='2' type='text' data-conekta="card[exp_month]" required>
                                    	                        </div>
                                    	                        <div class='col-xs-4 form-group expiration required'>
                                    	                            <label class='control-label'>&nbsp;</label>
                                    	                            <input class='form-control card-expiry-year' placeholder='YYYY' size='4' type='text' data-conekta="card[exp_year]" required>
                                    	                        </div>
                                    	                    </div>
                                    	                    <div class='form-row'>
                                    	                        <div class='col-md-12 form-group'>
                                    	                            <button class='form-control btn btn-primary submit-button' type='submit'>Pagar &raquo;</button>
                                    	                        </div>
                                    	                    </div>
                                    	                </form>
                                    	            </div>
                                    	        </div>
                                    	    </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
								</div>
								</div>
							</div>
						</div>
			            </div>
			        </div>
			    </div>
			</div>

	</section>
@endsection

@section('js')

	<script type="text/javascript" src="https://cdn.conekta.io/js/latest/conekta.js"></script>

	<script>
		Conekta.setPublicKey('key_fEFNYtVBGQr6wdKDQSQShBg');

        var conektaSuccessResponseHandler = function(token) {
	    var $form = $("#card-form");
	    //Inserta el token_id en la forma para que se envíe al servidor
	    $form.append($('<input type="hidden" name="conektaTokenId" id="conektaTokenId">').val(token.id));
	    $form.get(0).submit(); //Hace submit
	  };
	  var conektaErrorResponseHandler = function(response) {
	    var $form = $("#card-form");
	    if(response.object == 'error'){
            toastr.error(response.message_to_purchaser);
        }
	    $form.find(".card-errors").text(response.message_to_purchaser);
	    $form.find("button").prop("disabled", false);
	  };

	  //jQuery para que genere el token después de dar click en submit
	  $(function () {
	    $("#card-form").submit(function(event) {
	      var $form = $(this);
	      // Previene hacer submit más de una vez
	      $form.find("button").prop("disabled", true);
	      Conekta.Token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);
	      return false;
	    });
	  });
	</script>

@endsection
