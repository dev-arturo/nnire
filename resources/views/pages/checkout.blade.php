@section('css')
	<link rel="stylesheet" href="{{asset('css/hiddenSearch.css')}}">
@endsection

@extends('layouts.app')

@section('content')
	
	<section class="checkout-info">
		<div class="nav-section-checkout">
			<span>Checkout</span>
		</div>
		
			@if(Session::has('error'))
				<div class="">
					<div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
						<div id="charge-message" class="alert alert-warning">
							{{Session::get('error')}}
						</div>
					</div>
				</div>
			@endif
	
		<div class="table-products">
			<div class="header-table">
				<div class="photo-nav"></div>
				<div class="code-nav">Código</div>
				<div class="product-nav">Producto</div>
				<div class="count-nav">Cantidad</div>
				<div class="price-nav">P/U</div>
				<div class="subtotal-nav">Subtotal</div>
				<div class="actions-nav"></div>
			</div>
			<div class="items-products">
				@if(Session::has('cart'))
					@foreach($products as $product)
						<div class="items-product">
							<div class="photo-item">
								{{-- {{dd($product['item']['image'][0])}} --}}
								@if($product['item']['image'] != null)
									<img src="{{ asset('storage/products/'. $product['item']['image'][0]) }}" alt="">
								@else
									<img src="{{ asset('images/placeholder.jpg') }}" alt="placeholderImage">
								@endif
							</div>
							<div class="code-item">
								<span>{{$product['item']['sku']}}</span>
							</div>
							<div class="product-item">
								<p>{{$product['item']['name']}}</p>
							</div>
							<div class="count-item" >
								{{-- <button style="width: 20%;">-</button> --}}
								
								
								{{ Form::open(['route' => 'change-item-cart', 'style' => 'padding-top: 10px;']) }}
									<input id="{{$product['item']['sku']}}" name="id" type="text" value="{{$product['item']['id']}}" hidden style="display: none;">
									<input class="qtyItems" name="quantity" type="number" min="1" value="{{$product['qty']}}" style="width: 80%; height: 10px;" required="true">
								{{ Form::close() }}
								{{-- <select name="quantity" id="{{$product['item']['id']}}" class="item_qty">
									@for ($i = 1; $i <= 10; $i++)
										@if($product['qty'] == $i)
								  			<option value="{{ $i }}" selected>{{$i}}</option>
										@else
											<option value="{{ $i }}">{{$i}}</option>
										@endif
									  
									@endfor
								</select> --}}
								{{-- <button style="width: 20%;">+</button> --}}
							</div>
							@if(auth()->user()->isAdmin == 3)
    							<div class="price-item">
    								<span>$ {{ number_format($product['item']['wholesale'], 2, '.', ',') }}</span>
    							</div>
    						@elseif(auth()->user()->isAdmin == 2)
    						    <div class="price-item">
    								<span>$ {{ number_format($product['item']['wholesale'], 2, '.', ',') }}</span>
    							</div>
    						@else
    						    <div class="price-item">
    								<span>$ {{ number_format($product['item']['price'], 2, '.', ',') }}</span>
    							</div>
    						@endif
							@if(auth()->user()->isAdmin == 3)
    							<div class="subtotal-item">
    								<span>$ {{ number_format($product['item']['wholesale'] * $product['qty'], 2, '.', ',') }}</span>
    							</div>
							@elseif(auth()->user()->isAdmin == 2)
    							<div class="subtotal-item">
    								<span>$ {{ number_format($product['item']['wholesale'] * $product['qty'], 2, '.', ',') }}</span>
    							</div>
    						@else
        						<div class="subtotal-item">
    								<span>$ {{ number_format($product['item']['price'] * $product['qty'], 2, '.', ',') }}</span>
    							</div>
							@endif
							<div class="actions-item">
								<span class="delete">
									<a href="{{route('remove-item', ['id'=>$product['item']['id']])}}">
										<img src="{{ asset('images/dustbin.png') }}">
									</a>
								</span>
							</div>
						</div>
					@endforeach
				@else
					<div class="items-product">
							<div class="photo-item">
								
							</div>
							<div class="code-item">
								
							</div>
							<div class="product-item">
								<p>No items in this cart!</p>
							</div>
							<div class="count-item">
								
							</div>
							<div class="price-item">
								
							</div>
							<div class="subtotal-item">
								
							</div>
							<div class="actions-item">
								<span class="delete">
									
								</span>
							</div>
						</div>
				@endif
			<div class="sub-table-items">
				<div class="coupon-insert">
					<div class="title-header-checkout openCoupon">
						<span>TIENES UN CUPÓN DE DESCUENTO?</span>
					</div>
					<div class="apply-coupon">
						{{ Form::open(['route' => 'applyCoupon', 'style' => 'padding-top: 10px;']) }}
							<input type="text" name="coupon" required="true" placeholder="Escribe el código del cupón">
							<button class="uHover">
								<img src="{{ asset('images/black-tag.png') }}">
								<span>Aplicar Cupón</span>
							</button>
						{{ Form::close() }}
					</div>
				</div>
				<div class="checkout-total">
					<div class="title-header-checkout">
						<span>SHOPPING CART TOTAL</span>
					</div>
					<div class="cont-info-checkout">
						<div class="subtotal-checkout">
							<span>Subtotal</span> <span class="price">$ {{ number_format($totalPrice, 2, '.', ',') }}
							</span>
						</div>
						<div class="discount-checkout">
							@if(isset($discount))
								<span>Descuento</span> <span class="price">$ {{ number_format($discount , 2, '.', ',') }}</span>
							@else
								<span>Descuento</span> <span class="price">$ 0.00</span>
							@endif
						</div>
						<div class="total-checkout">
							<span>Envio </span> <span class="price" id="">$ 0.00</span>
						</div>
						<div class="total-checkout">
							@if(isset($discount))
								<span>Total</span> <span class="price" id="totalprice">$ {{ number_format($totalPrice - $discount, 2, '.', ',') }}</span>
							@else
								<span>Total</span> <span class="price" id="totalprice">$ {{ number_format($totalPrice, 2, '.', ',') }}</span>
							@endif
						</div>
						
							
							@if(auth()->user()->isAdmin == 3)
							@elseif(auth()->user()->isAdmin == 2)
							    {{ Form::open(['route' => 'summary', 'style' => 'padding-top: 10px;']) }}
    							    <button class="uHover" type="submit">
        								<img src="{{ asset('images/correct-symbol.png') }}">
        								<span class="checkoutToPay">Comprar ahora</span>
        							</button>
    							{{ Form::close() }}
							@else
    							<a class="uHover" href="{{route('pay')}}">
    								<img src="{{ asset('images/correct-symbol.png') }}">
    								<span class="checkoutToPay">Comprar ahora</span>
    							</a>
							@endif
						

						<!-- <div>
							<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" >
							<input type="hidden" name="cmd" value="_s-xclick">
							<input type="hidden" name="hosted_button_id" value="X9PFKZ7J4SN8L">
							<input type="image" src="https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea." style="cursor: pointer;">
							<img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1">
							</form>
						</div> -->

					</div>
				</div>
				@if(auth()->user()->isAdmin == 3)
					<div class="checkout-total" style="margin-top: 15px;">
						<div class="title-header-checkout">
							<span>ASIGNAR PEDIDO A UN CLIENTE</span>
						</div>
						<div class="cont-info-checkout">

								{{ Form::open(['route' => 'addToClient', 'style' => 'padding-top: 10px;']) }}
									<div class="form-group" style="width: 75%; margin: 0 auto; padding-bottom: 10px;">
						                <select name="clients" id="selClients" class="form-control selectpicker select2" required="true" onchange="updateAdresses(this.value, '#adresses');">
						                	<option value=""></option>
						                	@foreach($clients as $client)
						                		<option value="{{$client->id}}">{{$client->name}} {{$client->surname}}</option>
						                	@endforeach
						                </select>
						            </div>

						            <div class="form-group" style="width: 75%; margin: 0 auto;">
						                <select name="adresses" id="adresses" class="form-control selectpicker select2" required="true">
						                	
						                </select>
						            </div>

						            <button class="uHover" type="submit">
										<img src="{{ asset('images/correct-symbol.png') }}">
										<span class="checkoutToPay">Asignar ahora</span>
									</button>

								{{ Form::close() }}
						</div>
					</div>
				@endif
			</div>
		</div>
	</section>

	@if(count($relatedProducts))
	<section class="container-products product-interest">

		<div class="list-products">
			<div class="nav-slide-product">
				<div class="el-title-products">
					OTROS PRODUCTOS QUE TE PUEDEN INTERESAR
				</div>
				<div  class="itemsPromo">
					
				</div>
			</div>
			<div class="group-products">
				@foreach($relatedProducts as $relatedProduct)
					@include('partials.product-interest', [ 'product' => $relatedProduct ])
				@endforeach
			</div>
		</div>
	</section>
	@endif

	@include('partials.need-help')

	@include('partials.ways-to-pay')

@endsection

@section('js')

	<script>
		$('.apply-coupon').slideUp(0);
		$('.openCoupon').click(function(){
			$('.apply-coupon').slideToggle();
		});

		jQuery(document).ready(function () {
	        $('.select2').select2();

	        $("#adresses").select2({
			    placeholder: "Selecciona una dirección:",
			    allowClear: true
			});

			$("#selClients").select2({
			    placeholder: "Selecciona un cliente:",
			    allowClear: true
			});

		    $(".qtyItems").popover({
		        placement: 'bottom',
		        html: 'true',
		        title : '<span class="text-info"><strong>Actualizar</strong></span>'+
		                '<button type="button" id="close" class="close" onclick="$(&quot;.qtyItems&quot;).popover(&quot;hide&quot;);">&times;</button>',
		        content : '<div class="row"><div class="col-md-12"><button type="submit" class="btn btn-success">Aceptar</button></div></div>'
		    });
	    });

       function updateAdresses(value, selector)
        {
        	console.log(value);
            $.ajax({
                type: 'get',
                url: '{{ route('ajax.loadAdress') }}/' + value,
                success: function (response) {
                	console.log(response);
                    $(selector).html(response);
                }
            });
        }

        @if(session()->has('warning'))
	        toastr.warning('{{ session('warning')['title'] }}', '{{ session('warning')['body'] }}', {
	        closeButton: true,
	        debug: false,
	        newestOnTop: true,
	        progressBar: true,
	        positionClass: "toast-top-right",
	        preventDuplicates: false,
	        onclick: null,
	        showDuration: "5000",
	        hideDuration: "6000",
	        showEasing: "swing",
	        hideEasing: "linear",
	        showMethod: "fadeIn",
	        hideMethod: "fadeOut"
	    });
	    @endif

	</script>

	

@endsection