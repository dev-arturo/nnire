@extends('layouts.app')

@section('content')
	
	<section id="loginContainer">
		
		<div class="container-user">
			
			<div class="menu-bar">
				<div class="option uHover">
					<a href="{{ route('my-account') }}" class="decoration" style="color:#a1a1a1;">
						Mi cuenta
					</a>
				</div>
				<div class="option uHover">
					<a href="{{ route('adresses') }}" class="decoration" style="color:#a1a1a1;">
						Información de envio
					</a>
				</div>
				<div class="option uHover selected">
					<a href="{{ route('orders') }}" class="decoration" style="color:white;">
						Tus Compras
					</a>
				</div>
				<div class="option uHover">
					<a href="{{ route('logout') }}" class="decoration">
						Cerrar sesión
					</a>
				</div>
			</div>

			<div class="info-menu-container">
				<div class="option-selected">
					<div class="title">
						Tus compras
					</div>
					@foreach($orders as $order)
					<div class="panel panel-primary"> 
						<div class="panel-heading">No. de pedido: <a style="text-decoration: none; color: white;" href="{{route('infoOrder', ['order' => $order->no_order])}}">{{$order->no_order}}</a></div>
						  <div class="panel-body">
						    <ul class="list-group" style="width: 100%;">
						    	@foreach($order->cart->items as $item)
						    		<li class="list-group-item">
						    			<span class="badge">$ {{number_format($item['price'], 2, '.', ',')}}</span>
						    			{{$item['item']['name']}} x {{$item['qty']}}
						    		</li>
						    	@endforeach
						    	@if($order->cart->coupon != '')
						    		<li class="list-group-item">
						    			<span class="badge">- $ {{number_format($order->descuento, 2, '.', ',')}}</span>
						    			Descuento
						    		</li>
						    	@endif()
						    </ul>
						  </div>
						  <div class="panel-footer">
						  	@if($order->cart->coupon != '')
							  	<strong>
							  		Total: $ {{number_format($order->cart->totalPrice - $order->descuento, 2, '.', ',')}}
							  	</strong>
							@else
								<strong>
							  		Total: $ {{number_format($order->cart->totalPrice, 2, '.', ',')}}
							  	</strong>
						  	@endif()
						  	<span class="pull-right">Fecha: {{$order->created_at}}</span>
						  </div>
					</div>
					@endforeach
				</div>
			</div>

		</div>	

	</section>

@endsection

