@section('css')
	<link rel="stylesheet" href="{{asset('css/hiddenSearch.css')}}">
@endsection

@extends('layouts.app')

@section('content')
	
	<section id="loginContainer">
		
		<div class="container-user">
			
			<div class="menu-bar">
				<div class="option selected uHover">
					<a href="{{ route('my-account') }}" class="decoration" style="color:white;">
						Mi cuenta
					</a>
				</div>
				{{-- <div class="option uHover">
					Información de cuenta
				</div> --}}
				<div class="option uHover">
					<a href="{{ route('adresses') }}" class="decoration" style="color:#a1a1a1;">
						Información de envio
					</a>
				</div>
				<div class="option uHover">
					<a href="{{ route('orders') }}" class="decoration" style="color:#a1a1a1;">
						Tus Compras
					</a>
				</div>
				{{-- <div class="option uHover">
					Wishlist
				</div> --}}
				<div class="option uHover">
					<a href="{{ route('logout') }}" class="decoration" style="color:#a1a1a1;">
						Cerrar sesión
					</a>
				</div>
			</div>

			<div class="info-menu-container">
				<div class="option-selected">
					<div class="title">
						Mi cuenta
					</div>
					<div class="welcome-msg">
						<span></span> Gracias por registrarte con Refacciones Nissan
					</div>
					<div class="info-details">
						<span class="subtitle">
							Hola {{ auth()->user()->fullName() }}
						</span>
						<span class="description">Desde aquí puedes ver tus actividades recientes y actualizar la informacion de tu cuenta.</span>
						<div class="menu-details-info">

							<div class="container-menu-info">
								<span class="title-menu-info">
									Datos de cuenta
								</span>
								<div class="form-user">
									<input value="{{ auth()->user()->email }}" disabled style="margin-bottom: 1em;">
									<div class="password-change">
									    {{ Form::open(['route' => 'changePassword', 'class' => '']) }}
    									    <label>
    											<span>Introduzca la contraseña actual</span>
    											<input type="password" name="actualPassword" required>
    										</label>
    										<label>
    											<span>Introduzca la contraseña nueva</span>
    											<input id="password" type="password" name="password" required>
    										</label>
    										<label>
    											<span>Confirma la contraseña nueva</span>
    											<input id="confirm_password" type="password" name="confirmationPassword" required>
    										</label>
    										<button type="submit" class="btn btn-danger">Actualizar</button>
                                		{{ Form::close() }}
                                        
									</div>
									
								</div>
								<span id="changePassword" class="action-element uHover">
									Cambiar contraseña
								</span>
							</div>

							{{-- <div class="container-menu-info">
								<span class="title-menu-info">
									Información de cuenta
								</span>
								<span class="action-element uHover">
									Modificar
								</span>
							</div> --}}

							<div class="container-menu-info">
								<span class="title-menu-info">
									Información de envio
								</span>
								<span class="action-element uHover">
									<a href="{{ route('adresses') }}" class="action-element" >
										Modificar
									</a>
								</span>
							</div>

							<div class="container-menu-info">
								<span class="title-menu-info">
									Tus compras
								</span>
								<span class="action-element uHover">
									<a href="{{ route('orders') }}" class="action-element" >
										Ver compras
									</a>
								</span>
							</div>

							{{-- <div class="container-menu-info">
								<span class="title-menu-info">
									Tus lista de deseos
								</span>
								<span class="action-element uHover">
									Ver deseos
								</span>
							</div> --}}

						</div>
					</div>
				</div>
			</div>

		</div>	

	</section>

@endsection

@section('js')

	<script type="text/javascript">
		
		$('.password-change').slideUp(0);

		$('#changePassword').click(function(){
			$('.password-change').slideDown();
		});
		
		var password = document.getElementById("password"), confirm_password = document.getElementById("confirm_password");
        
        function validatePassword(){
          if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Los campos no coinciden.");
          } else {
            confirm_password.setCustomValidity('');
          }
        }
        
        password.onchange = validatePassword;
        confirm_password.onkeyup = validatePassword;

	</script>

@endsection