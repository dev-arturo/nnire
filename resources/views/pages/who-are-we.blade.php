@extends('layouts.app')

@section('content')

	<section id="contact-header">
		<div class="container-contact-info">
			<div class="infor-contact">
				<h2>Refaccionesnissan.com.mx</h2>
				<p>Refaccionesnissan.com.mx</p>
				<p>Dirección: Av. Adolfo López Mateos Sur 1460, Colonia Chapalita</p>
				<p>C.P. 44500 Guadalajara, Jal.</p>
				<p>Teléfono: 01 33 3122 6677</p>
				<p>Whatsapp: (33) 38006129</p>
			</div>
			<div class="image-contact">
				<img src="{{asset('images/contact.png')}}">
			</div>
		</div>
	</section>

	<section id="contact-form">
		<div class="container-form">
			<div class="title-container-contact" style="text-align: justify;">
				<h1>¿Quienes somos?</h1>
				<p>Somos un Grupo Automotriz con más de cincuenta años de presencia en el Occidente del país representando marcas líderes y vanguardistas de alta calidad, durabilidad y confiabilidad.</p>
                <p>Nuestro enfoque es la satisfacción total del Cliente a través del esfuerzo integral, actuando siempre con sensibilidad, honestidad y ética, anticipándonos a sus necesidades y logrando prestar un servicio que exceda sus expectativas.</p>
                <p>Estamos comprometidos en el desarrollo humano de nuestros colaboradores en todas sus facetas impulsando su talento, esfuerzo y creatividad en un entorno de confianza y respeto mutuos.</p>
                <p>Todas nuestras acciones están orientadas a cuidar y respetar nuestro entorno de negocios, social y del ambiente que nos rodea.</p>
                
                <h1>Filosofía</h1>
                <p>Nuestra filosofía está fundamentada en la continua búsqueda de la lealtad de nuestros clientes, a través de un trabajo conjunto de todas las áreas de la empresa, actuando siempre con sensibilidad, honestidad y ética profesional, para anticiparnos a sus necesidades, intentado brindar un servicio superior a las expectativas del mercado, e innovando constantemente nuestros procesos, instalaciones y tecnología para lograr nuestras metas.</p>
                
                <h1>Misión</h1>
                <p>Nuestra MISION es lograr ser el Grupo Automotriz referente por su eficiencia, rentabilidad y responsabilidad, desarrollando equipos de trabajo dinámicos y comprometidos, utilizando sistemas de información, comunicación y control de última tecnología.</p>
                <h1>Visión</h1>
                <p>Ser el Grupo Automotriz mas respetado por nuestra congruencia, innovación y profesionalismo.</p>
                <h1>Historia</h1>
                <ul>
                    <li><strong>1933</strong> Jidosha Seizo Co., Ltd. se establece en Japón, como productora y distribuidora de partes y autos Datsun.</li>
                    <li><strong>1934</strong> La compañía se consolida y toma el nombre de Nissan Motor Co., Ltd.</li>
                    <li><strong>1959</strong> Nissan Motor Co. llega a México como distribuidora de autos de marca Datsun.</li>
                    <br>
                    <li><strong>1961</strong> Se constituye Nissan Mexicana, S.A. de C.V.</li>
                    <li><strong>1966</strong> Inicia operaciones la planta de Cuernavaca, la primera planta de Nissan establecida fuera de Japón. En ese año se produce el primer automóvil mexicano: Datsun Sedán Bluebird.1984 Cambia la imagen de Datsun a Nissan en todo el mundo.</li>
                    <li><strong>2000</strong> Nissan Mexicana llega a la producción de 3,000,000 vehículos. Se inicia la fabricación del primer vehículo dentro de la Alianza Renault-Nissan.</li>
                </ul>

                <h1>Valores</h1>
                <ul>
                    <li><strong>Servicio al cliente</strong>: La satisfacción de nuestros clientes, es el principal objetivo, excederemos las expectativas de ellos en todo momento.</li>
                    <li><strong>Responsabilidad</strong>: Como individuos debemos exigirnos total responsabilidad a nosotros mismos, y como colaboradores apoyamos la responsabilidad de los otros.</li>
                    <li><strong>Honestidad</strong>: Nuestro trato será honesto, justo y equitativo.</li>
                    <li><strong>Calidad</strong>: La calidad de nuestros recursos humanos y procedimientos, son el principal componente de nuestra empresa.</li>
                    <li><strong>Eficiencia</strong>: Utilizaremos nuestros recursos al máximo.</li>
                    <li><strong>Libertad</strong>: Necesitamos libertad para formar nuestro futuro; necesitamos utilidades para permanecer libres.</li>
                    <li><strong>Reciprocidad</strong>: Un beneficio mutuo es un beneficio compartido; un beneficio compartido perdurará.</li>
                    <li><strong>Tenacidad</strong>: Solo el trabajo continuo, consistente y comprometido nos llevará al éxito.</li>
                </ul>

			</div>
			<div class="form-container-contact">
				
			</div>
		</div>
	</section>


@endsection

@section('js')
	

@endsection
