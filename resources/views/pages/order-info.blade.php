@extends('layouts.app')

@section('content')
	
	<section id="loginContainer">
		
		<div class="container-user">
			
			<div class="menu-bar">
				<div class="option uHover">
					<a href="{{ route('my-account') }}" class="decoration" style="color:#a1a1a1;">
						Mi cuenta
					</a>
				</div>
				<div class="option uHover">
					<a href="{{ route('adresses') }}" class="decoration" style="color:#a1a1a1;">
						Información de envio
					</a>
				</div>
				<div class="option uHover selected">
					<a href="{{ route('orders') }}" class="decoration" style="color:white;">
						Tus Compras
					</a>
				</div>
				<div class="option uHover">
					<a href="{{ route('logout') }}" class="decoration">
						Cerrar sesión
					</a>
				</div>
			</div>

			

			<div class="info-menu-container">
				<div class="option-selected">

					<ol class="breadcrumb">
					  <li><a href="{{route('orders')}}">Compras</a></li>
					  <li class="active">{{$order->no_order}}</li>
					</ol>

					<div class="panel panel-primary">
					  <div class="panel-heading">
				        <div>
				    	    <h2 class="">Detalles de Pedido #{{$order->no_order}}</h2>
				        </div>
					    <!-- <h5 class="panel-title">Pago a través de paypal.</h5> -->
					  </div>
					  <div class="panel-body">
				    	<h1>{{ $adress->name }}</h1>
				    	<div class="row">
				    		<div class="col-md-4">
				    			<h5>Detalles Generales</h5>
				    			<div>
				    				<p><strong>Fecha del pedido:</strong></p>
				    				<span>{{$adress->created_at}}</span>
				    			</div>
				    			<br>
				    			<div>
				    				@if(isset($reference))
				    					<div>
				    						<p><strong>Tipo de pago:</strong></p>
				    						@if(isset($reference->charges[0]->payment_method->service_name))
							    				<span>{{$reference->charges[0]->payment_method->service_name}}</span>
				    						@else
				    							<span>{{$reference->charges[0]->payment_method->object == 'card_payment' ? 'Tarjeta de débito/crédito' : ''}}</span>
				    						@endif
				    					</div>
				    					<br>
					    				<div>
							    			@if(isset($reference->charges[0]->payment_method->service_name))
							    				<p><strong>Información de pago:</strong></p>
								    			<span>Referencia: {{$reference->charges[0]->payment_method->reference}}</span>
							    			@endif
					    				</div>
						    			<br>
						    			<div>
							    			<p><strong>Estado del pedido:</strong></p>
							    			<span><strong>{{$reference->payment_status == 'paid' ? 'Pagado' : 'En Proceso'}}</strong></span>
						    			</div>
						    		@else
						    			{{-- {{dd($order)}} --}}
						    			@if($order->paypal == 1)
							    			<div>
					    						<p><strong>Tipo de pago:</strong></p>
								    			<span>Paypal</span>
					    					</div>
							    			<br>
							    			<div>
								    			<p><strong>Estado del pedido:</strong></p>
								    			<span><strong>Pagado</strong></span>
							    			</div>
							    		@elseif($order->payment_id == 2)
											<div>
					    						<p><strong>Tipo de pago:</strong></p>
								    			<span>Depósito bancario</span>
					    					</div>
							    			<br>
							    			<div>
								    			<p><strong>Estado del pedido:</strong></p>
								    			<span><strong>{{ $order->estado == 1 ? 'Pagado y enviado.' : 'En proceso.' }}</strong></span>
							    			</div>
							    			<br>
							    			<div>
							    				@if(isset($order->no_guia))
									    			<p><strong>Número de guía:</strong></p>
									    			<span><strong>{{ $order->no_guia }} - {{$order->ship_company}}</strong></span>
								    			@endif()
							    			</div>
						    			@endif()
					    			@endif()
				    			</div>
				    			<br>
				    			<div>
					    			<p><strong>Cliente:</strong></p>
					    			<span>{{$adress->name}} {{$adress->surname}} - {{$userinfo->email}}</span>
				    			</div>
				    		</div>
				    		<div class="col-md-4">
				    			<h5>Detalles de facturación</h5>
				    			<p><strong>Dirección:</strong></p>
				    			<p>{{$adress->adress}}</p>
				    			<p>{{$adress->colony}}</p>
				    			<p>{{$adress->city}}</p>
				    			<p>{{$adress->state}}</p>
				    			<p>{{$adress->postal_code}}</p>
				    			<br>
				    			<p>Correo electrónico:</p>
				    			<a href="#">{{$userinfo->email}}</a>
				    			<p>Teléfono:</p>
				    			<a href="#">{{$adress->phone}}</a>
				    		</div>
				    		<div class="col-md-4">
				    			<h5>Detalles de envio</h5>
								<p><strong>Dirección:</strong></p>
				    			<p>{{$adress->adress}}</p>
				    			<p>{{$adress->colony}}</p>
				    			<p>{{$adress->city}}</p>
				    			<p>{{$adress->state}}</p>
				    			<p>{{$adress->postal_code}}</p>
				    		</div>
				    	</div>
					  </div>
					</div>
	

					<div class="panel panel-primary"> 
						<div class="panel-heading">No. de pedido: {{$order->no_order}}</div>
						  <div class="panel-body">
						    <ul class="list-group" style="width: 100%;">
						    	@foreach($order->cart->items as $item)
						    		<li class="list-group-item">
						    			<span class="badge">$ {{number_format($item['price'], 2, '.', ',')}}</span>
						    			{{$item['item']['name']}} | {{$item['qty']}}
						    		</li>
						    	@endforeach
						    	@if($order->cart->coupon != '')
						    		<li class="list-group-item">
						    			<span class="badge">- $ {{number_format($order->descuento, 2, '.', ',')}}</span>
						    			Descuento
						    		</li>
						    	@endif()
						    </ul>
						  </div>
						  <div class="panel-footer">
						  	@if($order->cart->coupon != '')
							  	<strong>
							  		Total: $ {{number_format($order->cart->totalPrice - $order->descuento, 2, '.', ',')}}
							  	</strong>
							@else
								<strong>
							  		Total: $ {{number_format($order->cart->totalPrice, 2, '.', ',')}}
							  	</strong>
						  	@endif()
						  	<span class="pull-right">Fecha: {{$order->created_at}}</span>
						  </div>
					</div>
				</div>
			</div>

		</div>	

	</section>

@endsection

