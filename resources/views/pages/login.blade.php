@section('css')
	<link rel="stylesheet" href="{{asset('css/hiddenSearch.css')}}">
@endsection

@extends('layouts.app')

@section('content')

	<section id="loginContainer">

		<div class="login-user-title">
			Inicia sesión o crea una cuenta
		</div>

		<div class="container-register">

			<div class="login-section">
			
				<span class="title">REGÍSTRATE</span>
				<div class="desc-container margin-desc-container">
					<span class="desc-sub">Al resgistrarte con nosotros obtienes grandes beneficios</span>
					<p>Compra de manera rápida</p>
					<p>Guarda tu direccion</p>
					<p>Consulta el Status de tú pedido</p>
					<div class="container-button">
						<a href="{{ route('register') }}">
							<span class="button-login uHover">
								Continuar
							</span>
						</a>
					</div>
				</div>

			</div>
            {{--
			<div class="login-section conect-login">

				<span class="title">CONECTATE</span>
				<div class="desc-container">
					<div class="container-button">
						<span class="button-login-facebook uHover">
							<img src="{{ asset('images/fb-login.png') }}">
						</span>
						<p class="footer-txt-login">Utiliza tus redes sociales para ingresar o crear una cuenta en el sitio</p>
					</div>
				</div>
			</div>
			--}}

		</div>
		
		<div class="container-register">
			
			<div class="login-section">

				<span class="title">YA ESTOY REGISTRADO</span>
				<div class="desc-container">
					<span class="desc-sub">Inicia sesión para realizar tu compra</span>
					{{ Form::open(['route' => 'log', 'class' => 'login-form']) }}
						<label class="form-type-1">
							<span>Correo electronico *</span>
							{{ Form::text('email', null, ['class' => $errors->has('email') ? 'parsley-error' : '', 'required'=>true])}}
							@if($errors->has('email'))
                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('email') }}</li></ul>
                            @endif
						</label>
						<label class="form-type-1">
							<span>Contraseña *</span>
							{{ Form::password('password', ['class' => $errors->has('password') ? 'parsley-error' : '', 'required'=>true])}}
							@if($errors->has('password'))
                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('password') }}</li></ul>
                            @endif
						</label>

						@if(session()->has('feedback'))
                       <ul class="parsley-errors-list filled" id="parsley-id-20">
                       		<li class="parsley-required" style="color:red;">{{ session()->get('feedback') }}</li>
                       	</ul>
                        @endif

					<div class="container-button">
						<a href="{{ route('recover') }}">
							<span class="button-password uHover">
								¿Olvidaste tu contraseña?
							</span>
						</a>
						<button class="button-login uHover" type="submit">
							Ingresar
						</button>
					</div>
					{{ Form::close() }}
				</div>
				
			</div>
            {{--
			<div class="login-section">
				<span class="title">INVITADO</span>
				<div class="desc-container">
					<div class="container-button">
						<a href="{{ route('pay') }}">
							<span class="button-login full-button uHover">
								Realiza tu compra como invitado
							</span>
						</a>
					</div>
				</div>
			</div>
            --}}
        
		</div>
	</section>

@endsection

@section('js')

@endsection

