@extends('layouts.app')

@section('content')

	<section class="productDetails">

		<div class="productDtls">
			<h2>{{ $product->name }}</h2>
			<div class="productDesc">
				<span class="sku">SKU: {{ $product->sku }}</span> /
				<span class="model">MODELO: {{ $carModel }}</span> /
				<span class="ean">EAN/UPC: {{ $product->eanUpc }}</span>
			</div>
			<div class="galleryProduct">
				<div class="thumbs">
					<ul>
						{{-- {{dd($product)}} --}}
						@if($product->image)
							@foreach($product->image as $img)
								<li class="uHover selected">
									<img src="{{ asset('storage/products/'.$img) }}" alt="">
								</li>
							@endforeach
						@else
							<li class="uHover selected">
								<img src="{{ asset($product->placeholderImage()) }}" alt="">
							</li>
						@endif
					</ul>
				</div>
				<div class="imageLarge">
					@if($product->hasImage())
						<figure class="zoom" onmousemove="zoom(event)" style="background-image: url({{asset($product->imagePath())}})">
							<img src="{{ asset($product->imagePath()) }}" />
						</figure>
					@else
						<img src="{{ asset($product->placeholderImage()) }}" alt="">
					@endif
				</div>
			</div>
			<div class="descDetails">
				<div class="navDetails">
					<span class="selected">Descripción</span>
					<span>Ficha Técnica</span>
				</div>
				<div class="complete-info">
					<div class="description-info selected">
						<span>Beneficios y Características</span>
						<p>
							{!! nl2br($product->description) !!}
						</p>
					</div>
					<div class="tech-info">
						<span>Ficha Tecnica</span>
						<p>{{ $product->carModel->name }}</p>
						<p>Años: 
							@if($product->years)
								@foreach(unserialize($product->years) as $item)
									<span>{{$item->year}}</span>
								@endforeach
							@endif
						</p>
						<br>
						@if($product->dataSheet)
							<p>
								{!! nl2br($product->dataSheet) !!}
							</p>
						@endif
					</div>
				</div>
			</div>
		</div>

		<div class="addtocart">
			<div class="add">
				{{ Form::open(['route' => 'add-to-cart', 'class' => '']) }}
					
					@if(auth()->check() && auth()->user()->isAdmin == 3)
                		<div class="priceProduct">
                			<span>${{ number_format($product->wholesale, 2, '.', ',') }}</span>
                		</div>
                	@elseif(auth()->check() && auth()->user()->isAdmin == 2)
                    	<div class="priceProduct">
                    		<span>${{ number_format($product->wholesale, 2, '.', ',') }}</span>
                    	</div>
                	@else
                		<div class="priceProduct">
                			${{ number_format($product->price, 2, '.', ',') }}
                		</div>
                	@endif
					
					
					<input type="text" hidden value="{{$product->id}}" name="id">
					<div class="priceProduct" >
						<span>Cantidad: </span>
						<input name="quantity" type="number" value="1" min="1" max="99" style="width: 30%;">
						
						<button class="uHover" type="submit" style="background-color: #e74431; color: white; margin-top: 25px;">
						    <img src="{{asset('images/cart.png')}}" alt="cart" style="width: 30px; margin-right: 15px;">
    						AGREGAR AL CARRITO
    					</button>
					</div>
				{{ Form::close() }}
				
				<button id="productBack" class="btn btn-lg btn-primary uHover center-block">Regresar</button>
			</div>
		</div>

	</section>

	@if(count($relatedProducts))
		<section class="container-products">

			<div class="list-products">
				<div class="nav-slide-product">
					<div class="el-title-products">
						PRODUCTOS RELACIONADOS
					</div>
					<div  class="itemsPromo">

					</div>
				</div>
				<div class="group-products">
					@foreach($relatedProducts as $relatedProduct)
						@include('partials.product', ['product' => $relatedProduct])
					@endforeach
				</div>
			</div>

		</section>
	@endif

	@include('partials.need-help')

	@include('partials.ways-to-pay')

@endsection

@section('js')
	<script>

		function zoom(e){
		  var zoomer = e.currentTarget;
		  e.offsetX ? offsetX = e.offsetX : offsetX = e.touches[0].pageX;
		  e.offsetY ? offsetY = e.offsetY : offsetX = e.touches[0].pageX;
		  x = offsetX/zoomer.offsetWidth*100;
		  y = offsetY/zoomer.offsetHeight*100;
		  zoomer.style.backgroundPosition = x + '% ' + y + '%';
		}

		$("#productBack").click(function () {
			window.history.back();
		});
		
		$('.thumbs li').click(function(){
			var pos = $(this).index();
			$('.thumbs li').removeClass('selected');
			$('.thumbs li:nth-child('+(pos+1)+')').addClass('selected');
			$('.imageLarge img').attr('src', $('img',this).attr('src'));
			$('.imageLarge figure').css('backgroundImage','url('+ $('img',this).attr('src') +')');
		});

		$('.navDetails span').click(function(){
			var pos = $(this).index();
			$('.navDetails > span').removeClass('selected');
			$('.complete-info > div').removeClass('selected');
			$('.navDetails > span:nth-child('+(pos+1)+')').addClass('selected');
			$('.complete-info > div:nth-child('+(pos+1)+')').addClass('selected');
		});

	</script>


@endsection