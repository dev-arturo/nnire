@extends('layouts.app')

@section('content')
	
	<section id="loginContainer">

		
				
			

			<div class="container">

				@if(Session::has('success'))
					<div class="row">
						<div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
							<div id="charge-message" class="alert alert-success">
								{{Session::get('success')}}
							</div>
						</div>
					</div>
				@endif

				
				<div class="row form-group">
			        <div class="col-xs-12">
			            <ul class="nav nav-pills nav-justified thumbnail setup-panel">
			                <li class="active">
			                	<a href="#step-1">
				                    <h4 class="list-group-item-heading">Dirección</h4>
				                    <p class="list-group-item-text">Seleccionar dirección de envio</p>
			                	</a>
			           		</li>
			                <li class="disabled">
			                	<a href="#step-2">
				                    <h4 class="list-group-item-heading">Pago</h4>
				                    <p class="list-group-item-text">Paga con tú método de preferencia</p>
			                	</a>
			            	</li>
			            </ul>
			        </div>
				</div>
				

			    <div class="row setup-content" id="step-1">
			        <div class="col-xs-12">
			            <div class="col-md-12 well">
			                <div class="login-section">
			                	
			                	<div class="row">
			                		@foreach($adresses as $adress)
									  <div class="col-sm-6 col-md-4">
									    <div class="thumbnail">
									      <div class="caption">
									        <h4>{{$adress->name}} {{$adress->surname}}</h4>
									        <p>{{$adress->adress}}</p>
									        <p>{{$adress->colony}}</p>
									        <p>{{$adress->city}}, {{$adress->state}}, {{$adress->postal_code}}</p>
									        <p>{{$adress->phone}}</p>
									        <br>
									        <p>
									        	{{ Form::open(['route' => 'paycheck', 'class' => 'login-form']) }}
									        	<div class="adressRemove">
										        	<input class="adressId" type="text" name="adress" value="{{$adress->id}}" hidden style="display: none;">
										        	<button href="#" class="btn btn-danger" type="submit">Entregar en esta dirección</button>
										        	<a href="#" class="btn waves-effect waves-light btn-danger" id="{{$adress->id}}" >
						                                <i class="fa fa-trash"></i>
						                            </a>
									        	</div>
					                            {{ Form::close() }}
					                        </p>
									      </div>
									    </div>
									  </div>
									@endforeach
								</div>

								<span class="title">INFORMACIÓN DE COMPRA</span>
								<div class="desc-container">
									{{ Form::open(['route' => 'createAdress', 'class' => 'login-form']) }}
										<label class="form-type-1">
											<span>Nombre *</span>
											<input type="text" name="name" required>
										</label>
										<label class="form-type-1">
											<span>Apellido *</span>
											<input type="text" name="surname" required>
										</label>
										<label class="form-type-1">
											<span>Ciudad *</span>
											<input type="text" class="country" required name="city">
											@if($errors->has('city'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('city') }}</li></ul>
				                            @endif
										</label>
										<label class="form-type-1">
											<span>Estado *</span>
											<input type="text" class="state" required name="state">
											@if($errors->has('state'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('state') }}</li></ul>
				                            @endif
										</label>
										<label class="form-type-1">
											<span>Colonia *</span>
											<input name="colony" required>
											@if($errors->has('colony'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('colony') }}</li></ul>
				                            @endif
										</label>
										<label class="form-type-1">
											<span>Código postal *</span>
											<input name="postal_code" required>
											@if($errors->has('postal_code'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('postal_code') }}</li></ul>
				                            @endif
										</label>
										<label class="form-type-1">
											<span>Calle y No. *</span>
											<input name="adress" required>
											@if($errors->has('adress'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('adress') }}</li></ul>
				                            @endif
										</label>
										<label class="form-type-1">
											<span>Teléfono *</span>
											<input name="phone" required>
											@if($errors->has('phone'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('phone') }}</li></ul>
				                            @endif
										</label>
									
									<div class="container-button">
										<button class="button-login uHover" type="submit">
											Guardar nueva dirección
										</button>
									</div>
									{{ Form::close() }}
								</div>
							</div>
			                <!-- <button id="activate-step-2" class="btn btn-primary btn-lg">Activate Step 2</button> -->
			            </div>
			        </div>
			    </div>
			    <div class="row setup-content" id="step-2">
			        <div class="col-xs-12">
			            <div class="col-md-12 well">
			                <div class="login-section">
			                	<div class="pay-section">
								<span class="title">MÉTODO DE PAGO</span>
								<div class="desc-container">
									<div class="option-choice">
										<label>
											<input type="radio" class="choice-element" name="choice-opt">  
											<span class="desc-choice-elem">Conekta</span>
											<div class="element-view">
												<img src="{{ asset('images/ways_to_pay_comp.png') }}">
											</div>
											<div class="element-choice desc-container">
												<h4>Not yet implemented</h4>
											</div>
										</label>
									</div>
									<div class="option-choice-paypal">
										<label>
											<input type="radio" class="choice-element" name="choice-opt">  
											<span class="desc-choice-elem">PayPal</span>
											<div class="element-choice desc-container">
												<div class="form-paypal">
													{{ Form::open(['route' => 'paypalp', 'class' => '']) }}
														<input type="text" name="amount" value="" hidden>
														<input type="image" src="https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea." style="cursor: pointer;">
														<img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="30" height="30">
														<!-- <button class="uHover" type="submit">
															<img src="{{ asset('images/correct-symbol.png') }}">
															<span class="checkoutToPay"><a>Comprar ahora</a></span>
														</button> -->
												{{ Form::close() }}
												</div>
											</div>
										</label>
									</div>
								</div>
							</div>
						</div>
			            </div>
			        </div>
			    </div>
			</div>

	</section>
@endsection

@section('js')

	<script type="text/javascript" src="https://cdn.conekta.io/js/latest/conekta.js"></script>

	<script>
		$('div.element-choice').slideUp(0);
 		$('.choice-element').click(function(){
 			$('div.element-choice').slideUp(0);
 			$(this).siblings('div.element-choice').slideDown(500);
 		});
	</script>

	<script>
		$(document).ready(function() {
    
		    var navListItems = $('ul.setup-panel li a'),
		        allWells = $('.setup-content');

		    allWells.hide();

		    navListItems.click(function(e)
		    {
		        e.preventDefault();
		        var $target = $($(this).attr('href')),
		            $item = $(this).closest('li');
		        
		        if (!$item.hasClass('disabled')) {
		            navListItems.closest('li').removeClass('active');
		            $item.addClass('active');
		            allWells.hide();
		            $target.show();
		        }
		    });
		    
		    $('ul.setup-panel li.active a').trigger('click');
		    
		    // DEMO ONLY //
		    $('#activate-step-2-select-thumbnail').on('click', function(e) {
		        $('ul.setup-panel li:eq(1)').removeClass('disabled');
		        $('ul.setup-panel li a[href="#step-2"]').trigger('click');
		        // $(this).remove();
		    })    
		});

		$('.adressRemove').on('click', 'a', function(){
		    console.log( $(this).attr('id') );
		    let adress_id = $(this).attr('id');
		    swal({
			  title: '¿Está seguro de querer borrarlo?',
			  text: "Este cambio no puede revertirse",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Si'
			}).then((result) => {
			  if (result.value) {
			    $.ajax({
			        url: '/rmadress',
			        type: 'POST',
			        dataType: 'json',
			        data: {
			        	_token: "{{ csrf_token() }}",
			        	id: adress_id
			        },
			    })
			    .done(function(resp) {
			    	if (resp.data) {
			    		swal({
			    			allowOutsideClick: false,
			    			title: 'Se ha borrado correctamente',
			              	type: 'success',
			              	confirmButtonClass: 'btn btn-success',
			              	confirmButtonText: 'Ok'
			    		}).then((result) => {
			    			console.log(result);
			    			if (result.value) {
			    				// console.log("eliminado prro");
			    				location.reload();
			    			}
			    		});
			    	}
			    })
			    .fail(function() {
			        console.log("error");
			    })
			    .always(function() {
			        console.log("complete");
			    });
			  }
			});
	});

        

	</script>

	

@endsection