@extends('layouts.app')

@section('content')
	
		@if(Session::has('success'))
			<div class="" style="margin-top: 20px;">
				<div class="col-md-4 col-md-offset-4">
					<div id="charge-message" class="alert alert-success">
						{{Session::get('success')}}
					</div>
				</div>
			</div>
		@endif
	<section id="loginContainer">


		<div class="container-user">
			
			<div class="menu-bar">
				<div class="option uHover">
					<a href="{{ route('my-account') }}" class="decoration" style="color:#a1a1a1;">
						Mi cuenta
					</a>
				</div>
				<div class="option uHover selected">
					<a href="{{ route('adresses') }}" class="decoration" style="color:white;">
						Información de envio
					</a>
				</div>
				<div class="option uHover" >
					<a href="{{ route('orders') }}" class="decoration" style="color:#a1a1a1;">
						Tus Compras
					</a>
				</div>
				<div class="option uHover">
					<a href="{{ route('logout') }}" class="decoration">
						Cerrar sesión
					</a>
				</div>
			</div>

			<div class="info-menu-container">
				<div class="option-selected">
					<div class="panel panel-default">
					  <div class="panel-heading">Direcciones</div>
					  <div class="panel-body">
					  	<div class="row">
			            @foreach($adresses as $adress)
							<div class="col-sm-6 col-md-6">
								<div class="thumbnail">
									<div class="caption">
									    <h4>{{$adress->name}} {{$adress->surname}}</h4>
									    <p>{{$adress->adress}}</p>
									    <p>{{$adress->colony}}</p>
									    <p>{{$adress->city}}, {{$adress->state}}, {{$adress->postal_code}}</p>
									    <p>{{$adress->phone}}</p>
									    <br>
									    <p>
									        	{{ Form::open(['route' => 'myAdress', 'class' => '']) }}
									        	<div class="adressRemove">
										        	<input class="adressId" type="text" name="adress" value="{{$adress->id}}" hidden style="display: none;">
										        	<button type="submit" class="btn btn-danger" type="submit">Editar</button>
										        	<a href="#" class="btn waves-effect waves-light btn-danger" id="{{$adress->id}}" >
						                                <i class="fa fa-trash"></i>
						                            </a>
									        	</div>
					                            {{ Form::close() }}
					                        </p>
									      </div>
									    </div>
									  </div>
									@endforeach
								</div>
					  </div>
					</div>
					<div class="panel panel-default">
					  <div class="panel-heading">Añadir una nueva dirección</div>
					  <div class="panel-body">
					  	<div class="desc-container">
									{{ Form::open(['route' => 'createAdress', 'class' => 'login-form']) }}
										<label class="form-type-1">
											<span>Nombre *</span>
											<input type="text" name="name" required>
										</label>
										<label class="form-type-1">
											<span>Apellido *</span>
											<input type="text" name="surname" required>
										</label>
										<label class="form-type-1">
											<span>Ciudad *</span>
											<input type="text" class="country" required name="city">
											@if($errors->has('city'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('city') }}</li></ul>
				                            @endif
										</label>
										<label class="form-type-1">
											<span>Estado *</span>
											<input type="text" class="state" required name="state">
											@if($errors->has('state'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('state') }}</li></ul>
				                            @endif
										</label>
										<label class="form-type-1">
											<span>Colonia *</span>
											<input name="colony" required>
											@if($errors->has('colony'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('colony') }}</li></ul>
				                            @endif
										</label>
										<label class="form-type-1">
											<span>Código postal *</span>
											<input name="postal_code" required>
											@if($errors->has('postal_code'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('postal_code') }}</li></ul>
				                            @endif
										</label>
										<label class="form-type-1">
											<span>Calle y No. *</span>
											<input name="adress" required>
											@if($errors->has('adress'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('adress') }}</li></ul>
				                            @endif
										</label>
										<label class="form-type-1">
											<span>Teléfono *</span>
											<input name="phone" required>
											@if($errors->has('phone'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('phone') }}</li></ul>
				                            @endif
										</label>
									
									<div class="container-button">
										<button class="button-login uHover" type="submit">
											Guardar nueva dirección
										</button>
									</div>
									{{ Form::close() }}
								</div>
							</div>
					  </div>
					</div>
			</div>

		</div>	

	</section>

@endsection


@section('js')

<script>

	$('.adressRemove').on('click', 'a', function(){
		    console.log( $(this).attr('id') );
		    let adress_id = $(this).attr('id');
		    swal({
			  title: '¿Está seguro de querer borrarlo?',
			  text: "Este cambio no puede revertirse",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Si'
			}).then((result) => {
			  if (result.value) {
			    $.ajax({
			        url: '/rmadress',
			        type: 'POST',
			        dataType: 'json',
			        data: {
			        	_token: "{{ csrf_token() }}",
			        	id: adress_id
			        },
			    })
			    .done(function(resp) {
			    	if (resp.success) {
			    		console.log(resp);
			    		swal({
			    			allowOutsideClick: false,
			    			title: 'Se ha borrado correctamente',
			              	type: 'success',
			              	confirmButtonClass: 'btn btn-success',
			              	confirmButtonText: 'Ok'
			    		}).then((result) => {
			    			console.log(result);
			    			if (result.value) {
			    				// console.log("eliminado prro");
			    				location.reload();
			    			}
			    		});
			    	}else{
			    		swal({
			    			allowOutsideClick: false,
			    			title: resp.data,
			              	type: 'error',
			              	confirmButtonClass: 'btn btn-success',
			              	confirmButtonText: 'Ok'
			    		}).then((result) => {
			    			console.log(result);
			    			if (result.value) {
			    				// location.reload();
			    			}
			    		});
			    	}
			    })
			    .fail(function() {
			        console.log("error");
			    })
			    .always(function() {
			        console.log("complete");
			    });
			  }
			});
	});
</script>

@endsection

