@extends('layouts.app')

@section('content')
	@if(Session::has('success'))
		<div class="container">
			<div class="row" style="margin-top: 25px;">
				<div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
					<div id="charge-message" class="alert alert-success">
						{{Session::get('success')}}
					</div>
				</div>
			</div>
		</div>
	@endif

	@if(Session::has('warning'))
		<div class="container">
			<div class="row" style="margin-top: 25px;">
				<div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
					<div id="charge-message" class="alert alert-danger">
						{{Session::get('warning')}}
					</div>
				</div>
			</div>
		</div>
	@endif

	<div class="row" style="margin-top: 35px;">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">Reenviar correo de activación</h3>
			  </div>
			  <div class="panel-body">
			    Su cuenta necesita ser activada, dirigase al correo registrado y active su cuenta.
			    <div class="row">
			    	<div class="col-md-4 col-md-offset-4">
			    		{{ Form::open(['route' => 'resendActivation', 'class' => '']) }}
			    			<input type="text" name="user_id" value="{{$user->id}}" hidden="true">
				    		<button type="submit" id="myButton" data-loading-text="Enviando..." class="btn btn-primary" autocomplete="off">
							  Reenviar correo
							</button>
						{{ Form::close() }}
			    	</div>
			    </div>
			  </div>
			</div>
		</div>
	</div>

	<section class="best-option">
		<div class="image-option">
			<img src="{{ asset('images/mechanic.png') }}">
		</div>
		<div class="detail-options">
			<span class="titleDetails">¿Por qué somos <br><strong>la mejor opción</strong>?</span>
			<ul>
				<li>
					<span class="icon"><img src="{{ asset('images/pays.png') }}"></span>
					<span>Múltiples formas de pago</span>
				</li>
				<li>
					<span class="icon"><img src="{{ asset('images/seends.png') }}"></span>
					<span>Envios a todo México</span>
				</li>
				<li>
					<span class="icon"><img src="{{ asset('images/atention.png') }}"></span>
					<span>Atención personalizada</span>
				</li>
			</ul>
		</div>
	</section>

	<section class="newsletter">
		<div class="msg-newsletter">
			Recibe ofertas y promociones <span>Suscribete a nuestro boletín</span>
		</div>
		<form>
			<label>
				<input type="text" placeholder="Escribe tu e-mail">
			</label>
			<button class="uHover">
				SUSCRIBIRME
			</button>
		</form>
	</section>

	@include('partials.ways-to-pay')

@endsection

@section('js')

	<script type="text/javascript">
		$('#myButton').on('click', function () {
		    var $btn = $(this).button('loading')

		    // business logic...
		    // $btn.button('reset')
		  })
	</script>

@endsection
