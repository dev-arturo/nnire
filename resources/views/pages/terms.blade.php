@section('css')
	<link rel="stylesheet" href="{{asset('css/hiddenSearch.css')}}">
@endsection

@extends('layouts.app')

@section('content')

	<section id="textDescriptionContent">
		<p><strong>T&eacute;rminos y Condiciones de Uso</strong></p>
		<p><span style="font-weight: 400;">&nbsp;</span></p>
		<p><strong>INFORMACI&Oacute;N RELEVANTE</strong></p>
		<p><span style="font-weight: 400;">Es requisito necesario para la adquisici&oacute;n de los productos que se ofrecen en este sitio, que lea y acepte los siguientes T&eacute;rminos y Condiciones que a continuaci&oacute;n se redactan. El uso de nuestros servicios as&iacute; como la compra de nuestros productos implicar&aacute; que usted ha le&iacute;do y aceptado los T&eacute;rminos y Condiciones de Uso en el presente documento. Todas los productos &nbsp;que son ofrecidos por nuestro sitio web pudieran ser creadas, cobradas, enviadas o presentadas por una p&aacute;gina web tercera y en tal caso estar&iacute;an sujetas a sus propios T&eacute;rminos y Condiciones. En algunos casos, para adquirir un producto, ser&aacute; necesario el registro por parte del usuario, con ingreso de datos personales fidedignos y definici&oacute;n de una contrase&ntilde;a.</span></p>
		<p><span style="font-weight: 400;">El usuario puede elegir y cambiar la clave para su acceso de administraci&oacute;n de la cuenta en cualquier momento, en caso de que se haya registrado y que sea necesario para la compra de alguno de nuestros productos. No asume la responsabilidad en caso de que entregue dicha clave a terceros.</span></p>
		<p><span style="font-weight: 400;">Todas las compras y transacciones que se lleven a cabo por medio de este sitio web, est&aacute;n sujetas a un proceso de confirmaci&oacute;n y verificaci&oacute;n, el cual podr&iacute;a incluir la verificaci&oacute;n del stock y disponibilidad de producto, validaci&oacute;n de la forma de pago, validaci&oacute;n de la factura (en caso de existir) y el cumplimiento de las condiciones requeridas por el medio de pago seleccionado. En algunos casos puede que se requiera una verificaci&oacute;n por medio de correo electr&oacute;nico.</span></p>
		<p><span style="font-weight: 400;">Los precios de los productos ofrecidos en esta Tienda Online es v&aacute;lido solamente en las compras realizadas en este sitio web.</span></p>
		<p><strong>LICENCIA</strong></p>
		<p><span style="font-weight: 400;">A trav&eacute;s de su sitio web concede una licencia para que los usuarios utilicen&nbsp; los productos que son vendidos en este sitio web de acuerdo a los T&eacute;rminos y Condiciones que se describen en este documento.</span></p>
		<p><strong>USO NO AUTORIZADO</strong></p>
		<p><span style="font-weight: 400;">En caso de que aplique (para venta de software, templetes, u otro producto de dise&ntilde;o y programaci&oacute;n) usted no puede colocar uno de nuestros productos, modificado o sin modificar, en un CD, sitio web o ning&uacute;n otro medio y ofrecerlos para la redistribuci&oacute;n o la reventa de ning&uacute;n tipo.</span></p>
		<p><br /><br /></p>
		<p><strong>PROPIEDAD</strong></p>
		<p><span style="font-weight: 400;">Usted no puede declarar propiedad intelectual o exclusiva a ninguno de nuestros productos, modificado o sin modificar. Todos los productos son propiedad &nbsp;de los proveedores del contenido. En caso de que no se especifique lo contrario, nuestros productos se proporcionan&nbsp; sin ning&uacute;n tipo de garant&iacute;a, expresa o impl&iacute;cita. En ning&uacute;n esta compa&ntilde;&iacute;a ser&aacute; &nbsp;responsables de ning&uacute;n da&ntilde;o incluyendo, pero no limitado a, da&ntilde;os directos, indirectos, especiales, fortuitos o consecuentes u otras p&eacute;rdidas resultantes del uso o de la imposibilidad de utilizar nuestros productos.</span></p>
		<p><strong>POL&Iacute;TICA DE REEMBOLSO Y GARANT&Iacute;A</strong></p>
		<p><span style="font-weight: 400;">En el caso de productos que sean&nbsp; mercanc&iacute;as irrevocables no-tangibles, no realizamos reembolsos despu&eacute;s de que se env&iacute;e el producto, usted tiene la responsabilidad de entender antes de comprarlo. &nbsp;Le pedimos que lea cuidadosamente antes de comprarlo. Hacemos solamente excepciones con esta regla cuando la descripci&oacute;n no se ajusta al producto. Hay algunos productos que pudieran tener garant&iacute;a y posibilidad de reembolso pero este ser&aacute; especificado al comprar el producto. En tales casos la garant&iacute;a solo cubrir&aacute; fallas de f&aacute;brica y s&oacute;lo se har&aacute; efectiva cuando el producto se haya usado correctamente. La garant&iacute;a no cubre aver&iacute;as o da&ntilde;os ocasionados por uso indebido. Los t&eacute;rminos de la garant&iacute;a est&aacute;n asociados a fallas de fabricaci&oacute;n y funcionamiento en condiciones normales de los productos y s&oacute;lo se har&aacute;n efectivos estos t&eacute;rminos si el equipo ha sido usado correctamente. Esto incluye:</span></p>
		<p><span style="font-weight: 400;">&ndash; De acuerdo a las especificaciones t&eacute;cnicas indicadas para cada producto.</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">&ndash; En condiciones ambientales acorde con las especificaciones indicadas por el fabricante.</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">&ndash; En uso espec&iacute;fico para la funci&oacute;n con que fue dise&ntilde;ado de f&aacute;brica.</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">&ndash; En condiciones de operaci&oacute;n el&eacute;ctricas acorde con las especificaciones y tolerancias indicadas.</span></p>
		<p><strong>COMPROBACI&Oacute;N ANTIFRAUDE</strong></p>
		<p><span style="font-weight: 400;">La compra del cliente puede ser aplazada para la comprobaci&oacute;n antifraude. Tambi&eacute;n puede ser suspendida por m&aacute;s tiempo para una investigaci&oacute;n m&aacute;s rigurosa, para evitar transacciones fraudulentas.</span></p>
		<p><strong>PRIVACIDAD</strong></p>
		<p><span style="font-weight: 400;">Este sitio web garantiza que la informaci&oacute;n personal que usted env&iacute;a cuenta con la seguridad necesaria. Los datos ingresados por usuario o en el caso de requerir una validaci&oacute;n de los pedidos no ser&aacute;n entregados a terceros, salvo que deba ser revelada en cumplimiento a una orden judicial o requerimientos legales.</span></p>
		<p><span style="font-weight: 400;">La suscripci&oacute;n a boletines de correos electr&oacute;nicos publicitarios es voluntaria y podr&iacute;a ser seleccionada al momento de crear su cuenta.</span></p>
		<p><span style="font-weight: 400;">reserva los derechos de cambiar o de modificar estos t&eacute;rminos sin previo aviso.</span></p>
	</section>

@endsection

@section('js')

@endsection
