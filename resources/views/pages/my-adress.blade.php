@extends('layouts.app')

@section('content')
	
		@if(Session::has('success'))
			<div class="row" style="margin-top: 20px;">
				<div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
					<div id="charge-message" class="alert alert-success">
						{{Session::get('success')}}
					</div>
				</div>
			</div>
		@endif
	<section id="loginContainer">


		<div class="container-user">
			
			<div class="menu-bar">
				<div class="option uHover">
					<a href="{{ route('my-account') }}" class="decoration" style="color:#a1a1a1;">
						Mi cuenta
					</a>
				</div>
				<div class="option uHover selected">
					<a href="{{ route('adresses') }}" class="decoration" style="color:white;">
						Información de envio
					</a>
				</div>
				<div class="option uHover" >
					<a href="{{ route('orders') }}" class="decoration" style="color:#a1a1a1;">
						Tus Compras
					</a>
				</div>
				<div class="option uHover">
					<a href="{{ route('logout') }}" class="decoration">
						Cerrar sesión
					</a>
				</div>
			</div>

			<div class="info-menu-container">
				<div class="option-selected">
					
					<div class="panel panel-default">
					  <div class="panel-heading">Actualizar dirección</div>
					  <div class="panel-body">
					  	<div class="desc-container">
									{{ Form::open(['route' => 'updateAdress', 'class' => 'login-form']) }}
										<input type="text" name="id" value="{{$adress->id}}" required hidden="true">
										<label class="form-type-1">
											<span>Nombre *</span>
											<input type="text" name="name" value="{{$adress->name}}" required>
										</label>
										<label class="form-type-1">
											<span>Apellido *</span>
											<input type="text" name="surname" value="{{$adress->surname}}" required>
										</label>
										<label class="form-type-1">
											<span>Ciudad *</span>
											<input type="text" class="country" required name="city" value="{{$adress->city}}">
											@if($errors->has('city'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('city') }}</li></ul>
				                            @endif
										</label>
										<label class="form-type-1">
											<span>Estado *</span>
											<input type="text" class="state" required name="state" value="{{$adress->state}}">
											@if($errors->has('state'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('state') }}</li></ul>
				                            @endif
										</label>
										<label class="form-type-1">
											<span>Colonia *</span>
											<input name="colony" required value="{{$adress->colony}}">
											@if($errors->has('colony'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('colony') }}</li></ul>
				                            @endif
										</label>
										<label class="form-type-1">
											<span>Código postal *</span>
											<input name="postal_code" required value="{{$adress->postal_code}}">
											@if($errors->has('postal_code'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('postal_code') }}</li></ul>
				                            @endif
										</label>
										<label class="form-type-1">
											<span>Calle y No. *</span>
											<input name="adress" required value="{{$adress->adress}}">
											@if($errors->has('adress'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('adress') }}</li></ul>
				                            @endif
										</label>
										<label class="form-type-1">
											<span>Teléfono *</span>
											<input name="phone" required value="{{$adress->phone}}">
											@if($errors->has('phone'))
				                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('phone') }}</li></ul>
				                            @endif
										</label>
									
									<div class="container-button">
										<a href="{{route('adresses')}}" class="button-login uHover" >
											Cancelar
										</a>
										<button class="button-login uHover" type="submit" style="margin-right: 5px;">
											Actualizar
										</button>
									</div>
									{{ Form::close() }}
								</div>
							</div>
					  </div>
					</div>
			</div>

		</div>	

	</section>

@endsection


@section('js')

<script>
	$("#Back").click(function () {
		window.history.back();
	});
</script>

@endsection

