@extends('layouts.app')

@section('content')

	<section class="headerListProducts">
		<div class="imgHeaderList">
			<img src="{{ asset('images/header-products.png') }}">
		</div>
	</section>

	@if(count($products))
		<section class="container-products">
			<div class="list-products">
				<div class="productFilter">
					<!-- <form>
						<label>
							<input type="text" placeholder="Escribe lo que quieres buscar">
						</label>
						<button class="uHover">BUSCAR</button>
					</form> -->
					<!-- <form>
						<span>Ordenar por: </span>
						<select>
							<option selected>Precio Mayor</option>
							<option selected>Precio Menor</option>
						</select>
					</form> -->
				</div>
				<div class="group-products">
					<div class="row" style="width: 100%;">
						@if(isset($allProducts))
					  		<div class="col-md-6 col-md-offset-3">{{ $products->links() }}</div>
						@endif()
					</div>
						@foreach($products as $product)
								@include('partials.product', ['product' => $product])
						@endforeach
					<div class="row" style="width: 100%;">
					  @if(isset($allProducts))
					  		<div class="col-md-6 col-md-offset-3">{{ $products->links() }}</div>
						@endif()
					</div>
					<!--
					<div class="loadMore">
						<span class="uHover">Cargar más productos</span>
					</div>
					-->
				</div>
			</div>
		</section>
	@else
		<div class="container">
			<div class="row" style="margin-top: 20px;">
				<div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
					<div id="charge-message" class="alert alert-danger">
						Si no has encontrado el producto que estas buscando, envía un mensaje por chat para darte la información.
					</div>
				</div>
			</div>
		</div>
	@endif

	@if(count($promotionItems))
            <section class="product-promo">
                <div class="list-products">
                    <div class="nav-slide-product">
                        <div class="el-title-products">
                            REFACCIONES EN <span>PROMOCIÓN</span>
                        </div>
                        <div  class="itemsPromo">

                        </div>
                    </div>
                    <ul class="slideProducts">
                        <li>
                            @for($i = 1; $i <= count($promotionItems); $i++)
                                <?php $product = $promotionItems[$i - 1]; ?>
                                @include('partials.product', [ 'name' => $product->name ])

                                @if($i == 6)
                                    @break;
                                @endif
                            @endfor
                        </li>
                        @if(count($promotionItems) > 6)
                            <li>
                                <?php $product = $promotionItems[$i - 1]; ?>
                                @for($i = 7; $i <= count($promotionItems); $i++)
                                    @include('partials.product', [ 'name' => $product->name ])
                                @endfor
                            </li>
                        @endif
                    </ul>
                </div>
            </section>
    @endif

	<section class="need-help">
		Necesitas Ayuda:  <span><img src="{{ asset('images/whatsapp_icon.png') }}"></span> (33) 36904050
	</section>

	@include('partials.ways-to-pay')

@endsection


@section('js')

	<script type="text/javascript">
		$(document).on('ready', initSliders);

		function initSliders()
		{
		    var touchEnabled = false;

            var md = new MobileDetect(window.navigator.userAgent);
            if (md.mobile()) {
                touchEnabled = true;
            }

			$('.slideHome').bxSlider({
                adaptiveHeight: true,
				auto: true,
                autoHover: true,
                hideControlOnEnd: true,
				pause: 4000,
                touchEnabled: touchEnabled
			});

			$('.slideProducts').bxSlider({
				speed: 1000
			});
		}
	</script>

@endsection