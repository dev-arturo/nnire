<footer>

	<div class="footer-container">
		<div>
			<span>Acerca de nosotros</span>
			<ul>
				<li><a href="{{route('about')}}">¿Quiénes somos?</a></li>
				<li><a href="{{route('contact')}}">¿Dónde estamos?</a></li>
			</ul>
		</div>

		<div>
			<span>Servicio al Cliente</span>
			<ul>
				<li><a href="{{route('contact')}}">Contacto</a></li>
				<li><a href="{{route('questions')}}">Preguntas Frecuentes</a></li>
				<li><a href="{{route('questions')}}">Formas de Pago</a></li>
				<li><a href="{{route('terms')}}">Política de Privacidad</a></li>
				<li><a href="{{route('terms')}}">Políticas de Envío, Devoluciones y Cancelaciones</a></li>
				<li><a href="{{route('terms')}}">Términos y condiciones</a></li>
			</ul>
		</div>

		<div>
			<span>Contacto</span>
			<ul>
				<li>Dirección: Av. Adolfo López Mateos Sur 1460, Chapalita</li>
				<li>C.P. 44500 Guadalajara, Jal.</li>
				<li>Teléfono: <a href="tel:+513331226677">01 33 3122 6677</a></li>
			</ul>
		</div>
	</div>

</footer>