<section class="way-to-pay">
	<div class="descr-way">
		<div class="textInfo">
			<span class="titleInfo">Formas de Pago</span>
			<span class="titleDesc">Aceptamos tarjetas de crédito y débito<br>trasferencias bancarias ó depositos bancarios</span>
		</div>
		<div class="imageWayto">
			<img src="{{ asset('images/ways_to_pay.png') }}">
		</div>
	</div>
</section>