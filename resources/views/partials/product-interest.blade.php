<div class="product">
	<div class="image">
		{{-- {{dd($product)}} --}}
		{{-- <img src="{{ asset('storage/products/'.$product->image[0]) }}" alt=""> --}}

		@if($product->image != null)
			<img src="{{ asset('storage/products/'.$product->image[0]) }}" alt="">
		@else
			<img src="{{ asset('images/placeholder.jpg') }}" alt="placeholderImage">
		@endif
	</div>
	<div class="titleProduct truncate">
		<span>
			{{$product->name}}	
		</span>
	</div>

	@if(auth()->check() && auth()->user()->isAdmin == 3)
		<div class="priceProduct truncate">
			${{ $product->wholesale }}
		</div>
	@elseif(auth()->check() && auth()->user()->isAdmin == 2)
    	<div class="priceProduct truncate">
    		${{ $product->wholesale }}
    	</div>
	@else
		<div class="priceProduct truncate">
			${{ $product->price }}
		</div>
	@endif
	<div class="viewMoreProduct uHover">
		{{ Form::open(['route' => 'add-to-cart', 'class' => '']) }}
			<input name="id" type="text" value="{{$product->id}}" hidden="true">
			<input name="quantity" type="text" value="1" hidden="true">
			<button type="submit">Agregar al carrito</button>
		{{ Form::close() }}
		{{-- <a href="{{route('product', [$product, str_slug($product->name)])}}">Ver detalles</a> --}}
	</div>
</div>