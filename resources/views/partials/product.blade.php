<div class="product">
	<div class="image" style="height: 140px;">
		@if($product->hasImage())
		    <a href="{{route('product', [$product, str_slug($product->name)])}}">
		        <img src="{{ asset($product->imagePath()) }}" style="height: 142px;" alt="producto">
		    </a>
		@else
		    <a href="{{route('product', [$product, str_slug($product->name)])}}">
			    <img src="{{ asset($product->placeholderImage()) }}" alt="producto">
			</a>
		@endif


		{{-- @if($product->image)
			<img src="{{ asset('storage/products/'.$img) }}" alt="producto">
		@else
			<img src="{{ asset($product->placeholderImage()) }}" alt="producto">
		@endif --}}
	</div>
	<div class="titleProduct truncate">
		<span>
			{{ $product->name }}
		</span>
	</div>
	<div class="descProduct">
		<span>SKU: </span>
		<span class="code">{{ $product->sku }}</span>
		<p class="notesProduct truncate">
			{!! nl2br(substr($product->description, 0, 50)) !!}
		</p>
	</div>
	
	@if(auth()->check() && auth()->user()->isAdmin == 3)
		<div class="priceProduct">
			${{ $product->wholesale }}
		</div>
	@elseif(auth()->check() && auth()->user()->isAdmin == 2)
    	<div class="priceProduct">
    		${{ $product->wholesale }}
    	</div>
	@else
		<div class="priceProduct">
			${{ $product->price }}
		</div>
	@endif
	
	{{ Form::open(['route' => 'add-to-cart', 'class' => '']) }}
		<div class="viewMoreProduct ">
			<input type="text" hidden value="{{$product->id}}" name="id">
			<label for="quantity">Cantidad:</label>
			<input id="quantity" name="quantity" type="number" value="1" min="1" max="99" style="width: 30%;" required="true">
		</div>
		<div class="viewMoreProduct uHover" style="width: 100% !important;">
			<button class="uHover" type="submit" style="background-color: #e74431; color: white; width: 100%; font-size: 14px; height: 30px;">
			    <img src="{{asset('images/cart.png')}}" alt="cart" style="width: 20px; margin-right: 15px; display: inline;">AGREGAR
			</button>
		</div>
	{{ Form::close() }}
	<div class="viewMoreProduct uHover">
		<a href="{{route('product', [$product, str_slug($product->name)])}}">Ver detalles</a>
	</div>
</div>