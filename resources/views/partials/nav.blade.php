<nav>
	
	<div class="container-nav">
		<ul class="menu">
			<li class="uHover"><a href="{{route('home')}}">inicio</a></li>
			<li class="uHover"><a href="{{route('products')}}">refacciones</a></li>
			<li class="uHover"><a href="{{route('products')}}">accesorios</a></li>
			@if(!auth()->check())
				<li class="uHover"><a href="{{route('register')}}">registro</a></li>
			@endif
			<li class="uHover"><a href="{{route('contact')}}">contacto</a></li>
		</ul>

		<div class="search">
			<form method="get" action="{{ route('searcher') }}" id="searcherForm">
				<label>
					<input type="text" name="criteria">
				</label>
				<span class="uHover">
					<img src="{{ asset('images/search.png') }}" onclick="submitSearcherForm();">
				</span>
			</form>
			<!--
			<div class="results_search">
				<div class="results_search-item">
					<a href="">
						<div class="item_search-image">
							<img src="{{asset('images/product02.png')}}">
						</div>
						<div class="item_search-description">
							<span class="title_item-search">Duralast Balata Name</span>
							<span class="note_item-search">Semi - Metálica Incluye laminas reductoras de ruido</span>
						</div>
					</a>
				</div>
				<div class="results_search-item">
					<a href="">
						<div class="item_search-image">
							<img src="{{asset('images/product02.png')}}">
						</div>
						<div class="item_search-description">
							<span class="title_item-search">Duralast Balata Name</span>
							<span class="note_item-search">Semi - Metálica Incluye laminas reductoras de ruido</span>
						</div>
					</a>
				</div>
				<div class="results_search-item">
					<a href="">
						<div class="item_search-image">
							<img src="{{asset('images/product02.png')}}">
						</div>
						<div class="item_search-description">
							<span class="title_item-search">Duralast Balata Name</span>
							<span class="note_item-search">Semi - Metálica Incluye laminas reductoras de ruido</span>
						</div>
					</a>
				</div>
			</div>
			-->
		</div>

		<div class="social">
			<ul class="menu-social">
				<li class="uHover">
					<a href="https://www.facebook.com/Refacciones-Nissan-1961708180730803/" target="_blank">
						<img src="{{ asset('images/facebook.png') }}">
					</a>
				</li>
			</ul>
		</div>
	</div>

</nav>

<section class="search-general">

	<div class="container-search">
		{{-- <form method="get" action="{{ route('searcher') }}"> --}}
		    {{ Form::open(['route' => 'searcher']) }}
			<label>
				<select name="carModel" onchange="updateYears(this.value, '#searcherYearField');">>
					<option value="" selected="selected">Modelo</option>
					@foreach($searcherCarModels as $searcherCarModel)
						<option value="{{ $searcherCarModel->id }}" {{ app('request')->input('carModel') == $searcherCarModel->id ? 'selected="selected"' : '' }}>{{ $searcherCarModel->name }}</option>
					@endforeach
				</select>
			</label>
			<label>
				<select name="year" id="searcherYearField">>
					<option value="" selected="selected">Año</option>
					@foreach($searcherYears as $searcherYear)
						<option value="{{ $searcherYear->id }}" {{ app('request')->input('year') == $searcherYear->id ? 'selected="selected"' : '' }}>{{ $searcherYear->year }}</option>
					@endforeach
				</select>
			</label>
			<label>
				<select name="category" onchange="updateSubcategories(this.value, '#searcherSubcategoryField');">
					<option value="" selected="selected">Categoría</option>
					@foreach($searcherCategories as $searcherCategory)
						<option value="{{ $searcherCategory->id }}" {{ app('request')->input('category') == $searcherCategory->id ? 'selected="selected"' : '' }}>{{ $searcherCategory->name }}</option>
					@endforeach
				</select>
			</label>
			<label>
				<select name="subcategory" id="searcherSubcategoryField">
					<option value="" selected="selected">Subcategoría</option>
					@foreach($searcherSubcategories as $searcherSubcategory)
						<option value="{{ $searcherSubcategory->id }}" {{ app('request')->input('subcategory') == $searcherSubcategory->id ? 'selected="selected"' : '' }}>{{ $searcherSubcategory->name }}</option>
					@endforeach
				</select>
			</label>
			<button class="uHover">Buscar</button>
		{{-- </form> --}}
		{{ Form::close() }}
	</div>

</section>
