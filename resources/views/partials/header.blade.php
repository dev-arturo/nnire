<header>

	<!--
	<div class="cartMessage">
		<div class="msgCartItem">
			El producto x ha sido agregado.
		</div>

		<div class="msgCartItem">
			El producto x ha sido agregado.
		</div>
	</div>
	-->

	<div class="header-container">
		<div class="logo">
			<a href="{{ route('home') }}">
				<img src="{{ asset('images/nissancom.png') }}">
			</a>
		</div>
		<div class="header-details bNeue">
			<div class="account">
				<span>
					<img src="{{ asset('images/account_icon.png') }}"> 
					@if(auth()->check())
						<a href="{{ route('login') }}"><span>Mi cuenta</span></a>
					@else
						<a href="{{ route('login') }}"><span>Login</span></a>
					@endif
				</span>
				<span>
					<a href="{{ route('checkout') }}">
						<span class="icon-float antBold">{{Session::has('cart') ? Session::get('cart')->totalQty : '' }}</span>
						<img src="{{ asset('images/bag_icon.png') }}">
					</a>
				</span>
			</div>
			<div class="whatsapp antBold">
				<span>
					<img src="{{ asset('images/whatsapp_icon.png') }}"> 
					<a href="tel:+523338006129" style="text-decoration: none;"><span>(33)</span> 38006129</a>					
				</span>
			</div>
		</div>
	</div>

</header>
