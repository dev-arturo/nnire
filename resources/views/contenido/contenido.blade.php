@extends('principal')
@section('contenido')

@if(Auth::check())
@if (Auth::user()->idrol == 1)
<template v-if="menu==0">

    <dashboard></dashboard>
</template>

<div v-if="menu==1">
  @yield('content')
</div>

<template v-if="menu==2">
    <articulo></articulo>
</template>

<template v-if="menu==3">
    <modeloauto></modeloauto>

</template>

<template v-if="menu==4">
    <cupon></cupon>

</template>

<template v-if="menu==5">
    <proveedor></proveedor>

</template>

<template v-if="menu==6">
    <cliente></cliente>
</template>

<template v-if="menu==7">
    <user></user>
</template>

<template v-if="menu==8">
    <rol></rol>
</template>

<template v-if="menu==9">
    <consultaingreso></consultaingreso>
</template>

<template v-if="menu==10">
    <consultaventa></consultaventa>
</template>

<template v-if="menu==11">
    <imagen></imagen>
</template>

<template v-if="menu==12">
    <imagen></imagen>
</template>
<template v-if="menu==13">
    <envio></envio>
</template>
<template v-if="menu==14">
    <ayuda></ayuda>
</template>
<template v-if="menu==15">
    <acerca></acerca>
</template>
<template v-if="menu==16">
    <venta></venta>
</template>
<template v-if="menu==17">
    <ingreso></ingreso>
</template>
<template v-if="menu==18">
    <subcategoria></subcategoria>
</template>
@endif
@endif
@endsection
