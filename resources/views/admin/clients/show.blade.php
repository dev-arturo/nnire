@extends('principal')

@section('content')


<main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">

        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Detalles del cliente
                </div>

                <div class="card-body">

    @if(isset($client))
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                	<label>Nombre:</label>
                	<p class="form-control">{{$client->users->name}}</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Nombre:</label>
                    <p class="form-control">{{$client->users->surname}}</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Calle:</label>
                    <p class="form-control">{{$client->adress->adress}}</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Colonia:</label>
                    <p class="form-control">{{$client->adress->colony}}</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Código postal:</label>
                    <p class="form-control">{{$client->adress->colony}}</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Ciudad:</label>
                    <p class="form-control">{{$client->adress->city}}</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Estado:</label>
                    <p class="form-control">{{$client->adress->state}}</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Teléfono:</label>
                    <p class="form-control">{{$client->adress->phone}}</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Correo electrónico:</label>
                    <p class="form-control">{{$client->users->email}}</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>RFC:</label>
                    <p class="form-control">{{$client->rfc}}</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>CURP:</label>
                    <p class="form-control">{{$client->curp}}</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Forma de pago:</label>
                    <p class="form-control">{{$payment_methodOptions[$client->payment_method]}}</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Pagos:</label>
                    <p class="form-control">{{$paymentOptions[$client->payment]}}</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Uso de CFDI:</label>
                    <p class="form-control">{{$usage_cfdiOptions[$client->usage_cfdi]}}</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Tipo de cliente:</label>
                    <p class="form-control">{{$client->clientType->name}}</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group" id="limitCredit" style="display: {{ (old('client_type_id') == 1 or $client->client_type_id == 1) ? 'block' : 'none' }};">
                    <label>Limite de crédito:</label>
                    <p class="form-control">{{$client->creditLimit}}</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group" id="limitCredit" style="display: {{ (old('client_type_id') == 1 or $client->client_type_id == 1) ? 'block' : 'none' }};">
                    <label>Limite de crédito:</label>
                    <p class="form-control">{{$client->creditLimit}}</p>
                </div>
            </div>
        </div>


        <div class="form-group">
        	<label>Vendedor:</label>
        	<p class="form-control">{{$client->seller->fullName()}}</p>
        </div>
        <div class="text-right">
            <a href="{{ route('admin.clients') }}" class="btn btn-danger btn-bordered">Cancelar</a>
        </div>
    @elseif(isset($user))
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                	<label>Nombre:</label>
                	<p class="form-control">{{$user->name}}</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Apellido:</label>
                    <p class="form-control">{{$user->surname}}</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Correo electrónico:</label>
                    <p class="form-control">{{$user->email}}</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Conekta_Id:</label>
                    <p class="form-control">{{$user->conekta_id}}</p>
                </div>
            </div>
        </div>
        <div class="text-right">
            <a href="{{ route('admin.clients') }}" class="btn btn-danger btn-bordered">Cancelar</a>
        </div>
    @endif



@endsection

@section('js')

    <script type="text/javascript">
        function updateCreditLimitField(value)
        {
            if (value == 1) {
                $('#limitCredit').show();
            } else {
                $('#limitCredit').hide();
            }
        }
    </script>

@endsection
<!-- Fin ejemplo de tabla Listado -->
</div>
</main>
