@extends('principal')

@section('content')

<main class="main">
    <!-- Breadcrumb -->
    <ol class="breadcrumb">

    </ol>
    <div class="container-fluid">
        <!-- Ejemplo de tabla Listado -->
        <div class="card card-accent-danger mb-3">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Lista de clientes

                <a href="{{ route('admin.clients.create') }}" class="btn btn-info btn-bordered waves-effect waves-light m-b-20 btn-sm"
                    data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200" data-overlayColor="#36404a"><i
                        class="fa fa-plus"></i> Nuevo</a>


            </div>

            <div class="card-body">



                @if(count($sellers))
                <table class="table  table-responsive-md">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            @if(auth()->user()->isAdmin)
                            <th>Vendedor</th>
                            @endif
                            <th>Tipo</th>
                            <th class="text-right">Límite de crédito</th>
                            <th class="text-right">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sellers as $seller)
                        @foreach($seller->allClients as $client)
                        {{-- {{dd($client)}} --}}
                        <tr>
                            <td><a href="{{ route('admin.clients.show', $client->client_id) }}">{{ $client->name }}
                                    {{$client->surname}}</a></td>
                            <td>{{ $seller->name }} {{ $seller->surname }}</td>
                            @if($client->client_type_id == 1)
                            <td>Mayorista</td>
                            @elseif($client->client_type_id == 2)
                            <td>Minorista</td>
                            @endif
                            <td class="text-right">{{ is_null($client->creditLimit) ? 0.00 : $client->creditLimit}}</td>
                            <td class="text-right">
                                @if($client->activo == 0)
                                <a href="{{ route('admin.clients.updateStatus', $client->client_id) }}" class="btn btn-xs waves-effect waves-light btn-success"><i
                                        class="fa fa-pencil"></i></a>
                                @endif
                                <a href="{{ route('admin.clients.edit', $client->client_id) }}" class="btn btn-xs waves-effect waves-light btn-warning"><i
                                        class="fa fa-pencil"></i></a>
                                <a href="{{ route('admin.clients.delete', $client->client_id) }}" class="btn btn-xs waves-effect waves-light btn-danger"><i
                                        class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        @endforeach
                    </tbody>
                </table>
                @else
                <div class="alert alert-icon alert-danger fade in" role="alert">
                    <i class="mdi mdi-block-helper"></i>
                    No se encontraron clientes en la base de datos.
                </div>
                @endif

                @if(auth()->user()->isAdmin == 1)

                <hr />

                @if(count($minoristas))
                <table class="table  table-responsive-md" id="griding">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Correo electrónico</th>
                            <th>Tipo</th>
                            <th class="text-right">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($minoristas as $minorista)
                        <tr>
                            <td><a href="{{ route('admin.clients.ver', $minorista->id) }}">{{ $minorista->name }}
                                    {{$minorista->surname}}</a></td>
                            <td>{{ $minorista->email }}</td>
                            <td>Minorista</td>
                            <td class="text-right">
                                @if($minorista->activo == 0)
                                <a href="{{ route('admin.clients.updateStatusUser', $minorista->id) }}" class="btn btn-xs waves-effect waves-light btn-success"><i
                                        class="fa fa-pencil"></i></a>
                                @endif
                                <a href="{{ route('admin.clients.eliminarUser', $minorista->id) }}" class="btn btn-xs waves-effect waves-light btn-danger"><i
                                        class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                <div class="alert alert-icon alert-danger fade in" role="alert">
                    <i class="mdi mdi-block-helper"></i>
                    No se encontraron clientes en la base de datos.
                </div>
                @endif

                @endif

                @endsection

                @section('js')
                <script type="text/javascript">
                    $('.grid').DataTable({
                        columnDefs: [{
                            orderable: false,
                            targets: [4]
                        }],
                        order: [
                            [0, 'asc']
                        ]
                    });

                    $('#griding').DataTable({
                        columnDefs: [{
                            orderable: false
                            // targets: [4]
                        }],
                        order: [
                            [0, 'asc']
                        ]
                    });
                </script>
                @endsection
                <!-- Fin ejemplo de tabla Listado -->
            </div>
</main>
