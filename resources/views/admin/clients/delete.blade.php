@extends('principal')

@section('content')


<main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">

        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Eliminar cliente
                </div>

                <div class="card-body">

    @if(isset($client))
        {{ Form::open(['url' => route('admin.clients.destroy', $client), 'method' => 'DELETE']) }}

            <div class="form-group">
                ¿ Confirma que desea eliminar el cliente <strong>{{ $client->name }} {{ $client->surname }}</strong> ?
                {{-- <p><em>Esta acción no podrá ser revertida.</em></p> --}}
            </div>

            <div class="">
                <a href="{{ route('admin.clients') }}" class="btn btn-danger btn-bordered">Cancelar</a>
                {{ Form::submit('Aceptar', ['class' => 'btn btn-success btn-bordered']) }}
            </div>
        {{ Form::close() }}
    @elseif(isset($user))
        {{ Form::open(['url' => route('admin.clients.deleteUser', $user), 'method' => 'DELETE']) }}

            <div class="form-group">
                ¿ Confirma que desea eliminar el cliente <strong>{{ $user->name }} {{ $user->surname }}</strong> ?
                {{-- <p><em>Esta acción no podrá ser revertida.</em></p> --}}
            </div>

            <div class="">
                <a href="{{ route('admin.clients') }}" class="btn btn-danger btn-bordered">Cancelar</a>
                {{ Form::submit('Aceptar', ['class' => 'btn btn-success btn-bordered']) }}
            </div>
        {{ Form::close() }}
    @endif



@endsection
<!-- Fin ejemplo de tabla Listado -->
</div>
</main>
