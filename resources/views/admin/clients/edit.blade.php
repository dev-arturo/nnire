@extends('principal')

@section('content')


<main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">

        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Editar cliente

                </div>

                <div class="card-body">

    {{ Form::model($client, ['url' => route('admin.clients.update', $client), 'method' => 'PATCH']) }}

        @if($errors->any())
                <div class="alert alert-icon alert-danger fade in" role="alert">
                    <i class="mdi mdi-block-helper"></i>
                    Han ocurrido algunos inconvenientes.
                </div>
            @endif

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('name', 'Nombre: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('name', $client->users->name, ['required' => true, 'class' => $errors->has('name') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('name'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('surname', 'Apellidos: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('surname', $client->users->surname, ['required' => true, 'class' => $errors->has('surname') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('surname'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('surname') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('adress', 'Calle: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('adress', $client->adress->adress, ['required' => true, 'class' => $errors->has('adress') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('adress'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('adress') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('colony', 'Colonia: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('colony', $client->adress->colony, ['required' => true, 'class' => $errors->has('colony') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('colony'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('colony') }}</li></ul>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('postal_code', 'Código postal: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('postal_code', $client->adress->postal_code, ['required' => true, 'class' => $errors->has('postal_code') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('postal_code'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('postal_code') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('city', 'Ciudad: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('city', $client->adress->city, ['required' => true, 'class' => $errors->has('city') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('city'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('city') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('state', 'Estado: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('state', $client->adress->state, ['required' => true, 'class' => $errors->has('state') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('state'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('state') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('phone', 'Teléfono: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('phone', $client->adress->phone, ['required' => true, 'class' => $errors->has('phone') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('phone'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('phone') }}</li></ul>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('rfc', 'RFC: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('rfc', $client->rfc, ['required' => true, 'class' => $errors->has('rfc') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('rfc'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('rfc') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('curp', 'CURP: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('curp', $client->curp, ['required' => true, 'class' => $errors->has('curp') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('curp'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('curp') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('payment_method', 'Forma de pago: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::select('payment_method', $payment_methodOptions, null, ['required' => true, 'class' => $errors->has('payment_method') ? 'form-control parsley-error select2' : 'form-control select2']) }}
                        @if($errors->has('payment_method'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('payment_method') }}</li></ul>
                        @endif
                    </div>
                </div>
            </div>



             <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('payment', 'Pagos: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::select('payment', $paymentOptions, null, ['required' => true, 'class' => $errors->has('payment') ? 'form-control parsley-error select2' : 'form-control select2']) }}
                        @if($errors->has('payment'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('payment') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('usage_cfdi', 'Uso de CFDI: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::select('usage_cfdi', $usage_cfdiOptions, null, ['required' => true, 'class' => $errors->has('usage_cfdi') ? 'form-control parsley-error select2' : 'form-control select2']) }}
                        @if($errors->has('usage_cfdi'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('usage_cfdi') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group" id="limitCredit">
                        {!! Form::label('creditLimit', 'Límite de crédito: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::number('creditLimit', null, ['required'=>true,'min' => 1,'class' => $errors->has('creditLimit') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('creditLimit'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('creditLimit') }}</li></ul>
                        @endif
                    </div>
                </div>
            </div>

            @if(auth()->user()->isAdmin == 1)
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {!! Form::label('vendor_id', 'Vendedor: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::select('vendor_id', $sellers, null, ['required' => true, 'class' => $errors->has('vendor_id') ? 'form-control parsley-error select2' : 'form-control select2']) }}
                        @if($errors->has('vendor_id'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('vendor_id') }}</li></ul>
                        @endif
                    </div>
                </div>
            </div>

            @endif

            <div class="text-right">
                <a href="{{ route('admin.clients') }}" class="btn btn-danger btn-bordered">Cancelar</a>
                {{ Form::submit('Aceptar', ['class' => 'btn btn-success btn-bordered']) }}
            </div>

    {{ Form::close() }}

@endsection

@section('js')

@endsection
<!-- Fin ejemplo de tabla Listado -->
</div>
</main>
