@extends('principal')

@section('content')

<main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">

        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Nuevo cliente

                </div>

                <div class="card-body">

    @if(auth()->user()->isAdmin and count($sellers) < 2)
        <div class="alert alert-icon alert-info fade in" role="alert">
            <i class="mdi mdi-information"></i>
            No es posible dar de alta clientes ya que no existen vendedores. Da <a href="{{ route('admin.sellers.create') }}" class="text-info"><strong><ins><em>clic aquí</em></ins></strong></a> para crear vendedores.
        </div>
    @else
        {{ Form::open(['url' => route('admin.clients.store')]) }}

            @if($errors->any())
                <div class="alert alert-icon alert-danger fade in" role="alert">
                    <i class="mdi mdi-block-helper"></i>
                    Han ocurrido algunos inconvenientes.
                </div>
            @endif

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('name', 'Nombre: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('name', null, ['required' => true, 'class' => $errors->has('name') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('name'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('surname', 'Apellidos: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('surname', null, ['required' => true, 'class' => $errors->has('surname') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('surname'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('surname') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('adress', 'Calle: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('adress', null, ['required' => true, 'class' => $errors->has('adress') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('adress'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('adress') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('colony', 'Colonia: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('colony', null, ['required' => true, 'class' => $errors->has('colony') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('colony'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('colony') }}</li></ul>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('postal_code', 'Código postal: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('postal_code', null, ['required' => true, 'class' => $errors->has('postal_code') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('postal_code'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('postal_code') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('city', 'Ciudad: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('city', null, ['required' => true, 'class' => $errors->has('city') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('city'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('city') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('state', 'Estado: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('state', null, ['required' => true, 'class' => $errors->has('state') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('state'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('state') }}</li></ul>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('phone', 'Teléfono: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('phone', null, ['required' => true, 'class' => $errors->has('phone') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('phone'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('phone') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('email', 'Correo electrónico: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('email', null, ['required' => true, 'class' => $errors->has('email') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('email'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('email') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('password', 'Contraseña: <span class="text-danger">*</span>', [], false) !!}
                        {{-- {{ Form::password('password', null, ['required' => true, 'class' => $errors->has('password') ? 'form-control parsley-error' : 'form-control']) }} --}}
                        {{ Form::password('password', ['class' => $errors->has('password') ? 'form-control parsley-error' : 'form-control', 'required'=>true])}}
                        @if($errors->has('password'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('password') }}</li></ul>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('rfc', 'RFC: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('rfc', null, ['required' => true, 'class' => $errors->has('rfc') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('rfc'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('rfc') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('curp', 'CURP: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('curp', null, ['required' => true, 'class' => $errors->has('curp') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('curp'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('curp') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('payment_method', 'Forma de pago: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::select('payment_method', $payment_methodOptions, null, ['required' => true, 'class' => $errors->has('payment_method') ? 'form-control parsley-error select2' : 'form-control select2']) }}
                        @if($errors->has('payment_method'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('payment_method') }}</li></ul>
                        @endif
                    </div>
                </div>
            </div>



             <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('usage_cfdi', 'Uso de CFDI: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::select('usage_cfdi', $usage_cfdiOptions, null, ['required' => true, 'class' => $errors->has('usage_cfdi') ? 'form-control parsley-error select2' : 'form-control select2']) }}
                        @if($errors->has('usage_cfdi'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('usage_cfdi') }}</li></ul>
                        @endif
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('payment', 'Pagos: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::select('payment', $paymentOptions, null, ['required' => true, 'class' => $errors->has('payment') ? 'form-control parsley-error select2' : 'form-control select2']) }}
                        @if($errors->has('payment'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('payment') }}</li></ul>
                        @endif
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group" id="limitCredit">
                        {!! Form::label('creditLimit', 'Límite de crédito: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::number('creditLimit', null, ['required'=>true,'min' => 1,'class' => $errors->has('creditLimit') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('creditLimit'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('creditLimit') }}</li></ul>
                        @endif
                    </div>
                </div>
            </div>

            @if(auth()->user()->isAdmin == 1)
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('vendor_id', 'Vendedor: <span class="text-danger">*</span>', [], false) !!}
                            {{ Form::select('vendor_id', $sellers, null, ['required' => true, 'class' => $errors->has('vendor_id') ? 'form-control parsley-error select2' : 'form-control select2']) }}
                            @if($errors->has('vendor_id'))
                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('vendor_id') }}</li></ul>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            @elseif(auth()->user()->isAdmin == 3)
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group" style="display: none;">
                        {!! Form::label('vendor_id', 'vendor_id: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('vendor_id', auth()->user()->id, ['required' => true, 'class' => $errors->has('vendor_id') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('vendor_id'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('vendor_id') }}</li></ul>
                        @endif
                    </div>
                </div>
            </div>
            @endif

            <div class="text-right">
                <a href="{{ route('admin.clients') }}" class="btn btn-danger btn-bordered">Cancelar</a>
                {{ Form::submit('Aceptar', ['class' => 'btn btn-success btn-bordered']) }}
            </div>

        {{ Form::close() }}
    @endif

@endsection

@section('js')

    <script type="text/javascript">
        function updateCreditLimitField(value)
        {
            if (value == 1) {
                $('#limitCredit').show();
            } else {
                $('#limitCredit').hide();
            }
        }
    </script>

@endsection
            <!-- Fin ejemplo de tabla Listado -->
        </div>
    </main>
