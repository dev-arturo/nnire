@extends('principal')

@section('content')

<main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">

        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Nuevo cupon

                </div>

                <div class="card-body">

    {{ Form::open(['url' => route('admin.coupons.store')]) }}

        @if($errors->any())
            <div class="alert alert-icon alert-danger fade in" role="alert">
                <i class="mdi mdi-block-helper"></i>
                Han ocurrido algunos inconvenientes.
            </div>
        @endif

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('code', 'Código: <span class="text-danger">*</span>', [], false) !!}
                    {{ Form::text('code', null, ['required' => true, 'class' => $errors->has('code') ? 'form-control parsley-error' : 'form-control']) }}
                    @if($errors->has('code'))
                        <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('code') }}</li></ul>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('usageLimit', 'Usos permitidos: <span class="text-danger">*</span>', [], false) !!}
                    {{ Form::number('usageLimit', null, ['required' => true, 'min' => 1,'class' => $errors->has('usageLimit') ? 'form-control parsley-error' : 'form-control']) }}
                    @if($errors->has('usageLimit'))
                        <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('usageLimit') }}</li></ul>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('percent', 'Porcentaje de descuento: <span class="text-danger">*</span>', [], false) !!}


                    <div class="input-group">
                      {{ Form::number('percent', null, ['id'=>'increase', 'required' => true, 'min' => 1, 'max' => 100, 'step' => '1', 'class' => $errors->has('percent') ? 'form-control parsley-error' : 'form-control']) }}
                      <span class="input-group-addon">%</span>
                    </div>

                    @if($errors->has('percent'))
                        <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('percent') }}</li></ul>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('limitDate', 'Fecha límite de uso: <span class="text-danger">*</span>', [], false) !!}
                    {{ Form::text('limitDate', null, ['required' => true, 'class' => $errors->has('limitDate') ? 'form-control parsley-error dateField' : 'form-control dateField', 'readonly' => 'readonly', 'style' => 'background-color: #fff;']) }}
                    @if($errors->has('limitDate'))
                        <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('limitDate') }}</li></ul>
                    @endif
                </div>
            </div>
        </div>

        <div class="text-right">
            <a href="{{ route('admin.coupons') }}" class="btn btn-danger btn-bordered">Cancelar</a>
            {{ Form::submit('Aceptar', ['id'=>'btnSave', 'class' => 'btn btn-success btn-bordered']) }}
        </div>

    {{ Form::close() }}

@endsection

@section('js')

    <script type="text/javascript">
    </script>

@endsection
<!-- Fin ejemplo de tabla Listado -->
</div>
</main>
