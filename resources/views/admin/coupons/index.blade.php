@extends('principal')

@section('content')

<main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">

        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Lista de cupones
                    <a href="{{ route('admin.coupons.create') }}" class="btn btn-info btn-bordered waves-effect waves-light m-b-20 btn-sm" data-animation="fadein" data-plugin="custommodal"
                    data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-plus"></i> Nuevo</a>
                </div>

                <div class="card-body">


    @if(count($coupons))
        <table class="table no-cellborder grid table-responsive-sm">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Usos</th>
                    <th>Porcentaje</th>
                    <th class="text-right">Fecha límite</th>
                    <th class="text-right">Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($coupons as $coupon)
                    <tr>
                        <td>{{ $coupon->code }}</td>
                        <td>{{$coupon->uses}} de {{ $coupon->usageLimit }}</td>
                        <td>{{ $coupon->percent }} %</td>
                        <td class="text-right">{{ $coupon->limitDate }}</td>
                        <td class="text-right">
                            <a href="{{ route('admin.coupons.edit', $coupon) }}" class="btn btn-xs waves-effect waves-light btn-warning"><i class="fa fa-pencil"></i></a>
                            <a href="{{ route('admin.coupons.delete', $coupon) }}" class="btn btn-xs waves-effect waves-light btn-danger"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert alert-icon alert-danger fade in" role="alert">
            <i class="mdi mdi-block-helper"></i>
            No se encontraron cupones de descuento en la base de datos.
        </div>
    @endif

@endsection

@section('js')
    <script type="text/javascript">
        $('.grid').DataTable({
            columnDefs: [{
                orderable: false,
                targets: [3]
            }],
            order: [[0, 'DESC']]
        });
    </script>
@endsection
<!-- Fin ejemplo de tabla Listado -->
</div>
</main>
