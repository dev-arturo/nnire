@extends('principal')

@section('content')

        <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">

        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Lista de ordenes
                </div>
                <div class="card-body">
                        <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#pendientes" aria-controls="pendientes" role="tab" data-toggle="tab">Pendientes</a></li>
                                <li role="presentation"><a href="#proceso" aria-controls="proceso" role="tab" data-toggle="tab">En Proceso</a></li>
                                <li role="presentation"><a href="#completados" aria-controls="completados" role="tab" data-toggle="tab">Completados</a></li>
                              </ul>

                              <!-- Tab panes -->
                              <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="pendientes">
                                    <div class="row">
                                        {{-- <div class="col-sm-4">
                                            <a href="{{ route('admin.clients.create') }}" class="btn btn-success btn-bordered waves-effect waves-light m-b-20" data-animation="fadein" data-plugin="custommodal"
                                               data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-plus"></i> Nuevo</a>
                                        </div> --}}

                                    </div>

                                    @if(count($ordersPending))
                                        <table class="table table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>Shipped</th>
                                                    <th>Pedido #</th>
                                                    <th>Enviar a</th>
                                                    <th>Fecha</th>
                                                    <th class="text-right">Total</th>
                                                    <th class="text-right">Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($ordersPending as $order)
                                                    <tr>
                                                        <td>
                                                            <a href="#" class="btn btn-xs waves-effect waves-light btn-danger" data-toggle="tooltip" data-placement="right" title="Aún no se ha enviado este pedido">
                                                                <i class="fa fa-exclamation-triangle"></i>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="#"># {{$order->no_order}} </a> por {{$order->name}} {{$order->surname}} <br> {{$order->email}}
                                                        </td>
                                                        <td>{{$order->adress->name}} {{$order->adress->surname}}, {{$order->adress->adress}}, {{$order->adress->city}}, {{$order->adress->state}}, {{$order->adress->postal_code}}</td>
                                                        <td>{{$order->created_at}}</td>
                                                        @if($order->cart->coupon != '')

                                                            <td class="text-right">$ {{number_format($order->cart->totalPrice - $order->descuento, 2, '.', ',')}}</td>
                                                            {{-- <td class="text-right">$ d{{$order->cart->totalPrice - ()}}</td> --}}
                                                        @else
                                                            <td class="text-right">$ {{number_format($order->cart->totalPrice, 2, '.', ',')}}</td>
                                                        @endif()
                                                        <td class="text-right">
                                                            <a href="{{ route('admin.dashboard.show', $order->no_order) }}" class="btn btn-xs waves-effect waves-light btn-success"><i class="fa fa-eye"></i></a>
                                                            @if(auth()->user()->isAdmin == 1)
                                                                <a href="{{ route('admin.dashboard.delete', $order->id) }}" class="btn btn-xs waves-effect waves-light btn-danger"><i class="fa fa-trash"></i></a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                    <div class="alert alert-icon alert-danger fade in" role="alert">
                                            <i class="mdi mdi-block-helper"></i>
                                            No se encontraron pedidos en la base de datos.
                                        </div>
                                    @endif
                                </div>
                                <div role="tabpanel" class="tab-pane" id="proceso">
                                    @if(count($ordersInProgress))
                                        <table class="table  table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>Shipped</th>
                                                    <th>Pedido #</th>
                                                    <th>Enviar a</th>
                                                    <th>Fecha</th>
                                                    <th class="text-right">Total</th>
                                                    <th class="text-right">Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($ordersInProgress as $order)
                                                    <tr>
                                                        <td>
                                                            <a href="#" class="btn btn-xs waves-effect waves-light btn-success" data-toggle="tooltip" data-placement="right" title="Ya se ha enviado el pedido">
                                                                <i class="fa fa-truck"></i>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="#"># {{$order->no_order}} </a> por {{$order->name}} {{$order->surname}} <br> {{$order->email}}
                                                        </td>
                                                        <td>{{$order->adress->name}} {{$order->adress->surname}}, {{$order->adress->adress}}, {{$order->adress->city}}, {{$order->adress->state}}, {{$order->adress->postal_code}}</td>
                                                        <td>{{$order->created_at}}</td>
                                                        @if($order->cart->coupon != '')

                                                            <td class="text-right">$ {{number_format($order->cart->totalPrice - $order->descuento, 2, '.', ',')}}</td>
                                                            {{-- <td class="text-right">$ d{{$order->cart->totalPrice - ()}}</td> --}}
                                                        @else
                                                            <td class="text-right">$ {{number_format($order->cart->totalPrice, 2, '.', ',')}}</td>
                                                        @endif()
                                                        <td class="text-right">
                                                            <a href="{{ route('admin.dashboard.show', $order->no_order) }}" class="btn btn-xs waves-effect waves-light btn-success"><i class="fa fa-eye"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                    <div class="alert alert-icon alert-danger fade in" role="alert">
                                            <i class="mdi mdi-block-helper"></i>
                                            No se encontraron pedidos en la base de datos.
                                        </div>
                                    @endif
                                </div>
                                <div role="tabpanel" class="tab-pane" id="completados">
                                    @if(count($ordersCompleted))
                                        <table class="table  table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>Shipped</th>
                                                    <th>Pedido #</th>
                                                    <th>Enviar a</th>
                                                    <th>Fecha</th>
                                                    <th class="text-right">Total</th>
                                                    <th class="text-right">Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($ordersCompleted as $order)
                                                    <tr>
                                                        <td>
                                                            <a href="#" class="btn btn-xs waves-effect waves-light btn-success">
                                                                <i class="fa fa-exclamation-triangle"></i>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="#"># {{$order->no_order}} </a> por {{$order->name}} {{$order->surname}} <br> {{$order->email}}
                                                        </td>
                                                        <td>{{$order->adress->name}} {{$order->adress->surname}}, {{$order->adress->adress}}, {{$order->adress->city}}, {{$order->adress->state}}, {{$order->adress->postal_code}}</td>
                                                        <td>{{$order->created_at}}</td>
                                                        @if($order->cart->coupon != '')

                                                            <td class="text-right">$ {{number_format($order->cart->totalPrice - $order->descuento, 2, '.', ',')}}</td>
                                                            {{-- <td class="text-right">$ d{{$order->cart->totalPrice - ()}}</td> --}}
                                                        @else
                                                            <td class="text-right">$ {{number_format($order->cart->totalPrice, 2, '.', ',')}}</td>
                                                        @endif()
                                                        <td class="text-right">
                                                            <a href="{{ route('admin.dashboard.show', $order->no_order) }}" class="btn btn-xs waves-effect waves-light btn-success"><i class="fa fa-eye"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                    <div class="alert alert-icon alert-danger fade in" role="alert">
                                            <i class="mdi mdi-block-helper"></i>
                                            No se encontraron pedidos en la base de datos.
                                        </div>
                                    @endif
                                </div>
                              </div>




                        @endsection

                </div>
            </div>
            <!-- Fin ejemplo de tabla Listado -->
        </div>
    </main>

        <!-- Nav tabs -->


