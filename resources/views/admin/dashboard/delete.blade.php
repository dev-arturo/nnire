@extends('principal')

@section('content')

        <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">

        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Ayuda
                </div>
                <div class="card-body">
                    <div class="form-group row">

                                    <div class="col-xs-12">
                                        <div class="page-title-box">
                                            <h4 class="page-title">Eliminar pedido</h4>
                                            <ol class="breadcrumb p-0 m-0">
                                                <li>
                                                    <a href="{{ route('admin.dashboard') }}">Pedidos</a>
                                                </li>
                                                <li class="active">
                                                    Eliminar
                                                </li>
                                            </ol>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                {{ Form::open(['url' => route('admin.dashboard.destroy', $order), 'method' => 'DELETE']) }}

                                    <div class="form-group">
                                        ¿ Confirma que desea eliminar el pedido <strong>{{ $order->no_order }}</strong> ?
                                        <p><em>Esta acción no podrá ser revertida.</em></p>
                                    </div>

                                    <div class="">
                                        <a href="{{ route('admin.dashboard') }}" class="btn btn-danger btn-bordered">Cancelar</a>
                                        {{ Form::submit('Aceptar', ['class' => 'btn btn-success btn-bordered']) }}
                                    </div>

                                {{ Form::close() }}

                            @endsection

                </div>
            </div>
            <!-- Fin ejemplo de tabla Listado -->
        </div>
    </main>

