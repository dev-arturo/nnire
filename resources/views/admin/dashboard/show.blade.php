@extends('principal')

@section('content')


        <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
                <li><a href="{{route('admin.dashboard')}}">Pedidos</a></li>
                <li class="active">{{$orders->no_order}}</li>
              </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Ayuda
                </div>
                <div class="card-body">


        <div class="form-group row">

            <div class="col-md-6">
                @if($orders->estado == 0)
                    <div class="">
                        {{ Form::open(['route' => 'admin.updateNoGuia', 'class' => 'form-inline']) }}
                            <input type="text" name="order_id" value="{{$orders->no_order}}" hidden="true">
                            <input name="no_guia" type="text" placeholder="No. de guía" class="form-control" required="true">
                            <input name="ship_company" type="text" placeholder="Compañía" class="form-control" required="true">
                            <button type="submit" class="btn btn-success">Procesar pedido</button>
                        {{Form::close()}}
                    </div>
                @elseif($orders->estado == 1)
                    <div class="">
                        {{ Form::open(['route' => 'admin.updateOrderToCompleted', 'class' => 'form-inline']) }}
                            <input type="text" name="order_id" value="{{$orders->no_order}}" hidden="true">
                            <button type="submit" class="btn btn-success">Marcar como completado</button>
                        {{Form::close()}}
                    </div>
                @endif()
            </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <div>
                <h2 class="">Detalles de Pedido #{{$orders->no_order}}</h2>
            </div>
            <!-- <h5 class="panel-title">Pago a través de paypal.</h5> -->
          </div>
          <div class="panel-body">
            <h1>{{ $userinfo->name }} {{$userinfo->surname}}</h1>
            <div class="row">
                <div class="col-md-4">
                    <h5>Detalles Generales</h5>
                    <div>
                        <p><strong>Fecha del pedido:</strong></p>
                        <span>{{$orders->created_at}}</span>
                    </div>
                    <br>
                    <div>
                        @if(isset($reference))
                            <div>
                                <p><strong>Tipo de pago:</strong></p>
                                @if(isset($reference->charges[0]->payment_method->service_name))
                                    <span>{{$reference->charges[0]->payment_method->service_name}}</span>
                                @else
                                    <span>{{$reference->charges[0]->payment_method->object == 'card_payment' ? 'Tarjeta de débito/crédito' : ''}}</span>
                                @endif
                            </div>
                            <br>
                            <div>
                                @if(isset($reference->charges[0]->payment_method->service_name))
                                    <p><strong>Información de pago:</strong></p>
                                    <span>Referencia: {{$reference->charges[0]->payment_method->reference}}</span>
                                @endif
                            </div>
                            <br>
                            <div>
                                <p><strong>Estado del pedido:</strong></p>
                                <span><strong>{{$reference->payment_status == 'paid' ? 'Pagado' : 'En Proceso'}}</strong></span>
                            </div>
                        @else
                            {{-- {{dd($order)}} --}}
                            @if($orders->paypal == 1)
                                <div>
                                    <p><strong>Tipo de pago:</strong></p>
                                    <span>Paypal</span>
                                </div>
                                <br>
                                <div>
                                    <p><strong>Estado del pedido:</strong></p>
                                    <span><strong>Pagado</strong></span>
                                </div>
                            @elseif($orders->payment_id == 2)
                                <div>
                                    <p><strong>Tipo de pago:</strong></p>
                                    <span>Depósito bancario</span>
                                </div>
                                <br>
                                <div>
                                    <p><strong>Estado del pedido:</strong></p>
                                    <span><strong>{{ ($orders->estado == 1 || $orders->estado == 2) ? 'Pagado y enviado.' : 'En proceso.' }}</strong></span>
                                </div>
                                <br>
                                <div>
                                    @if(isset($orders->no_guia))
                                        <p><strong>Número de guía:</strong></p>
                                        <span><strong>{{ $orders->no_guia }} - {{$orders->ship_company}}</strong></span>
                                    @endif()
                                </div>
                            @endif()
                        @endif()
                    </div>
                    <br>
                    <div>
                        <p><strong>Cliente:</strong></p>
                        <span>{{$userinfo->name}} {{$userinfo->surname}} - {{$userinfo->email}}</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <h5>Detalles de facturación</h5>
                    <p><strong>Dirección:</strong></p>
                    <p>{{$adress->adress}}</p>
                    <p>{{$adress->colony}}</p>
                    <p>{{$adress->city}}</p>
                    <p>{{$adress->state}}</p>
                    <p>{{$adress->postal_code}}</p>
                    <br>
                    <p>Correo electrónico:</p>
                    <a href="#">{{$userinfo->email}}</a>
                    <p>Teléfono:</p>
                    <a href="#">{{$adress->phone}}</a>
                </div>
                <div class="col-md-4">
                    <h5>Detalles de envio</h5>
                    <p><strong>Dirección:</strong></p>
                    <p>{{$adress->adress}}</p>
                    <p>{{$adress->colony}}</p>
                    <p>{{$adress->city}}</p>
                    <p>{{$adress->state}}</p>
                    <p>{{$adress->postal_code}}</p>
                    @if($orders->estado >= 1)
                        <p><strong>Número de guía:</strong></p>
                        <p>{{$orders->no_guia}}</p>
                    @endif()
                </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="">Artículos</h3>
          </div>
          <div class="panel-body">
             <table class="table  table-responsive">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Artículo</th>
                        <th class="text-right">Coste</th>
                        <th class="text-right">Cantidad</th>
                        <th class="text-right">Total</th>
                        <!-- <th class="text-right">Opciones</th> -->
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders->cart->items as $item)
                        <tr>
                            <td>
                                <a href="#" class="btn btn-xs waves-effect waves-light btn-danger">
                                    <i class="fa fa-exclamation-triangle"></i>
                                </a>
                            </td>
                            <td>
                                <p>{{$item['item']['name']}}</p>
                                <span><strong>SKU: </strong>{{$item['item']['sku']}}</span>
                            </td>
                            <td class="text-right">$ {{number_format($item['item']['price'], 2, '.', ',')}}</td>
                            <td class="text-right">x {{$item['qty']}}</td>
                            <td class="text-right">$ {{number_format($item['price'], 2, '.', ',')}}</td>
                        </tr>
                    @endforeach
                    @if($orders->descuento > 0)
                        <tr>
                            <td>
                                <a href="#" class="btn btn-xs waves-effect waves-light btn-danger">
                                    <i class="fa fa-exclamation-triangle"></i>
                                </a>
                            </td>
                            <td>
                                <p>Descuento</p>
                            </td>
                            <td class="text-right"></td>
                            <td class="text-right"></td>
                            <td class="text-right">$ -{{number_format($orders->descuento, 2, '.', ',')}}</td>
                        </tr>
                    @endif()
                </tbody>
            </table>
          </div>
          <div class="panel-footer" style="height: 55px;">
              <div class="pull-right">
                  <h3>Total: ${{number_format($orders->cart->totalPrice - $orders->descuento, 2, '.', ',')}}</h3>

              </div>
          </div>
        </div>
    </div>
            </div>
    @endsection
                </div>
            </div>
            <!-- Fin ejemplo de tabla Listado -->
        </div>
    </main>

