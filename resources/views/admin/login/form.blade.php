
<!DOCTYPE html>
<html lang="es">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Refacciones Nissan</title>

  <link href="/css/plantilla.css" rel="stylesheet">

</head>

<body class="app flex-row align-items-center">
  <div class="container">
      <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card-group mb-0">
          <div class="card p-4">
              <div class="card-body">
              <h1>Acceder</h1>
              <p class="text-muted">Control de acceso al sistema</p>
              {{ Form::open(['route' => 'admin.login', 'class' => 'form-horizontal']) }}
              <div  class="form-group mb-3">

                        {{ Form::text('email', null, ['class' => $errors->has('email') ? 'form-control parsley-error' : 'form-control', 'placeholder' => 'Email: *']) }}
                        @if($errors->has('email'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('email') }}</li></ul>
                        @endif
                        @if(session()->has('feedback'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ session()->get('feedback') }}</li></ul>
                        @endif

                </div>
                <div class="form-group mb-4">

                            {{ Form::password('password', ['class' => $errors->has('password') ? 'form-control parsley-error' : 'form-control', 'placeholder' => 'Password: *']) }}
                            @if($errors->has('password'))
                                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('password') }}</li></ul>
                            @endif

                    </div>

                    <!--
                    <div class="form-group text-center m-t-30">
                        <div class="col-sm-12">
                            <a href="page-recoverpw.html" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                        </div>
                    </div>
                    -->

                    <div class="row">
                            <div class="col-6">
                            <button class="btn btn-secondary px-4" type="submit">Acceder</button>
                        </div>
                    </div>
                </div>
                    {{ Form::close() }}

          </div>
          <div class="card text-white bg-danger py-5 d-md-down-none" style="width:44%">
            <div class="card-body text-center">
              <div>
                <h2>Refacciones Nissan</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap and necessary plugins -->

<!-- scripts -->
<script>
    var resizefunc = [];
</script>

</body>
</html>
