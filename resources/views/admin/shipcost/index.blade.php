@extends('principal')

@section('content')
<main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">

        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Listado de costos de envío
                    <a href="{{ route('admin.products.create') }}" class="btn btn-info btn-bordered waves-effect waves-light m-b-20 btn-sm" data-animation="fadein" data-plugin="custommodal"
                    data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-plus"></i> Nuevo</a>
                </div>
                <div class="card-body">

     <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Costo de envío actual</h3>
      </div>
      <div class="panel-body">
        Panel content
      </div>
    </div>






@endsection

@section('js')
    <script type="text/javascript">
        $('.grid').DataTable({
            columnDefs: [{
                orderable: false,
                targets: [4]
            }],
            order: [[0, 'asc']]
        });

        $('.inprogress').DataTable({
            columnDefs: [{
                orderable: false,
                targets: [4]
            }],
            order: [[0, 'asc']]
        });

        $('.completed').DataTable({
            columnDefs: [{
                orderable: false,
                targets: [4]
            }],
            order: [[0, 'asc']]
        });
    </script>

    <script>

    </script>

@endsection
<!-- Fin ejemplo de tabla Listado -->
</div>
</main>
