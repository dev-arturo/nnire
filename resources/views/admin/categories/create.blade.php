@extends('principal')

@section('content')

<main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">

        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Agregar categoria
                </div>
                <div class="card-body">

    {{ Form::open(['url' => route('admin.categories.store')]) }}

        @if($errors->any())
            <div class="alert alert-icon alert-danger fade in" role="alert">
                <i class="mdi mdi-block-helper"></i>
                Han ocurrido algunos inconvenientes.
            </div>
        @endif

        <div class="form-group">
            {!! Form::label('name', 'Nombre: <span class="text-danger">*</span>', [], false) !!}
            {{ Form::text('name', null, ['class' => $errors->has('name') ? 'form-control parsley-error' : 'form-control']) }}
            @if($errors->has('name'))
                <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
            @endif
        </div>

        <div class="text-right">
            <a href="{{ route('admin.categories') }}" class="btn btn-danger btn-bordered">Cancelar</a>
            {{ Form::submit('Aceptar', ['class' => 'btn btn-success btn-bordered']) }}
        </div>

    {{ Form::close() }}

@endsection
            <!-- Fin ejemplo de tabla Listado -->
        </div>
    </main>
