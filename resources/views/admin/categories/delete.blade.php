@extends('principal')

@section('content')
<main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">

        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Eliminar categoria
                </div>
                <div class="card-body">

    {{ Form::open(['url' => route('admin.categories.destroy', $category), 'method' => 'DELETE']) }}

        <div class="form-group">
            ¿ Confirma que desea eliminar la categoría <strong>{{ $category->name }}</strong> ?
            <p><em>Esta acción no podrá ser revertida.</em></p>
        </div>

        <div class="">
            <a href="{{ route('admin.categories') }}" class="btn btn-danger btn-bordered">Cancelar</a>
            {{ Form::submit('Aceptar', ['class' => 'btn btn-success btn-bordered']) }}
        </div>

    {{ Form::close() }}

@endsection
            <!-- Fin ejemplo de tabla Listado -->
        </div>
    </main>
