@extends('principal')

@section('content')
<main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">

        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Eliminar modelo de carro
                </div>
                <div class="card-body">

    {{ Form::open(['url' => route('admin.carModels.destroy', $carModel), 'method' => 'DELETE']) }}

        <div class="form-group">
            ¿ Confirma que desea eliminar el modelo <strong>{{ $carModel->name }}</strong> ?
            <p><em>Esta acción no podrá ser revertida.</em></p>
        </div>

        <div class="">
            <a href="{{ route('admin.carModels') }}" class="btn btn-danger btn-bordered">Cancelar</a>
            {{ Form::submit('Aceptar', ['class' => 'btn btn-success btn-bordered']) }}
        </div>

    {{ Form::close() }}

@endsection
</div>
<!-- Fin ejemplo de tabla Listado -->
</div>
</main>
