@extends('principal')

@section('content')

    <main class="main">
            <!-- Breadcrumb -->
            <ol class="breadcrumb">

            </ol>
            <div class="container-fluid">
                <!-- Ejemplo de tabla Listado -->
                <div class="card card-accent-danger mb-3">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Modelos de carro

                                <a href="{{ route('admin.carModels.create') }}" class="btn btn-success btn-bordered waves-effect waves-light m-b-20" data-animation="fadein" data-plugin="custommodal"
                                   data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-plus"></i> Nuevo</a>


                    </div>

                    <div class="card-body">


        <table class="table  table-responsive-sm">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Años</th>
                <th class="text-right">Opciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($carModels as $carModel)
                <tr>
                    <td>{{ $carModel->name }}</td>
                    <td>
                        @foreach($carModel->years as $year)
                            <span class="badge">{{ $year->year }}</span> &nbsp;&nbsp;
                        @endforeach
                    </td>
                    <td class="text-right">
                        <a href="{{ route('admin.carModels.edit', $carModel) }}" class="btn btn-xs waves-effect waves-light btn-warning"><i class="fa fa-pencil"></i></a>
                        <a href="{{ route('admin.carModels.delete', $carModel) }}" class="btn btn-xs waves-effect waves-light btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $carModels->links() }}

        <div class="alert alert-icon alert-danger fade in" role="alert">
            <i class="mdi mdi-block-helper"></i>
            No se encontraron models de carro en la base de datos.
        </div>


@endsection

</div>
<!-- Fin ejemplo de tabla Listado -->
</div>
</main>
