@extends('principal')

@section('content')


<main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">

        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Editar modelo de carro
                </div>
                <div class="card-body">

    {{ Form::model($carModel, ['url' => route('admin.carModels.update', $carModel), 'method' => 'PATCH']) }}

        @if($errors->any())
            <div class="alert alert-icon alert-danger fade in" role="alert">
                <i class="mdi mdi-block-helper"></i>
                Han ocurrido algunos inconvenientes.
            </div>
        @endif

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('name', 'Nombre: <span class="text-danger">*</span>', [], false) !!}
                    {{ Form::text('name', null, ['class' => $errors->has('name') ? 'form-control parsley-error' : 'form-control']) }}
                    @if($errors->has('name'))
                        <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('years', 'Años: <span class="text-danger">*</span>', [], false) !!}
                    {{ Form::text('years', implode (',', $carModel->years()->pluck('year')->toArray()), ['class' => $errors->has('years') ? 'form-control parsley-error' : 'form-control', 'data-role' => 'tagsinput', 'placeholder' => 'introduzca año..']) }}
                    <span class="text-muted font-13">Años separados por coma.</span>
                    @if($errors->has('years'))
                        <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('years') }}</li></ul>
                    @endif
                </div>
            </div>
        </div>

        <div class="text-right">
            <a href="{{ route('admin.carModels') }}" class="btn btn-danger btn-bordered">Cancelar</a>
            {{ Form::submit('Aceptar', ['class' => 'btn btn-success btn-bordered']) }}
        </div>

    {{ Form::close() }}

@endsection
</div>
<!-- Fin ejemplo de tabla Listado -->
</div>
</main>
