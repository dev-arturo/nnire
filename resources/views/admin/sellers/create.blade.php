@extends('principal')

@section('content')


<main class="main">
    <!-- Breadcrumb -->
    <ol class="breadcrumb">

    </ol>
    <div class="container-fluid">
        <!-- Ejemplo de tabla Listado -->
        <div class="card card-accent-danger mb-3">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Nuevo vendedor
                <a href="{{ route('admin.sellers.create') }}" class="btn btn-info btn-bordered waves-effect waves-light m-b-20 btn-sm" data-animation="fadein" data-plugin="custommodal"
               data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-plus"></i> Nuevo</a>
            </div>
            <div class="card-body">

    {{ Form::open(['url' => route('admin.sellers.store')]) }}

        @if($errors->any())
            <div class="alert alert-icon alert-danger fade in" role="alert">
                <i class="mdi mdi-block-helper"></i>
                Han ocurrido algunos inconvenientes.
            </div>
        @endif

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('name', 'Nombre: <span class="text-danger">*</span>', [], false) !!}
                    {{ Form::text('name', null, ['class' => $errors->has('name') ? 'form-control parsley-error' : 'form-control']) }}
                    @if($errors->has('name'))
                        <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('surname', 'Apellidos: <span class="text-danger">*</span>', [], false) !!}
                    {{ Form::text('surname', null, ['class' => $errors->has('surname') ? 'form-control parsley-error' : 'form-control']) }}
                    @if($errors->has('surname'))
                        <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('surname') }}</li></ul>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('email', 'Correo electrónico: <span class="text-danger">*</span>', [], false) !!}
                    {{ Form::email('email', null, ['class' => $errors->has('email') ? 'form-control parsley-error' : 'form-control']) }}
                    @if($errors->has('email'))
                        <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('email') }}</li></ul>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('email_confirmation', 'Confirmar correco electrónico: <span class="text-danger">*</span>', [], false) !!}
                    {{ Form::email('email_confirmation', null, ['class' => $errors->has('email_confirmation') ? 'form-control parsley-error' : 'form-control']) }}
                    @if($errors->has('email_confirmation'))
                        <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('email_confirmation') }}</li></ul>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('password', 'Clave: <span class="text-danger">*</span>', [], false) !!}
                    {{ Form::password('password', ['class' => $errors->has('password') ? 'form-control parsley-error' : 'form-control']) }}
                    @if($errors->has('password'))
                        <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('password') }}</li></ul>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('password_confirmation', 'Confirmar clave: <span class="text-danger">*</span>', [], false) !!}
                    {{ Form::password('password_confirmation', ['class' => $errors->has('password_confirmation') ? 'form-control parsley-error' : 'form-control']) }}
                    @if($errors->has('password_confirmation'))
                        <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('password_confirmation') }}</li></ul>
                    @endif
                </div>
            </div>
        </div>

        <div class="text-right">
            <a href="{{ route('admin.sellers') }}" class="btn btn-danger btn-bordered">Cancelar</a>
            {{ Form::submit('Aceptar', ['class' => 'btn btn-success btn-bordered']) }}
        </div>

    {{ Form::close() }}

@endsection
<!-- Fin ejemplo de tabla Listado -->
</div>
</main>
