@extends('principal')

@section('content')


<main class="main">
    <!-- Breadcrumb -->
    <ol class="breadcrumb">

    </ol>
    <div class="container-fluid">
        <!-- Ejemplo de tabla Listado -->
        <div class="card card-accent-danger mb-3">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Eliminar vendedor

            </div>
            <div class="card-body">

    {{ Form::open(['url' => route('admin.sellers.destroy', $seller), 'method' => 'DELETE']) }}

        <div class="form-group">
            ¿ Confirma que desea eliminar el vendedor <strong>{{ $seller->name }} {{ $seller->surname }}</strong> ?
            {{-- <p><em>Esta acción no podrá ser revertida.</em></p> --}}
        </div>

        <div class="">
            <a href="{{ route('admin.sellers') }}" class="btn btn-danger btn-bordered">Cancelar</a>
            {{ Form::submit('Aceptar', ['class' => 'btn btn-success btn-bordered']) }}
        </div>

    {{ Form::close() }}

@endsection
<!-- Fin ejemplo de tabla Listado -->
</div>
</main>
