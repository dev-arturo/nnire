@extends('principal')

@section('content')

<main class="main">
    <!-- Breadcrumb -->
    <ol class="breadcrumb">

    </ol>
    <div class="container-fluid">
        <!-- Ejemplo de tabla Listado -->
        <div class="card card-accent-danger mb-3">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Listado vendedores
                <a href="{{ route('admin.sellers.create') }}" class="btn btn-info btn-bordered waves-effect waves-light m-b-20 btn-sm" data-animation="fadein" data-plugin="custommodal"
               data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-plus"></i> Nuevo</a>
            </div>
            <div class="card-body">

    @if(count($sellers))
        <table class="table table-responsive-md">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th class="text-right">Clientes</th>
                    <th class="text-right">Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($sellers as $seller)
                    <tr>
                        <td>{{ $seller->name }} {{ $seller->surname }}</td>
                        <td>{{ $seller->email }}</td>
                        <td class="text-right">{{count($seller->clients)}}</td>
                        <td class="text-right">
                            @if($seller->activo == 0)
                                <a href="{{ route('admin.sellers.updateStatus', $seller) }}" class="btn btn-xs waves-effect waves-light btn-success"><i class="fa fa-pencil"></i></a>
                            @endif
                            <a href="{{ route('admin.sellers.edit', $seller) }}" class="btn btn-xs waves-effect waves-light btn-warning"><i class="fa fa-pencil"></i></a>
                            <a href="{{ route('admin.sellers.delete', $seller) }}" class="btn btn-xs waves-effect waves-light btn-danger"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="alert alert-icon alert-danger fade in" role="alert">
            <i class="mdi mdi-block-helper"></i>
            No se encontraron vendedores en la base de datos.
        </div>
    @endif

@endsection

@section('js')
    <script type="text/javascript">
        $('.grid').DataTable({
            columnDefs: [{
                orderable: false,
                targets: [3]
            }],
            order: [[0, 'asc']]
        });
    </script>
@endsection
<!-- Fin ejemplo de tabla Listado -->
</div>
</main>
