@extends('principal')

@section('content')

        <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">

        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Imagenes
                    <a href="{{ route('admin.banners.create') }}" class="btn btn-info btn-bordered waves-effect waves-light m-b-20 btn-sm" data-animation="fadein" data-plugin="custommodal"
                    data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-plus"></i> Nuevo</a>

                </div>
                <div class="card-body">
                        @if(count($banners))
                        <div class="row port m-b-20">
                            <div class="portfolioContainer">
                                @foreach($banners as $banner)
                                    <div class="col-sm-6 col-md-4 natural personal">
                                        <div class="thumb">
                                            <a href="{{ asset($banner->imagePath()) }}" target="_blank">
                                                <img src="{{ asset($banner->imagePath()) }}" class="thumb-img" alt="">
                                            </a>
                                            <div class="gal-detail text-center">
                                                <a href="{{ route('admin.banners.edit', $banner) }}" class="btn btn-warning btn-bordered waves-effect waves-light m-b-20" data-animation="fadein" data-plugin="custommodal"
                                                   data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-pencil"></i></a>
                                                <a href="{{ route('admin.banners.delete', $banner) }}" class="btn btn-danger btn-bordered waves-effect waves-light m-b-20" data-animation="fadein" data-plugin="custommodal"
                                                   data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @else
                        <div class="alert alert-icon alert-danger fade in" role="alert">
                            <i class="mdi mdi-block-helper"></i>
                            No se encontraron banners en la base de datos.
                        </div>
                    @endif

                @endsection
                </div>
            </div>
            <!-- Fin ejemplo de tabla Listado -->
        </div>
    </main>





