@extends('principal')

@section('content')

<main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">


                            <a href="{{ route('admin.banners.create') }}" class="btn btn-success btn-bordered waves-effect waves-light m-b-20" data-animation="fadein" data-plugin="custommodal"
                               data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-plus"></i> Nuevo</a>


        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Ayuda
                </div>
                <div class="card-body">

    {{ Form::model($banner, ['url' => route('admin.banners.update', $banner), 'method' => 'PATCH', 'files' => true]) }}

        @if($errors->any())
            <div class="alert alert-icon alert-danger fade in" role="alert">
                <i class="mdi mdi-block-helper"></i>
                Han ocurrido algunos inconvenientes.
            </div>
        @endif

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('image', 'Imagen:', [], false) !!}
                    {{ Form::file('image', ['class' => $errors->has('image') ? 'form-control parsley-error' : 'form-control']) }}
                    <span class="font-13 text-muted">Sólo si desea cambiar la imagen.</span>
                    @if($errors->has('image'))
                        <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('image') }}</li></ul>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('hyperlink', 'Enlace:', [], false) !!}
                    {{ Form::text('hyperlink', null, ['class' => $errors->has('hyperlink') ? 'form-control parsley-error' : 'form-control']) }}
                    @if($errors->has('hyperlink'))
                        <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('hyperlink') }}</li></ul>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('hyperlinkTarget', 'Target:', [], false) !!}
                    {{ Form::select('hyperlinkTarget', ['_self' => '_SELF', '_blank' => '_BLANK'], null, ['class' => $errors->has('hyperlink') ? 'form-control parsley-error select2' : 'form-control select2']) }}
                    @if($errors->has('hyperlinkTarget'))
                        <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('hyperlinkTarget') }}</li></ul>
                    @endif
                </div>
            </div>
        </div>

        <div class="text-right">
            <a href="{{ route('admin.banners') }}" class="btn btn-danger btn-bordered">Cancelar</a>
            {{ Form::submit('Aceptar', ['class' => 'btn btn-success btn-bordered']) }}
        </div>

    {{ Form::close() }}

@endsection
            <!-- Fin ejemplo de tabla Listado -->
        </div>
    </main>
