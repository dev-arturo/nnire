@extends('principal')

@section('content')

<main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">


        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Agregar subcategoria
                </div>
                <div class="card-body">

    @if(auth()->user()->isAdmin and count($categories) < 2)
        <div class="alert alert-icon alert-info fade in" role="alert">
            <i class="mdi mdi-information"></i>
            No es posible dar de alta subcategorías ya que no existen categorías. Da <strong><ins><em><a href="{{ route('admin.categories.create') }}" class="text-info">clic aquí</a></em></ins></strong> para crear categorías.
        </div>
    @else
        {{ Form::open(['url' => route('admin.subcategories.store')]) }}

            @if($errors->any())
                <div class="alert alert-icon alert-danger fade in" role="alert">
                    <i class="mdi mdi-block-helper"></i>
                    Han ocurrido algunos inconvenientes.
                </div>
            @endif

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('name', 'Nombre: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::text('name', null, ['required'=>true, 'class' => $errors->has('name') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('name'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('category_id', 'Categoría: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::select('category_id', $categories, null, ['required'=>true, 'class' => $errors->has('category_id') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('category_id'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('category_id') }}</li></ul>
                        @endif
                    </div>
                </div>
            </div>

            <div class="text-right">
                <a href="{{ route('admin.subcategories') }}" class="btn btn-danger btn-bordered">Cancelar</a>
                {{ Form::submit('Aceptar', ['class' => 'btn btn-success btn-bordered']) }}
            </div>

        {{ Form::close() }}
    @endif

@endsection
<!-- Fin ejemplo de tabla Listado -->
</div>
</main>
