@extends('principal')

@section('content')
<main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">

        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Agregar producto
                </div>
                <div class="card-body">

    @if(count($carModels) < 2)
        <div class="alert alert-icon alert-info fade in" role="alert">
            <i class="mdi mdi-information"></i>
            No es posible dar de alta productos ya que no existen modelos de carro. Da <strong><ins><em><a href="{{ route('admin.carModels.create') }}" class="text-info">clic aquí</a></em></ins></strong> para crear modelos de carro.
        </div>
    @elseif(count($categories) < 2)
        <div class="alert alert-icon alert-info fade in" role="alert">
            <i class="mdi mdi-information"></i>
            No es posible dar de alta productos ya que no existen categorías. Da <strong><ins><em><a href="{{ route('admin.categories.create') }}" class="text-info">clic aquí</a></em></ins></strong> para crear categorías.
        </div>
    @else
        {{ Form::open(['url' => route('admin.products.store'), 'files' => true]) }}

            @if($errors->any())
                <div class="alert alert-icon alert-danger fade in" role="alert">
                    <i class="mdi mdi-block-helper"></i>
                    Han ocurrido algunos inconvenientes.
                </div>
            @endif

            <div class="row ">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('name', 'Nombre: <span class="text-danger">*</span>', [], false) !!}
                                {{ Form::text('name', null, ['class' => $errors->has('name') ? 'form-control parsley-error' : 'form-control']) }}
                                @if($errors->has('name'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('price', 'Precio: <span class="text-danger">*</span>', [], false) !!}
                                {{ Form::text('price', null, ['class' => $errors->has('price') ? 'form-control parsley-error numberInput' : 'form-control numberInput']) }}
                                @if($errors->has('price'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('price') }}</li></ul>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('wholesale', 'Precio Mayoreo: <span class="text-danger">*</span>', [], false) !!}
                                {{ Form::text('wholesale', null, ['class' => $errors->has('wholesale') ? 'form-control parsley-error numberInput' : 'form-control numberInput']) }}
                                @if($errors->has('wholesale'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('wholesale') }}</li></ul>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('sku', 'SKU: <span class="text-danger">*</span>', [], false) !!}
                                {{ Form::text('sku', null, ['class' => $errors->has('sku') ? 'form-control parsley-error' : 'form-control']) }}
                                @if($errors->has('sku'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('sku') }}</li></ul>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('model', 'Modelo: <span class="text-danger">*</span>', [], false) !!}
                                {{ Form::text('model', null, ['class' => $errors->has('model') ? 'form-control parsley-error' : 'form-control']) }}
                                @if($errors->has('model'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('model') }}</li></ul>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('eanUpc', 'EAN / UPC: <span class="text-danger">*</span>', [], false) !!}
                                {{ Form::text('eanUpc', null, ['class' => $errors->has('eanUpc') ? 'form-control parsley-error' : 'form-control']) }}
                                @if($errors->has('eanUpc'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('eanUpc') }}</li></ul>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('car_model_id', 'Modelo de carro: <span class="text-danger">*</span>', [], false) !!}
                                {{ Form::select('car_model_id', $carModels, null, ['onchange' => 'updateYears(this.value);', 'class' => $errors->has('car_model_id') ? 'form-control parsley-error select2' : 'form-control select2']) }}
                                @if($errors->has('car_model_id'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('car_model_id') }}</li></ul>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('years', 'Año: <span class="text-danger">*</span>', [], false) !!}



                                {{Form::select('years',[],null,array('id' => 'yearField', 'class' => $errors->has('year_id') ? 'form-control parsley-error select2' : 'form-control select2', 'multiple'=>'multiple','name'=>'years[]'))}}
                                @if($errors->has('years'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('years') }}</li></ul>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('category_id', 'Categoría: <span class="text-danger">*</span>', [], false) !!}
                                {{ Form::select('category_id', $categories, null, ['onchange' => 'updateSubcategories(this.value);', 'class' => $errors->has('car_model_id') ? 'form-control parsley-error select2' : 'form-control select2']) }}
                                @if($errors->has('category_id'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('category_id') }}</li></ul>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('subcategory_id', 'Subcategoría: <span class="text-danger">*</span>', [], false) !!}
                                {{ Form::select('subcategory_id', $subcategories, null, ['id' => 'subcategoryField', 'class' => $errors->has('car_model_id') ? 'form-control parsley-error select2' : 'form-control select2']) }}
                                @if($errors->has('subcategory_id'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('subcategory_id') }}</li></ul>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('description', 'Descripción: <span class="text-danger">*</span>', [], false) !!}
                        {{ Form::textarea('description', null, ['class' => $errors->has('description') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('description'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('description') }}</li></ul>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('dataSheet', 'Ficha técnica:', [], false) !!}
                        {{ Form::textarea('dataSheet', null, ['class' => $errors->has('dataSheet') ? 'form-control parsley-error' : 'form-control']) }}
                        @if($errors->has('dataSheet'))
                            <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('dataSheet') }}</li></ul>
                        @endif
                    </div>
                </div>
            </div>

            <div>
                <label>Imagen:</label>
                {{ Form::file('images[]', ['multiple' => true, 'id' => 'images']) }}
                @if($errors->has('images'))
                    <ul class="parsley-errors-list filled" id="parsley-id-20"><li class="parsley-required">{{ $errors->first('images') }}</li></ul>
                @endif
                <div id="previews">

                </div>
            </div>

            <div class="text-right">
                <a href="{{ route('admin.products') }}" class="btn btn-danger btn-bordered">Cancelar</a>
                {{ Form::submit('Aceptar', ['class' => 'btn btn-success btn-bordered']) }}
            </div>

        {{ Form::close() }}
    @endif

@endsection

@section('js')

    <script type="text/javascript">

        $('.numberInput').on('input', function() {
            this.value = this.value
              .replace(/[^\d.]/g, '')             // numbers and decimals only
              .replace(/(^[\d]{7})[\d]/g, '$1')   // not more than 2 digits at the beginning
              .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
              .replace(/(\.[\d]{4})./g, '$1');    // not more than 4 digits after decimal
        });

        function updateSubcategories(value)
        {
            $.ajax({
                type: 'get',
                url: '{{ route('ajax.loadSubcategories') }}/' + value,
                success: function (response) {
                    $('#subcategoryField').html(response);
                }
            });
        }

        function updateYears(value)
        {
            $.ajax({
                type: 'get',
                url: '{{ route('ajax.loadYears') }}/' + value,
                success: function (response) {
                    $('#yearField').html(response);
                }
            });
        }

        $(document).ready(function () {
            $("#images").on('change', e => {
                $("#previews").empty();
                var files = e.target.files;
                for (var i = 0; i < files.length; i++) {
                    var imgId = 'img' + i ;
                    $("#previews").append('<img id="' + imgId + '" src="http://placehold.it/180" alt="your image" style="width: 65px;"/>')
                    readURL(files[i], imgId);
                }
            });
        });
        function readURL(input, el) {
            if (input) {
                var reader = new FileReader();

                reader.onload = function (e) {

                    $("#"+el)
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input);
            }
        }
    </script>

@endsection
<!-- Fin ejemplo de tabla Listado -->
</div>
</main>
