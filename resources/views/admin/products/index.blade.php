@extends('principal')

@section('content')
<main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">

        </ol>
        <div class="container-fluid">
            <!-- Ejemplo de tabla Listado -->
            <div class="card card-accent-danger mb-3">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Listado de productos
                </div>
                <div class="card-body">

    <div class="row">
        <div class="col-sm-4">
            <a href="{{ route('admin.products.create') }}" class="btn btn-info btn-bordered waves-effect waves-light m-b-20 btn-sm" data-animation="fadein" data-plugin="custommodal"
               data-overlaySpeed="200" data-overlayColor="#36404a"><i class="fa fa-plus"></i> Nuevo</a>
        </div>
        <div class="col-sm-4">
            <button type="button" class="btn btn-success btn-bordered waves-effect waves-light m-b-20 btn-sm" data-toggle="modal" data-target="#myModal">
              <i class="fa fa-upload"></i> Importar por excel
            </button>
        </div>
        <div class="col-sm-4  text-right">
            <form action="{{ route('admin.products.search') }}" method="get" class="form-inline">
                <div class="form-group">
                    {{ Form::text('criteria', null, ['class' => 'form-control', 'placeholder' => 'Buscador..']) }}
                    {{ Form::submit('Buscar', ['class' => 'btn btn-primary']) }}
                </div>
            </form>
        </div>
    </div>

    @if(count($products))
        {{ $products->links() }}
        <table class="table no-cellborder grid table-responsive-sm">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Modelo de carro</th>
                    <th>Subcategoría</th>
                    <th>Categoría</th>
                    <th class="text-right">Precio</th>
                    <th class="text-right">Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                    <tr>

                        <td><a href="{{ route('admin.products.show', $product) }}">{{ $product->name }}</a></td>
                        <td>{{ $product->carModel->name }}</td>
                        <td>{{ $product->subcategory->name }}</td>
                        <td>{{ $product->subcategory->category->name }}</td>
                        <td class="text-right">${{ $product->price }}</td>
                        <td class="text-right">
                            @if($product->hasImage())
                                <a href="{{ asset($product->imagePath()) }}" target="_blank" class="btn btn-xs waves-effect waves-light btn-info"><i class="fa fa-image"></i></a>
                            @endif
                            <a href="{{ route('admin.products.addToPromo', $product) }}" class="btn btn-xs waves-effect waves-light btn-success"><i class="fa fa-dollar"></i></a>
                            <a href="{{ route('admin.products.edit', $product) }}" class="btn btn-xs waves-effect waves-light btn-warning"><i class="fa fa-pencil"></i></a>
                            <a href="{{ route('admin.products.delete', $product) }}" class="btn btn-xs waves-effect waves-light btn-danger"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $products->links() }}
    @else
        <div class="alert alert-icon alert-danger fade in" role="alert">
            <i class="mdi mdi-block-helper"></i>
            No se encontraron productos en la base de datos.
        </div>
    @endif

    <hr>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="">Productos en promoción</h3>
      </div>
      <div class="panel-body">
        @if(count($inPromotion))
            {{ $inPromotion->links() }}
            <table class="table no-cellborder grid">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Modelo de carro</th>
                        <th>Subcategoría</th>
                        <th>Categoría</th>
                        <th class="text-right">Precio</th>
                        <th class="text-right">Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($inPromotion as $product)
                        <tr>
                            <td><a href="{{ route('admin.products.show', $product) }}">{{ $product->name }}</a></td>
                            <td>{{ $product->carModel->name }}</td>
                            <td>{{ $product->subcategory->name }}</td>
                            <td>{{ $product->subcategory->category->name }}</td>
                            <td class="text-right">${{ $product->price }}</td>
                            <td class="text-right">
                                @if($product->hasImage())
                                    <a href="{{ asset($product->imagePath()) }}" target="_blank" class="btn btn-xs waves-effect waves-light btn-info"><i class="fa fa-image"></i></a>
                                @endif
                                <a href="{{ route('admin.products.removeOffPromo', $product) }}" class="btn btn-xs waves-effect waves-light btn-success"><i class="fa fa-dollar"></i></a>
                                <a href="{{ route('admin.products.edit', $product) }}" class="btn btn-xs waves-effect waves-light btn-warning"><i class="fa fa-pencil"></i></a>
                                <a href="{{ route('admin.products.delete', $product) }}" class="btn btn-xs waves-effect waves-light btn-danger"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $inPromotion->links() }}
        @else
            <div class="alert alert-icon alert-danger fade in" role="alert">
                <i class="mdi mdi-block-helper"></i>
                No se encontraron productos en promoción en la base de datos.
            </div>
        @endif
      </div>
    </div>



@endsection

@section('js')
    <script type="text/javascript">
/*
        $('.grid').DataTable({
            deferRender: true,
            columnDefs: [{
                orderable: false,
                targets: [5]
            }],
            order: [[0, 'asc']]
        });
*/
    </script>
@endsection
<!-- Fin ejemplo de tabla Listado -->
</div>
</main>
