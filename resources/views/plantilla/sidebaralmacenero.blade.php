


                    <li>
                            <a href="{{ route('admin.dashboard') }}" class="waves-effect {{ $section == 'dashboard' ? 'active' : '' }}"><i class="mdi mdi-account"></i><span> Dashboard </span></a>
                        </li>
                        <li>
                            <a href="{{ route('admin.clients') }}" class="waves-effect {{ $section == 'clients' ? 'active' : '' }}"><i class="mdi mdi-account"></i><span> Clientes </span></a>
                        </li>
                        {{-- {{dd(auth()->user()->isAdmin)}} --}}
                        @if(auth()->user()->isAdmin == 1)
                            <li>
                                <a href="{{ route('admin.banners') }}" class="waves-effect {{ $section == 'banners' ? 'active' : '' }}"><i class="mdi mdi-account"></i><span> Banners </span></a>
                            </li>
                        @endif
                        @if(auth()->user()->isAdmin == 1)
                            <li>
                                <a href="{{ route('admin.products') }}" class="waves-effect {{ $section == 'products' ? 'active' : '' }}"><i class="mdi mdi-account"></i><span> Productos </span></a>
                            </li>
                        @endif
                        @if(auth()->user()->isAdmin == 1)
                            <li>
                                <a href="{{ route('admin.coupons') }}" class="waves-effect {{ $section == 'coupons' ? 'active' : '' }}"><i class="mdi mdi-cards"></i><span> Cupones </span></a>
                            </li>
                        @endif
                        @if(auth()->user()->isAdmin == 1)
                            <li>
                                <a href="{{ route('admin.shipcost') }}" class="waves-effect {{ $section == 'shipcost' ? 'active' : '' }}"><i class="mdi mdi-truck"></i><span> Costos de envío </span></a>
                            </li>
                        @endif
                        @if(auth()->user()->isAdmin == 1)
                            <li>
                                <a href="{{ route('admin.categories') }}" class="waves-effect {{ $section == 'categories' ? 'active' : '' }}"><i class="mdi mdi-account"></i><span> Categorías </span></a>
                            </li>
                        @endif
                        @if(auth()->user()->isAdmin == 1)
                            <li>
                                <a href="{{ route('admin.subcategories') }}" class="waves-effect {{ $section == 'subcategories' ? 'active' : '' }}"><i class="mdi mdi-account"></i><span> Subcategorías </span></a>
                            </li>
                        @endif
                        @if(auth()->user()->isAdmin == 1)
                            <li>
                                <a href="{{ route('admin.carModels') }}" class="waves-effect {{ $section == 'carModels' ? 'active' : '' }}"><i class="mdi mdi-account"></i><span> Modelos de carro </span></a>
                            </li>
                        @endif
                        @if(auth()->user()->isAdmin == 1)
                            <li>
                                <a href="{{ route('admin.sellers') }}" class="waves-effect {{ $section == 'sellers' ? 'active' : '' }}"><i class="mdi mdi-account"></i><span> Vendedores </span></a>
                            </li>
                        @endif
  
