
<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li @click="menu=0" class="nav-item">
                <a class="nav-link active"  href="#"><i class="cui-screen-desktop"></i> Escritorio</a>
            </li>
            <li class="nav-title">
                Mantenimiento
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.dashboard') }}" class="nav-link text-light {{ $section == 'dashboard' ? 'active' : '' }}"><i class="cui-dashboard "></i><span> Dashboard </span></a>
            </li>
             <li class="nav-item">
                <a href="{{ route('admin.clients') }}"class="nav-link text-light {{ $section == 'clients' ? 'active' : '' }}"><i class="cui-people "></i><span> Clientes </span></a>
            </li>
            @if(auth()->user()->isAdmin == 1)
                 <li class="nav-item">
                    <a href="{{ route('admin.banners') }}"class="nav-link text-light {{ $section == 'banners' ? 'active' : '' }}"><i class="cui-cloud "></i><span> Banners </span></a>
                </li>
            @endif
            @if(auth()->user()->isAdmin == 1)
                 <li class="nav-item">
                    <a href="{{ route('admin.products') }}"class="nav-link text-light {{ $section == 'products' ? 'active' : '' }}"><i class="cui-tags"></i><span> Productos </span></a>
                </li>
            @endif
            @if(auth()->user()->isAdmin == 1)
                 <li class="nav-item">
                    <a href="{{ route('admin.coupons') }}"class="nav-link text-light {{ $section == 'coupons' ? 'active' : '' }}"><i class="cui-thumb-up "></i><span> Cupon </span></a>
                </li>
            @endif
            @if(auth()->user()->isAdmin == 1)
            <li class="nav-item">
               <a href="{{ route('admin.categories') }}"class="nav-link text-light {{ $section == 'categories' ? 'active' : '' }}"><i class="cui-layers "></i><span> Categorías </span></a>
           </li>
       @endif
            @if(auth()->user()->isAdmin == 1)
                 <li class="nav-item">
                    <a href="{{ route('admin.shipcost') }}"class="nav-link text-light {{ $section == 'shipcost' ? 'active' : '' }}"><i class="cui-dollar "></i><span> Costos de envío </span></a>
                </li>
            @endif
            @if(auth()->user()->isAdmin == 1)
            <li class="nav-item">
               <a href="{{ route('admin.sellers') }}"class="nav-link text-light {{ $section == 'sellers' ? 'active' : '' }}"><i class="cui-briefcase "></i><span> Vendedores </span></a>
           </li>
       @endif
            @if(auth()->user()->isAdmin == 1)
            <li class="nav-item">
               <a href="{{ route('admin.carModels') }}"class="nav-link text-light {{ $section == 'carModels' ? 'active' : '' }}"><i class="cui-list "></i><span> Modelos de carro </span></a>
           </li>
       @endif
            @if(auth()->user()->isAdmin == 1)
                 <li class="nav-item">
                    <a href="{{ route('admin.subcategories') }}"class="nav-link text-light  {{ $section == 'subcategories' ? 'active' : '' }}"><i class="cui-file"></i><span> Subcategorías </span></a>
                </li>
            @endif
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
