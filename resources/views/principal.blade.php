<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="/img/favicon.png">
    <!-- Id for Channel Notification -->
    <meta name="userId" content="{{ Auth::check() ? Auth::user()->id : ''}}">

    <title>Refacciones Nissan</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Icons -->

    <link rel="stylesheet" href="https://unpkg.com/@coreui/icons/css/coreui-icons.min.css">

    <!-- Icons -->
    <link href="/css/plantilla.css" rel="stylesheet">
</head>

<body class="app header-show sidebar-fixed aside-menu-fixed aside-menu-hidden footer-fixed">
    <div id="wrapper">
    <header class="app-header navbar">
        <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
          <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#"></a>
        <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
          <span class="navbar-toggler-icon"></span>
        </button>
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <img src="/img/avatars/1.png" class="img-avatar" alt="">
                    <span class="d-md-down-none">{{ auth()->user()->fullName()}} </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-header text-center">
                        <strong>Cuenta</strong>
                    </div>
                    <a class="dropdown-item" href="{{ route('admin.logout') }}">
                    <i class="fa fa-lock"></i> Cerrar sesión</a>
                </div>
            </li>
        </ul>
    </header>
    <div class="app-body">

        @include('plantilla.sidebaradministrador')


    @yield('content')
    </div>

    </div>

    <footer class="app-footer">
        <span><a href="http://www.refaccionesnissan.com.mx/">RefaccionesNissan</a> &copy; 2019</span>
        <span class="ml-auto">Desarrollado por <a href="http://www.creaturi.com.mx/">Creaturi</a></span>
    </footer>

    <script src="/js/app.js"></script>

</body>

</html>
